<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage CRPKING
 * @since Crp King 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">

    <head>
<?php /* ?>		<!--  Clickcease.com tracking-->
<script type='text/javascript'>var script = document.createElement('script');
script.async = true; script.type = 'text/javascript';
var target = 'https://www.clickcease.com/monitor/stat.js';
script.src = target;var elem = document.head;elem.appendChild(script);
</script>
<noscript>
<a href='https://www.clickcease.com' rel='nofollow'><img src='https://monitor.clickcease.com/stats/stats.aspx' alt='ClickCease'/></a>
</noscript>
<!--  Clickcease.com tracking-->
        <!-- Begin TVSquared Tracking Code -->
<script type="text/javascript">
  var _tvq = window._tvq = window._tvq || [];
  (function() {
    var u = (("https:" == document.location.protocol) ? "https://collector-8695.tvsquared.com/" : "http://collector-8695.tvsquared.com/");
    _tvq.push(['setSiteId', "TV-18360945-1"]);
    _tvq.push(['setTrackerUrl', u + 'tv2track.php']);
    _tvq.push([function() {
        this.deleteCustomVariable(5, 'page')
    }]);
    _tvq.push(['trackPageView']);
    var d = document,
        g = d.createElement('script'),
        s = d.getElementsByTagName('script')[0];
    g.type = 'text/javascript';
    g.defer = true;
    g.async = true;
    g.src = u + 'tv2track.js';
    s.parentNode.insertBefore(g, s);
  })();
</script>
<!-- End TVSquared Tracking Code -->
        <meta name="google-site-verification" content="yZknhKFX5fQDgVrUnsWEzhZ3CShkaG4L3q3AIoHZ3gE" />
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-88794200-1', 'auto');
            ga('send', 'pageview');
        </script>
<?php */ ?>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11">

        <?php if ( is_singular() && pings_open( get_queried_object() ) ) { ?>
            <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
        <?php } ?>
        <?php
        if ( crpking_check_acf_activation() ) {
            $favicon = get_field( 'crpking_options_favicon', 'option' );
            if ( ! empty( $favicon ) ) {
                ?>
                <!-- Favicon -->
                <link rel="shortcut icon" href="<?php echo $favicon[ 'url' ]; ?>" type="image/x-icon" />
                <?php
            }
        }
        ?>

        <script src="https://maps.google.com/maps/api/js?key=AIzaSyB7Uq5o2oorc1o5VQqStXxryikBgk-i2NQ" type="text/javascript"></script>
     <?php /* ?>  
            <script type="text/javascript" src="https://www.googleadservices.com/pagead/conversion_async.js"></script>

        <script type="text/javascript">
            (function (a, e, c, f, g, h, b, d) {
                var k = {ak: "1067830285", cl: "rcJhCPCQ2HIQjZiX_QM", autoreplace: "(612) 588-9999"};
                a[c] = a[c] || function () {
                    (a[c].q = a[c].q || []).push(arguments)
                };
                a[g] || (a[g] = k.ak);
                b = e.createElement(h);
                b.async = 1;
                b.src = "//www.gstatic.com/wcm/loader.js";
                d = e.getElementsByTagName(h)[0];
                d.parentNode.insertBefore(b, d);
                a[f] = function (b, d, e) {
                    a[c](2, b, k, d, null, new Date, e)
                };
                a[f]()
            })(window, document, "_googWcmImpl", "_googWcmGet", "_googWcmAk", "script");
        </script>

        <!-- Facebook Pixel Code -->
        <script>
            !function (f, b, e, v, n, t, s) {
                if (f.fbq)
                    return;
                n = f.fbq = function () {
                    n.callMethod ?
                            n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                };
                if (!f._fbq)
                    f._fbq = n;
                n.push = n;
                n.loaded = !0;
                n.version = '2.0';
                n.queue = [];
                t = b.createElement(e);
                t.async = !0;
                t.src = v;
                s = b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t, s)
            }(window,
                    document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '207709443099539'); // Insert your pixel ID here.
            fbq('track', 'PageView');
        </script>

        <noscript>
    <img height="1" width="1" style="display:none" alt="facebook" src="https://www.facebook.com/tr?id=207709443099539&ev=PageView&noscript=1"/>
    </noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->
<?php */ ?>
    <?php wp_head(); ?>

</head>

<?php
global $post;
$post_id = $post->ID;
if ( $post_id == 998 ) {
    wp_redirect( 'https://accounts.zoho.com/login?servicename=ZohoCreator&serviceurl=https://creator.zoho.com%2Fjsp%2Fshowapp.jsp%3Fappurl%3D%2Fcarpetking%2Fcontacts%2Fview-perma%2FSales_Dashboardfinal%2F&hide_signup=true&css=https://creator.zoho.com/css/newlogin.css&load_ifr=true' );
}
?>

<body <?php body_class(); ?>>

    <div id="page" class="site">
        <div class="site-inner">
            <?php
            $site_logo     = '';
            $phone         = '';
            $menu_btn_text = '';
            $menu_btn_link = '';
            if ( crpking_check_acf_activation() ) {
                $site_logo     = get_field( 'crpking_options_site_logo', 'option' );
                $site_logo_url = is_array( $site_logo ) ? $site_logo[ 'sizes' ][ 'bbb-thumbnail' ] : '';
                $site_logo_alt = is_array( $site_logo ) ? $site_logo[ 'alt' ] : $site_logo[ 'name' ];

                $phone             = get_field( 'crpking_options_phone', 'option' );
                $menu_bbb_logo     = get_field( 'crpking_options_menu_bbb_logo', 'option' );
                $menu_bbb_logo_src = ! empty( $menu_bbb_logo ) ? $menu_bbb_logo[ 'sizes' ][ 'bbb-thumbnail' ] : '';
                $menu_bbb_logo_alt = ! empty( $menu_bbb_logo ) ? $menu_bbb_logo[ 'alt' ] : '';
                $menu_bbb_logo_alt = ! empty( $menu_bbb_logo_alt ) ? $menu_bbb_logo_alt : $menu_bbb_logo[ 'name' ];
                $menu_btn_text     = get_field( 'crpking_options_menu_btn_text', 'option' );
                $menu_btn_link     = get_field( 'crpking_options_menu_btn_link', 'option' );
                $menu_btn_link     = ! empty( $menu_btn_link ) ? $menu_btn_link : '#';
            }
            if ( class_exists( 'acf' ) ) {
                $location_head_show_hide = get_field( 'carpetking_gen_location_header' );
            }
            $args = array (
                'post_type'      => 'crpking_location',
                'post_status'    => 'publish',
                'posts_per_page' => -1,
                'orderby' => 'title',
                'order'   => 'ASC',
            );
            if ( $location_head_show_hide == 'yes' ) {
                ?>
                <section class="top-header">
                    <div class="container">
                        <div class="top-header-warp">
                            
                            <?php
                            $query = new WP_Query( $args );
                            if ( $query->have_posts() ) {
                                while ( $query->have_posts() ) {
                                    $query->the_post();
                                    $page       = get_the_ID();
                                    $post_title = get_the_title();
                                    $post_link  = get_permalink();
                                    $actual_link = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                                        $active_class = '';
                                    if( $actual_link == $post_link ){
                                        $active_class = 'active_design';
                                    }
                                    if ( class_exists( 'acf' ) ) {
                                        $crpking_loc_phone = get_field( 'crpking_loc_phone' );
                                    }
                                    ?>
                                    <div class="top-find-location <?php echo $active_class; ?>">
                                        <?php if ( ! empty($post_link) || ! empty( $post_title ) ) {
                                            ?>
                                            <div class="top-loctaion-title">
                                                <h3><a href="<?php echo $post_link; ?>"><?php echo $post_title; ?></a></h3>
                                            </div>
                                            <?php
                                        }
                                        if ( ! empty( $crpking_loc_phone ) ) {
                                            ?>
                                            <div class="top-loctaion-phone">
                                                <a href="tel:<?php echo $crpking_loc_phone; ?>"><?php echo $crpking_loc_phone; ?></a>
                                            </div>
                                        <?php }
                                        ?>
                                    </div>
                                    <?php
                                }
                                wp_reset_query();
                                wp_reset_postdata();
                            }
                            ?>
                        </div>
                    </div>
                </section>
<?php }
?>
            <section class="header">
                <div class="header-top">
                    <div class="header_left">
<?php if ( ! empty( $site_logo_url ) ) {
    ?>
                            <div class="logo">
                                <a href="<?php echo home_url(); ?>"><img src="/wp-content/uploads/2018/12/logo4.png" alt="Flooring Expo by Carpet King" title=""></a>
                            </div>
                            <div class="logom">
                                <a href="<?php echo home_url(); ?>"><img src="/wp-content/uploads/2018/12/logo4.png" alt="Minnesota Carpet" title=""></a>
                            </div>
<?php } ?>

                        <?php /* if ( ! empty( $menu_bbb_logo ) ) { ?>
                          <span class="top-bbb-logo">
                          <!--<img src="<?php echo $menu_bbb_logo_src; ?>" alt="<?php echo $menu_bbb_logo_alt; ?>" title="<?php echo $menu_bbb_logo[ 'name' ]; ?>">--><!--<a href="#videos" style="padding-top: 15px; position: relative; display: block; padding-left: 25px;">View Our Latest Videos</a>--><a href="https://youtu.be/HUTOB_-uZao" target="_blank" style="padding-top: 15px; position: relative; display: block; padding-left: 25px; text-transform:uppercase;"><b>Watch</b> our shop at home experience</a><a href="https://youtu.be/qgLR5orknko" target="_blank" style="padding-top: 0px; position: relative; display: block; padding-left: 25px; text-transform:uppercase;"><b>Watch</b> our in-store experience</a>
                          </span>
                          <?php } */ ?>
                    </div>
                    <div class="top-right">
<?php if ( ! empty( $phone ) ) { ?>
                            <span class="top-contact">
                                <a href="<?php echo 'tel:' . $phone; ?>">
                                    <img src="<?php echo get_stylesheet_directory_uri() ?>/images/call-icon.png" alt="Minnesota Flooring" title="">
    <?php echo $phone; ?>
                                </a>
                            </span>
<?php } ?>
                        <?php if ( ! empty( $menu_btn_text ) ) { ?>
                            <div class="top-bnt">
                                <a href="<?php echo $menu_btn_link; ?>">
                                    <span><?php echo $menu_btn_text; ?></span>
                                    <!--<i class="fa fa-calendar" aria-hidden="true"></i>-->
                                </a>
                            </div>
    <?php /*
      <div class="top-bntright">
      <a href="https://www.youtube.com/watch?v=24gR8kZ76lA&t=1s" target="_blank">
      <span>ABOUT US</span>
      <!--<i class="fa fa-calendar" aria-hidden="true"></i>-->
      </a>
      </div>
     */ ?>
                        <?php } ?>
                    </div>
                </div>
                <div class="mainmenu">
                    <div class="container">
<?php
echo wp_nav_menu(
        array (
            'container'      => false,
            'theme_location' => 'primary'
        )
);
?>
                    </div>
                </div>
            </section>
            <div id="content" class="site-content">