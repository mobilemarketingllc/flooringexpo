jQuery(document).ready(function($) {
	    jQuery('#locvalue').html(" - " + jQuery('#input_3_1').val());
		jQuery('.gform_page_footer .button').on('click', function(){
		jQuery(window).scrollTop(0);
	});
    
    jQuery(document).on('submit','form#order_status_get',function(e){
        e.preventDefault();
        var reference = jQuery(this).find('input[name="reference"]').val();
        jQuery.ajax({
            url: CRPKING.ajaxurl,
            method : 'post',
            data : {
                action      : 'cpking_get_order_status',
                reference   : reference
            },
            beforeSend: function() {
                jQuery(document).find('.order_status.order_status_form').css('opacity','0.1');
            },
            success: function (data) {
                jQuery(document).find('.order_status.order_status_form').css('opacity','1');
                jQuery(document).find('.order_status.order_status_form').hide();
                jQuery(document).find('.order_status.order_status_form').after(data);
            }
        });
    });
    jQuery(document).on('click','.serach_again',function(e){
        e.preventDefault();
        jQuery(this).parents('.order_status').remove();
        jQuery(document).find('.order_status.order_status_form').show();
        jQuery(document).find('.order_status.order_status_form').find('input[name="reference"]').val('');
        jQuery('html,body').animate({
            scrollTop: jQuery('.order_status.order_status_form').offset().top - jQuery(document).find('header').height() 
        }, 500);
    });
    $(document).on('click', '.mailpoet_sub_cb', function() {
        $(this).toggleClass('subscr_mail_cb');
    });
    
    jQuery('.wpcf7 #appointment_time').val('');
    jQuery('.wpcf7 #app_time').val('');
    $('.product-category li').click(function() {
        var cat_slug = $(this).children().attr('data-slug');
        if (cat_slug != '') {
            $('#ajax_loader').css('display', 'block');
            jQuery.post(CRPKING.ajaxurl, {
                cat_slug: cat_slug,
                action: 'search_product_action'
            }, function(response) {
                $('#ajax_loader').css('display', 'none');
                $("#product_list").html(response);
            });
        }
    });
    $('.sort_products span').click(function() {
        var sort_type = $(this).attr('id');
        if (sort_type == 'sort_featured') {
            $('#ajax_loader').css('display', 'block');
            jQuery.post(CRPKING.ajaxurl, {
                action: 'sort_featured_product_action'
            }, function(response) {
                $('#ajax_loader').css('display', 'none');
                if (response != '') {
                    $("#product_list").html(response);
                }
            });
        } else if (sort_type == 'sort_newest') {
            $('#ajax_loader').css('display', 'block');
            jQuery.post(CRPKING.ajaxurl, {
                action: 'sort_newest_product_action'
            }, function(response) {
                $('#ajax_loader').css('display', 'none');
                if (response != '') {
                    $("#product_list").html(response);
                }
            });
        } else if (sort_type == 'sort_name') {
            $('#ajax_loader').css('display', 'block');
            jQuery.post(CRPKING.ajaxurl, {
                action: 'sort_name_product_action'
            }, function(response) {
                $('#ajax_loader').css('display', 'none');
                if (response != '') {
                    $("#product_list").html(response);
                }
            });
        }
    });
    jQuery('input[name="appointment_date"]').change(function() {
        jQuery('.schedule-loader').css('display', '');
        jQuery('.schedule-loader').insertAfter('#appointment_date');
        var date = jQuery(this).val();
        if (date != '') {
            var temp_date = date.split("/");
            var new_date = temp_date[2] + '/' + temp_date[0] + '/' + temp_date[1];
            var new_date_format = temp_date[0] + '-' + temp_date[1] + '-' + temp_date[2];
            $('#appointment_date_format').val(new_date_format);
            jQuery.post(CRPKING.ajaxurl, {
                appointment_date: new_date,
                action: 'change_time_slots_action'
            }, function(response) {
                $('.schedule-loader').css('display', 'none');
                if (response != '') {
                    if (jQuery('#appointment_time').length > 0) {
                        jQuery('#appointment_time').html(response);
                    } else {
                        var sel_element = '<span class="wpcf7-form-control-wrap appointment_time">' + '<select name="appointment_time" id="appointment_time" class="wpcf7-select wpcf7-validates-as-required" aria-required="true" aria-invalid="true">' + response + '</select>' + '</span>';
                        jQuery('.appointment_time_wrap').children().append(sel_element);
                    }
                } else {
                    if (jQuery('#appointment_time').length > 0) {
                        jQuery('.wpcf7-form-control-wrap.appointment_time').remove();
                    } else {
                        swal("The time slot isn't available for selected date. Please select the different date.");
                        return false;
                    }
                }
            });
        }
    });
    if ($('.appointment-form input[name="appointment_about_other"]').length > 0) {
        $('.appointment-form input[name="appointment_about_other"]').hide();
    }
    if ($('.appointment-form input[name="appointment_about[]"]').length > 0) {
        $(document).on('click', '.appointment-form input[name="appointment_about[]"]', function() {
            if ($(this).attr('checked')) {
                $('input[name="appointment_about[]"]').not(this).prop('checked', false);
            }
            if ((this.checked) && (this.value == 'Other')) {
                $('.appointment-form input[name="appointment_about_other"]').show();
            } else {
                $('.appointment-form input[name="appointment_about_other"]').hide();
            }
        });
    }
    if ($('.project_description').length > 0) {
        $('.project_description').hide();
    }
    $('.appointment-form input[name="appointment_project[]"]').click(function() {
        var type = this.value;
        var app_time = jQuery('.wpcf7 #appointment_time').val();
        if ($(this).attr('checked')) {
            $('input[name="appointment_project[]"]').not(this).prop('checked', false);
            var $mail_val = $('input[id="email-' + $(this).attr('value') + '"]').val();
            $('#mailto').val($mail_val);
            var $mail_sub = $('input[id="subject-' + $(this).attr('value') + '"]').val();
            $('#mailsubject').val($mail_sub);
        }
        if ((this.checked) && (type == 'Business')) {
            $('.appointment-date').hide();
            $('.rooms_option').hide();
            $('.project_description').show();
            $('.appointment-message').hide();
            jQuery('.wpcf7 #appointment_time').val('');
        } else {
            $('.appointment-date').show();
            $('.rooms_option').show();
            $('.project_description').hide();
            $('.appointment-message').show();
            jQuery('.wpcf7 #appointment_time').val(app_time);
        }
        if ((this.checked) && (type == 'Home')) {
            $('.appointment-date').show();
            $('.rooms_option').show();
            $('.project_description').hide();
            jQuery('.wpcf7 #appointment_time').val(app_time);
        }
    });
    if ($('#print_button').length > 0) {
        $(document).on('click', '#print_button', function() {
            PrintElem('cupon_codesec');
        });
    }
    $(document).on('click', '#home_submit', function(e) {
        $('.zipcode-form-loader').css('display', '');
        var zipcode = $('#home_zipcode').val();
        jQuery.post(CRPKING.ajaxurl, {
            zipcode: zipcode,
            action: 'check_home_page_zipcode'
        }, function(response) {
            $('.zipcode-form-loader').css('display', 'none');
            if (response == 'Success') {
                $('.home-zipcode-form').submit();
            } else {
                swal(response);
                return false;
            }
        });
        e.preventDefault();
    });
    jQuery(document).on('click change', '#appointment_time', function() {
        var time = $(this).val();
        jQuery.post(CRPKING.ajaxurl, {
            time: time,
            action: 'add_schedule_two_hours_more'
        }, function(response) {
            $('#appointment_two_hors_plus_date').val(response);
        });
    });
    var enableDays = CRPKING.temp;

    function enableAllTheseDays(date) {
        var sdate = $.datepicker.formatDate('d-m-yy', date);
        if ($.inArray(sdate, enableDays) != -1) {
            return [true];
        }
        return [false];
    }
    $('#appointment_date').datepicker({
        dateFormat: 'mm/dd/yy',
        beforeShowDay: enableAllTheseDays
    });
    
    /*
     * Page appointment field changes
     */
    $(document).on('click', '.appointment-form input[name="app_project[]"]', function() {
        $('.carpet_details').hide('normal');
        var current_val = $(this).val();
        var app_time = jQuery('.wpcf7 #app_time').val();
        var app_date = jQuery('.wpcf7 #app_date').val();
        if ($(this).attr('checked')) {
            $('input[name="app_project[]"]').not(this).prop('checked', false);
            var $mail_val = $('input[id="email-' + $(this).attr('value') + '"]').val();
            $('#mailto').val($mail_val);
            var $mail_sub = $('input[id="subject-' + $(this).attr('value') + '"]').val();
            $('#mailsubject').val($mail_sub);
        }
        if ((this.checked) && (current_val == 'Business')) {
            $('.carpet_details').show('normal');
            $('.appointment-date').hide('normal');
            $('.rooms_option').hide('normal');
            $('.project_description').show('normal');
            $('.appointment-message').hide('normal');
            jQuery('.wpcf7 #app_time').val('');
        }
        if ((this.checked) && (current_val == 'Home') && (app_time !='') && (app_date != '') ) {
            $('.carpet_details').show('normal');
            $('.appointment-date').show('normal');
            $('.rooms_option').show('normal');
            $('.project_description').hide('normal');
            jQuery('.wpcf7 #app_time').val(app_time);
        } else if((this.checked) && (current_val == 'Home')){
            $('#appointment_date_wrap').show('normal');
        }
    });
    
    $(document).on('click','#app_time',function(){
        var app_time = jQuery('.wpcf7 #app_time').val();
        var app_date = jQuery('.wpcf7 #app_date').val();
        if(app_time !='' && app_date !=''){
            $('.carpet_details').show('normal');
            $('.appointment-date').show('normal');
            $('.rooms_option').show('normal');
            $('.project_description').hide('normal');
            jQuery('.wpcf7 #app_time').val(app_time);
        }
    });
            
    if ($('.appointment-form input[name="app_about_other"]').length > 0) {
        $('.appointment-form input[name="app_about_other"]').hide();
    }
    if ($('.appointment-form input[name="app_about[]"]').length > 0) {
        $(document).on('click', '.appointment-form input[name="app_about[]"]', function() {
            if ($(this).attr('checked')) {
                $('input[name="app_about[]"]').not(this).prop('checked', false);
            }
            if ((this.checked) && (this.value == 'Other')) {
                $('.appointment-form input[name="app_about_other"]').show('normal');
            } else {
                $('.appointment-form input[name="app_about_other"]').hide('normal');
            }
        });
    }
    jQuery('input[name="app_date"]').change(function() {
        jQuery('.schedule-loader').css('display', '');
        jQuery('.schedule-loader').insertAfter('#app_date');
        var date = jQuery(this).val();
        if (date != '') {
            var temp_date = date.split("/");
            var new_date = temp_date[2] + '/' + temp_date[0] + '/' + temp_date[1];
            var new_date_format = temp_date[0] + '-' + temp_date[1] + '-' + temp_date[2];
            $('#app_date_format').val(new_date_format);
            jQuery.post(CRPKING.ajaxurl, {
                appointment_date: new_date,
                action: 'change_time_slots_action'
            }, function(response) {
                $('.schedule-loader').css('display', 'none');
                if (response != '') {
                    if (jQuery('#app_time').length > 0) {
                        jQuery('#app_time').html(response);
                    } else {
                        var sel_element = '<span class="wpcf7-form-control-wrap app_time">' + '<select name="app_time" id="app_time" class="wpcf7-select wpcf7-validates-as-required" aria-required="true" aria-invalid="true">' + response + '</select>' + '</span>';
                        jQuery('.appointment_time_wrap').children().append(sel_element);
                    }
                } else {
                    if (jQuery('#app_time').length > 0) {
                        jQuery('.wpcf7-form-control-wrap.app_time').remove();
                    } else {
                        swal("The time slot isn't available for selected date. Please select the different date.");
                        return false;
                    }
                }
            });
        }
    });
    $('#app_date').datepicker({
        dateFormat: 'mm/dd/yy',
        beforeShowDay: enableAllTheseDays
    });
    
    $(document).on('click', '.land_app_form input[type="submit"]', function() {
        $('#page').addClass( 'loader_active' );
    });
    $(".land_app_form .wpcf7").on('wpcf7:invalid', function(event){
        $('#page').removeClass( 'loader_active' );
    });
    $(".land_app_form .wpcf7").on('wpcf7:mailsent', function(event){
        $('#page').removeClass( 'loader_active' );
    });
    
    document.addEventListener( 'wpcf7mailsent', function( event ) {
        if ( '408' == event.detail.contactFormId ) {
            crpking_redirect();
        }
        if ( '353' == event.detail.contactFormId ) {
            crpking_financing_redirect();
        }
        if ( '837' == event.detail.contactFormId ) {
            crpking_financing_redirect();
        }
        if ( '1703' == event.detail.contactFormId ) {
            crpking_form_redirect();
        }
    }, false );
});

function crpking_redirect() {
    var pro_date = jQuery('.wpcf7 input[name=appointment_date]').val();
    var pro_time = jQuery('.wpcf7 #appointment_time').val();
    var pro_end_time = jQuery('.wpcf7 #appointment_time').find('option:selected').attr('data-value');
    var pro_type = jQuery('.wpcf7 #appointment_project :checkbox:checked').map(function() {
        return jQuery(this).val();
    }).get();
    if (pro_type == 'Business') {
        pro_time = '';
    }
    var pro_floors = jQuery('.wpcf7 #appointment_products :checkbox:checked').map(function() {
        return jQuery(this).val();
    }).get();
    var pro_rooms = jQuery('.wpcf7 #appointment_rooms :checkbox:checked').map(function() {
        return jQuery(this).val();
    }).get();
    var pro_firstname = jQuery('.wpcf7 input[name=appointment_first_name]').val();
    var pro_lastname = jQuery('.wpcf7 input[name=appointment_last_name]').val();
    var pro_address = jQuery('.wpcf7 input[name=appointment_address]').val();
    var pro_city = jQuery('.wpcf7 input[name=appointment_city]').val();
    var pro_state = jQuery('.wpcf7 input[name=appointment_state]').val();
    var pro_zipcode = jQuery('.wpcf7 input[name=appointment_zipcode]').val();
    var pro_email = jQuery('.wpcf7 input[name=appointment_email]').val();
    var pro_phone = jQuery('.wpcf7 input[name=appointment_phone]').val();
    
    goog_report_conversion("https://www.carpetking.com/schedule-in-home-estimate/");
    
    location = CRPKING.site_url + '/appointment-confirmed/?pro_date=' + pro_date + '&pro_time=' + pro_time + '&pro_end_time=' + pro_end_time + '&pro_type=' + pro_type + '&pro_floors=' + pro_floors + '&pro_rooms=' + pro_rooms + '&pro_firstname=' + pro_firstname + '&pro_lastname=' + pro_lastname + '&pro_address=' + pro_address + '&pro_city=' + pro_city + '&pro_state=' + pro_state + '&pro_zipcode=' + pro_zipcode + '&pro_email=' + pro_email + '&pro_phone=' + pro_phone;
}

function crpking_form_redirect() {
    var pro_date = jQuery('.wpcf7 input[name=app_date]').val();
    var pro_time = jQuery('.wpcf7 #app_time').val();
    var pro_end_time = jQuery('.wpcf7 #app_time').find('option:selected').attr('data-value');
    var pro_type = jQuery('.wpcf7 #app_project :checkbox:checked').map(function() {
        return jQuery(this).val();
    }).get();
    if (pro_type == 'Business') {
        pro_time = '';
    }
    var pro_floors = jQuery('.wpcf7 #app_products :checkbox:checked').map(function() {
        return jQuery(this).val();
    }).get();
    var pro_rooms = jQuery('.wpcf7 #app_rooms :checkbox:checked').map(function() {
        return jQuery(this).val();
    }).get();
    var pro_firstname = jQuery('.wpcf7 input[name=app_first_name]').val();
    var pro_lastname = jQuery('.wpcf7 input[name=app_last_name]').val();
    var pro_address = jQuery('.wpcf7 input[name=app_address]').val();
    var pro_city = jQuery('.wpcf7 input[name=app_city]').val();
    var pro_state = jQuery('.wpcf7 input[name=app_state]').val();
    var pro_zipcode = jQuery('.wpcf7 input[name=app_zipcode]').val();
    var pro_email = jQuery('.wpcf7 input[name=your-email]').val();
    var pro_phone = jQuery('.wpcf7 input[name=app_phone]').val();
    
    /* goog_report_conversion("https://www.carpetking.com/schedule-in-home-estimate/"); */
    
    location = CRPKING.site_url + '/appointment-confirmed/?pro_date=' + pro_date + '&pro_time=' + pro_time + '&pro_end_time=' + pro_end_time + '&pro_type=' + pro_type + '&pro_floors=' + pro_floors + '&pro_rooms=' + pro_rooms + '&pro_firstname=' + pro_firstname + '&pro_lastname=' + pro_lastname + '&pro_address=' + pro_address + '&pro_city=' + pro_city + '&pro_state=' + pro_state + '&pro_zipcode=' + pro_zipcode + '&pro_email=' + pro_email + '&pro_phone=' + pro_phone;
}

function crpking_financing_redirect() {
    location = 'https://etail.mysynchrony.com/eapply/eapply.action?uniqueId=0A96374331BD70815088A5096F511363222142C0A3DB2455&client=Carpet_King';
}

function PrintElem(elem) {
    var mywindow = window.open('', 'PRINT', 'height=400,width=600');
    mywindow.document.write('<html><head><title>' + document.title + '</title>');
    mywindow.document.write('</head><body >');
    mywindow.document.write('<h1>' + document.title + '</h1>');
    mywindow.document.write(document.getElementById(elem).innerHTML);
    mywindow.document.write('</body></html>');
    mywindow.document.close();
    mywindow.focus();
    mywindow.print();
    mywindow.close();
    return true;
}

function mytest() {
    jQuery('.appointment_zipcode').find('.wpcf7-not-valid-tip').remove();
    jQuery('.appointment_zipcode').append('<span role="alert" class="wpcf7-not-valid-tip">Unfortunately we currently don\'t service the zip code that you requested.</span>');
    swal('Unfortunately we currently don\'t service the zip code that you requested.');
}

function form_zipcode_test() {
    jQuery('.app_zipcode').find('.wpcf7-not-valid-tip').remove();
    jQuery('.app_zipcode').append('<span role="alert" class="wpcf7-not-valid-tip">Unfortunately we currently don\'t service the zip code that you requested.</span>');
    swal('Unfortunately we currently don\'t service the zip code that you requested.');
}

jQuery(document).ready(function($) {
jQuery('#input_3_1').change(function() {
	//alert('sdsd');
     var val = jQuery(this).val();
     if(val == 'Schedule a FREE In-Home Consultation'){

         window.location.href = "https://www.flooringexpo.com/schedule-in-home-estimate";

     }
});
	});
