/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

jQuery( document ).ready(function($) {
    
    jQuery('#img-upload').click(function(e){
        e.preventDefault();
        var upload = wp.media({
        title:'Choose CSV', //Title for Media Box
        multiple:false, //For limiting multiple image
        library: {
            type: ['application/csv', 'text/csv']
        },
        })
        .on('select', function(){
            var select = upload.state().get('selection');
            var attach = select.first().toJSON();
            var mime_type = attach.mime;
            if ( mime_type == 'text/csv' || mime_type == 'application/csv' ) {
                jQuery(document).find('#csv_url').val(attach.url);
            } else {
                alert("Please Select CSV");
            }
        })
        .open();
   });
   
    jQuery(document).on('submit', 'form#order_csv_import_form', function (e) {
        e.preventDefault();
        var csv_url = jQuery(this).find('#csv_url').val();
        if ( csv_url == '' ) {
            alert('Please Select Any File');
            return;
        }
        var delete_existing = '';
        if (jQuery(this).find('#delete_existing').prop("checked") == true) {
            delete_existing = true;
        } else {
            delete_existing = false;
        }
        jQuery.ajax({
            url: CRPKINGADMIN.ajaxurl,
            method: 'post',
            data: {
                action: 'carpetking_order_csv_import',
                csv_url: csv_url,
                delete_existing: delete_existing
            },
            beforeSend: function() {
                jQuery(document).find('.form_loader_order').show();
                jQuery(document).find('#order_csv_import_form input[type="submit"]').prop('disabled', true);
            },
            success: function (data) {
                jQuery(document).find('#order_csv_import_form input[type="submit"]').prop('disabled', false);
                jQuery(document).find('.form_loader_order').hide();
                if( data == 'true' ) {
                    alert("CSV Imported Successfully");
                } else {
                    alert("There was some error! Please try again.");
                }
                location.reload();
            },
        });
    });
});