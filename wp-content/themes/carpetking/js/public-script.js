jQuery(document).ready(function($) {
    $('#reset_btn').click(function() {
        $('#input_1_1').val('');
    });
    $('.testimonial-slidersec .testimonial-slides').slick({
        dots: false,
        infinite: true,
        slidesToShow: 1,
        autoplay: true,
        arrows: false,
        autoplaySpeed: 2000,
        fade: true,
        speed: 500,
    });
    $('.testimonial-slides > div').slick({
        dots: false,
        infinite: true,
        slidesToShow: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        arrows: false,
        fade: true,
        speed: 500,
    });
    jQuery('.menu').before('<a class="toggle"><i></i></a>');
    jQuery('.toggle').click(function() {
        jQuery(this).toggleClass('active');
        jQuery(this).next().slideToggle(200);
    });
    jQuery('.menu > li > .sub-menu').before('<a class="child-trigger"><span></span></a>');
    jQuery('a.child-trigger').click(function() {
        jQuery(this).toggleClass('child-open');
        jQuery(this).parent().siblings().find('.sub-menu').slideUp(250);
        jQuery(this).next('.sub-menu').slideToggle(250);
    });
});
jQuery(window).scroll(function($) {
    if (jQuery(this).scrollTop() > 151) {
        jQuery('.header').addClass('fixed');
    } else {
        jQuery('.header').removeClass('fixed');
    }
});
jQuery(document).ready(function() {
    equalheight('.same-height');
    contentspace();
});
jQuery(window).resize(function($) {
    equalheight('.same-height');
    contentspace();
})

function equalheight(container) {
    var currentTallest = 0,
        currentRowStart = 0,
        rowDivs = new Array(),
        $el, topPosition = 0,
        currentDiv;
    jQuery(container).each(function() {
        $el = jQuery(this);
        jQuery($el).height('auto')
        topPosition = $el.position().top;
        if (currentRowStart != topPosition) {
            for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
                rowDivs[currentDiv].outerHeight(currentTallest);
            }
            rowDivs.length = 0;
            currentRowStart = topPosition;
            currentTallest = $el.height();
            rowDivs.push($el);
        } else {
            rowDivs.push($el);
            currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
        }
        for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
            rowDivs[currentDiv].height(currentTallest);
        }
    });
}

function contentspace() {
    var windwidth = jQuery(window).width();
    var containerwidth = jQuery('.container').width();
    var contentlrspace = (windwidth - containerwidth) / 2;
    jQuery('.testimonila-slide-content').css('padding-left', contentlrspace);
    jQuery('.testimonila-slide-content').css('padding-right', contentlrspace);
}