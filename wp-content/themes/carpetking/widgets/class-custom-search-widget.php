<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) )
    exit;

add_action( 'widgets_init', 'crpking_search_widget' );

/**
 * Register the Search Widget
 */
function crpking_search_widget() {
    register_widget( 'Search' );
}

class Search extends WP_Widget {

    /**
     * Register widget with WordPress.
     */
    function __construct(){
        parent::__construct(
                'search', // Base ID
                __( 'Crp King - Search', 'crpking' ), // Name
                array('description' => __( 'Custom search widget', 'crpking' )) // Args
        );
    }

    /**
     * Front end display
     * @param type $args
     * @param type $instance
     */
    public function widget($args, $instance) {

        extract( $args );

        echo $before_widget;
        ?>
            <form role="search" method="get" id="searchform" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                <input type="text" name="s" id="search_box" placeholder="Search"/>
            </form>
        <?php
        echo $after_widget;
    }

    /**
     * Update widget values
     * @param type $new_instance
     * @param type $old_instance
     * @return type
     */
    public function update($new_instance, $old_instance) {
        $instance = array();
        return $instance;
    }

    /*
     * Displays the widget form in the admin panel
     */
    function form($instance) {

    }
}
?>