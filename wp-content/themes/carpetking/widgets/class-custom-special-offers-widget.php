<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) )
    exit;

add_action( 'widgets_init', 'crpking_specialoffers_widget' );

/**
 * Register the Special Offers Widget
 */
function crpking_specialoffers_widget() {
    register_widget( 'SpecialOffers' );
}

class SpecialOffers extends WP_Widget {

    /**
     * Register widget with WordPress.
     */
    function __construct(){
        parent::__construct(
                'specialoffers', // Base ID
                __( 'Crp King - Special Offers', 'crpking' ), // Name
                array('description' => __( 'Custom special offers widget', 'crpking' )) // Args
        );
    }

    /**
     * Front end display
     * @param type $args
     * @param type $instance
     */
    public function widget($args, $instance) {

        extract( $args );

        echo $before_widget;

        global $post;
        $post_id = $post->ID;
        $widget_title    = get_field( 'crpking_app_widget_title', $post_id );
        $widget_subtitle = get_field( 'crpking_app_widget_subtitle', $post_id );
        $widget_content  = get_field( 'crpking_app_widget_content', $post_id );
        $widget_btn_text = get_field( 'crpking_app_widget_btn_text', $post_id );
        $widget_btn_link = get_field( 'crpking_app_widget_btn_link', $post_id );
        $widget_btn_link = !empty( $widget_btn_link ) ? $widget_btn_link : '#';
        if( !empty( $widget_subtitle ) || !empty( $widget_content ) ) {
            ?>
                <div class="cupon-codesec" id="cupon_codesec">
                    <?php if( !empty( $widget_title ) ) { ?>
                        <div class="widget_heading">
                            <h4><?php echo $widget_title; ?></h4>
                        </div>
                    <?php } ?>
                    <div class="widget_content">
                        <?php if( !empty( $widget_subtitle ) ) { ?>
                                <h2><?php echo $widget_subtitle; ?></h2>
                        <?php } ?>
                        <?php if( !empty( $widget_content ) ) { ?>
                                <span><?php echo $widget_content; ?></span>
                        <?php } ?>
                    </div>
                    <?php if( !empty( $widget_btn_text ) ) { ?>
                        <div class="widget_link">
                            <a href="<?php echo $widget_btn_link; ?>"><?php echo $widget_btn_text; ?></a>
                        </div>
                        <div class="widget_link">
                            <div class="print_button" id="print_button"><?php echo __( 'Print', 'tacademy' ); ?></div>
                        </div>
                    <?php } ?>
                </div>
            <?php
        }

        echo $after_widget;
    }

    /**
     * Update widget values
     * @param type $new_instance
     * @param type $old_instance
     * @return type
     */
    public function update($new_instance, $old_instance) {
        $instance            = array();

        return $instance;
    }

    /*
     * Displays the widget form in the admin panel
     */
    function form($instance) {
    }
}
?>