<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) )
    exit;

add_action( 'widgets_init', 'crpking_footersubscribe_widget' );

/**
 * Register the Footer Subscribe Widget
 */
function crpking_footersubscribe_widget() {
    register_widget( 'FooterSubscribe' );
}

class FooterSubscribe extends WP_Widget {

    /**
     * Register widget with WordPress.
     */
    function __construct(){
        parent::__construct(
                'footersubscribe', // Base ID
                __( 'Crp King - Footer Subscribe', 'crpking' ), // Name
                array('description' => __( 'Custom footer subscribe widget', 'crpking' )) // Args
        );
    }

    /**
     * Front end display
     * @param type $args
     * @param type $instance
     */
    public function widget($args, $instance) {

        extract( $args );

        echo $before_widget;

        $follow_us_title = apply_filters( 'widget_title', $instance['follow_us_title'] );
        $call_us_title   = apply_filters( 'widget_title', $instance['call_us_title'] );

//        if( !empty( $title ) ) {
//            echo $before_title . $title . $after_title;
//        }
        
        if( crpking_check_acf_activation() ) {
            $gp_url  = get_field( 'crpking_options_google_plus_link', 'option' );
            $insta_url  = get_field( 'crpking_options_instagram_link', 'option' );
            $fb_url  = get_field( 'crpking_options_facebook_link', 'option' );
            $tw_url  = get_field( 'crpking_options_twitter_link', 'option' );
            $li_url  = get_field( 'crpking_options_linkedin_link', 'option' );
            $phone   = get_field( 'crpking_options_footer_phone', 'option' );
?>
            <?php echo do_shortcode( '[contact-form-7 id="265" title="Footer Subscription"]' ); ?>

            <?php if( !empty($gp_url) || !empty($fb_url) || !empty($tw_url) || !empty($li_url) ) { ?>
                <p><?php echo $follow_us_title; ?></p>
                <ul class="f-social">
                    <?php if( !empty($gp_url) ) { ?>
                        <li>
                            <a href="<?php echo $gp_url; ?>" target="_blank"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                        </li>
                    <?php } ?>
                    <?php if( !empty($insta_url) ) { ?>
                        <li>
                            <a href="<?php echo $insta_url; ?>" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                        </li>
                    <?php } ?>
                    <?php if( !empty($fb_url) ) { ?>
                        <li>
                            <a href="<?php echo $fb_url; ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        </li>
                    <?php } ?>
                    <?php if( !empty($tw_url) ) { ?>
                        <li>
                            <a href="<?php echo $tw_url; ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        </li>
                    <?php } ?>
                    <?php if( !empty($li_url) ) { ?>
                        <li>
                            <a href="<?php echo $li_url; ?>" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                        </li>
                    <?php } ?>
                </ul>
            <?php } ?>
             
            <?php if( !empty($phone) ) { ?>
                <p><?php echo $call_us_title; ?> <a href="<?php echo 'tel:' . $phone; ?>"><?php echo $phone; ?></a></p>
            <?php } ?>
<?php
        }
        echo $after_widget;
    }

    /**
     * Update widget values
     * @param type $new_instance
     * @param type $old_instance
     * @return type
     */
    public function update($new_instance, $old_instance) {
        $instance            = array();
        $instance['follow_us_title']    = !empty( $new_instance['follow_us_title'] ) ? strip_tags( $new_instance['follow_us_title'] ) : '';        
        $instance['call_us_title']      = !empty( $new_instance['call_us_title'] ) ? strip_tags( $new_instance['call_us_title'] ) : '';        

        return $instance;
    }

    /*
     * Displays the widget form in the admin panel
     */
    function form($instance) {
        $follow_us_title = !empty( $instance['follow_us_title'] ) ? $instance['follow_us_title'] : '';        
        $call_us_title   = !empty( $instance['call_us_title'] ) ? $instance['call_us_title'] : '';        
    ?>
            <p>
                <label for="<?php echo $this->get_field_id( 'follow_us_title' ); ?>"><?php _e( 'Follow Us Title:', 'crpking' ); ?></label> 
                <input class="widefat" id="<?php echo $this->get_field_id( 'follow_us_title' ); ?>" name="<?php echo $this->get_field_name( 'follow_us_title' ); ?>" type="text" value="<?php echo esc_attr( $follow_us_title ); ?>"/>
            </p>
            <p>
                <label for="<?php echo $this->get_field_id( 'call_us_title' ); ?>"><?php _e( 'Call Us Title:', 'crpking' ); ?></label> 
                <input class="widefat" id="<?php echo $this->get_field_id( 'call_us_title' ); ?>" name="<?php echo $this->get_field_name( 'call_us_title' ); ?>" type="text" value="<?php echo esc_attr( $call_us_title ); ?>"/>
            </p>
    <?php
    }
}
?>