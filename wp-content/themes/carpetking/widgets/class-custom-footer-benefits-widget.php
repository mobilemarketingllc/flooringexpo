<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) )
    exit;

add_action( 'widgets_init', 'crpking_footer_benefits_widget' );

/**
 * Register the Footer Benefits Widget
 */
function crpking_footer_benefits_widget() {
    register_widget( 'FooterBenefits' );
}

class FooterBenefits extends WP_Widget {

    /**
     * Register widget with WordPress.
     */
    function __construct(){
        parent::__construct(
                'footer_benefits', // Base ID
                __( 'Crp King - Footer Benefits', 'crpking' ), // Name
                array('description' => __( 'Custom footer benefits widget', 'crpking' )) // Args
        );
    }

    /**
     * Front end display
     * @param type $args
     * @param type $instance
     */
    public function widget($args, $instance) {

        extract( $args );

        echo $before_widget;

        $title = apply_filters( 'widget_title', $instance['title'] );

        if( !empty( $title ) ) {
            echo $before_title . $title . $after_title;
        }
        
        if( crpking_check_acf_activation() ) {
            $benefits = get_field( 'crpking_footer_benefits', 'option' );
?>
            <?php if( !empty( $benefits ) ) { ?>
                <ul>
                    <?php foreach( $benefits as $key => $value ) { ?>
                        <li><?php echo $value['crpking_footer_benefit_text']; ?></li>
                    <?php } ?>
                </ul>
            <?php } ?>
<?php
        }
        echo $after_widget;
    }

    /**
     * Update widget values
     * @param type $new_instance
     * @param type $old_instance
     * @return type
     */
    public function update($new_instance, $old_instance) {
        $instance            = array();
        $instance['title']   = !empty( $new_instance['title'] ) ? strip_tags( $new_instance['title'] ) : '';        

        return $instance;
    }

    /*
     * Displays the widget form in the admin panel
     */
    function form($instance) {
        $title  = !empty( $instance['title'] ) ? $instance['title'] : '';        
    ?>
            <p>
                <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'crpking' ); ?></label> 
                <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>"/>
            </p>
    <?php
    }
}
?>