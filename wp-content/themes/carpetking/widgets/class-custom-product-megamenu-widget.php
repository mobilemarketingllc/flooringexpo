<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) )
    exit;

add_action( 'widgets_init', 'crpking_product_megamenu_widget' );

/**
 * Register the Product Mega Menu Widget
 */
function crpking_product_megamenu_widget() {
    register_widget( 'ProductMegaMenu' );
}

class ProductMegaMenu extends WP_Widget {

    /**
     * Register widget with WordPress.
     */
    function __construct(){
        parent::__construct(
                'product_megamenu', // Base ID
                __( 'Crp King - Product Mega Menu', 'crpking' ), // Name
                array('description' => __( 'Custom product mega menu widget', 'crpking' )) // Args
        );
    }

    /**
     * Front end display
     * @param type $args
     * @param type $instance
     */
    public function widget($args, $instance) {

        extract( $args );

        echo $before_widget;
        
        $args = array(
            'number'     => $number,
            'orderby'    => $orderby,
            'order'      => $order,
            'hide_empty' => $hide_empty,
            'include'    => $ids
        );

        $product_categories = get_terms( CRPKING_PRODUCT_POST_CAT, $args );
        
        if( !empty( $product_categories ) ) {
            ?>
                <!--<ul class="sub-menu">-->
                    <?php foreach( $product_categories as $key => $value ) { ?>
                        <li>
                            <a href="<?php echo get_term_link( $value ); ?>"><?php echo $value->name; ?></a>
                        </li>
                    <?php } ?>
                <!--</ul>-->
            <?php
        }

        echo $after_widget;
    }

    /**
     * Update widget values
     * @param type $new_instance
     * @param type $old_instance
     * @return type
     */
    public function update($new_instance, $old_instance) {
        $instance            = array();

        return $instance;
    }

    /*
     * Displays the widget form in the admin panel
     */
    function form($instance) {
    }
}
?>