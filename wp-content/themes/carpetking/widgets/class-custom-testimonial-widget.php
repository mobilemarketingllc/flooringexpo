<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) )
    exit;

add_action( 'widgets_init', 'crpking_testimonial_widget' );

/**
 * Register the Testimonial Slider Widget
 */
function crpking_testimonial_widget() {
    register_widget( 'TestimonialSlider' );
}

class TestimonialSlider extends WP_Widget {

    /**
     * Register widget with WordPress.
     */
    function __construct(){
        parent::__construct(
                'testimonial', // Base ID
                __( 'Crp King - Testimonial Slider', 'crpking' ), // Name
                array('description' => __( 'Custom testimonial slider widget', 'crpking' )) // Args
        );
    }

    /**
     * Front end display
     * @param type $args
     * @param type $instance
     */
    public function widget($args, $instance) {

        extract( $args );

        echo $before_widget;

        $testi_query = new WP_Query(
                                    array(
                                        'post_type' => CRPKING_TESTIMONIAL_POST_TYPE,
                                        'posts_per_page' => -1,
                                    )
                );
        if( $testi_query->have_posts() ) {
            ?>
                <div class="widget-testimonials">
                    <div class="testimonial-slides" >
                        <div>
                            <?php while( $testi_query->have_posts() ) { $testi_query->the_post(); ?>
                                <?php
                                    $post_id    = get_the_ID();
                                    $name       = get_field( 'crpking_testi_author_name', $post_id );
                                    $company    = get_field( 'crpking_testi_author_company', $post_id );
                                    $content    = get_field( 'crpking_testi_slider_content', $post_id );
                                    $bg_img     = get_field( 'crpking_testi_widget_slider_bg_img', 'option' );
                                    $bg_img_src = !empty( $bg_img ) ? $bg_img['sizes']['medium_large'] : '';
                                    if( !empty( $content ) ) {
                                ?>
                                        <div class="testimonial-bg "<?php echo !empty( $bg_img_src ) ? ' style="background-image:url(' . $bg_img_src . ')"' : ''; ?>>
                                            <div class=" testimonila-content">
                                                <?php echo $content; ?>
                                                <?php if( !empty( $name ) ) { ?>
                                                    <h4><?php echo $name; ?></h4>
                                                <?php } ?>
                                                <?php if( !empty( $company ) ) { ?>
                                                    <h6><?php echo $company; ?></h6>
                                                <?php } ?>
                                            </div>
                                        </div>
                                <?php } ?>
                            <?php } wp_reset_query(); ?>
                        </div>
                    </div>
                </div>
            <?php
        }
        
        echo $after_widget;
    }

    /**
     * Update widget values
     * @param type $new_instance
     * @param type $old_instance
     * @return type
     */
    public function update($new_instance, $old_instance) {
        $instance            = array();

        return $instance;
    }

    /*
     * Displays the widget form in the admin panel
     */
    function form($instance) {
    }
}
?>