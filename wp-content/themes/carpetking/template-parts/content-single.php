<?php
/**
 * The template part for displaying single posts
 *
 * @package WordPress
 * @subpackage CRPKING
 * @since Crp King 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h3 class="entry-title">', '</h3>' ); ?>
	</header><!-- .entry-header -->
        
        <span class="entry-meta">
            <?php _e( 'By ', 'crpking' ); ?>
            <a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) ;?>"><?php echo get_the_author(); ?></a>
            <?php echo __( ' on ', 'crpking' ) . get_the_date('F jS, Y'); ?>
            <?php
                if( !empty( get_the_category_list() ) ) {
                    echo __( ' in ', 'crpking' ) . get_the_category_list( ',' );
                }
            ?>
        </span>
        <?php
            $attachment_id = get_post_thumbnail_id( $post_id );
            $attachment_src = '';
            if( !empty( $attachment_id ) ) {
                $attachment_src = wp_get_attachment_image_src( $attachment_id, 'full' );
            }
            if( !empty( $attachment_src ) ) {
        ?>
                <div class="entry_image">
                    <img src="<?php echo $attachment_src[0]; ?>">
                </div>
        <?php } ?>
        
	<div class="entry-content">
		<?php
			the_content();

			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'crpking' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'crpking' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );

			if ( '' !== get_the_author_meta( 'description' ) ) {
				get_template_part( 'template-parts/biography' );
			}
		?>
	</div><!-- .entry-content -->


		<?php // crpking_entry_meta(); ?>
		<?php
//			edit_post_link(
//				sprintf(
//					/* translators: %s: Name of current post */
//					__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'crpking' ),
//					get_the_title()
//				),
//				'<span class="edit-link">',
//				'</span>'
//			);
		?>

</article><!-- #post-## -->