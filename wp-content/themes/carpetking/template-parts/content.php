<?php
/**
 * The template part for displaying content
 *
 * @package WordPress
 * @subpackage CRPKING
 * @since Crp King 1.0
 */
$post_id = $post->ID;
?>

<div class="col-md-6 entry_box">
    
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

            <?php
                $attachment_id = get_post_thumbnail_id( $post_id );
                $attachment_src = '';
                if( !empty( $attachment_id ) ) {
                    $attachment_src = wp_get_attachment_image_src( $attachment_id, 'full' );
                }
                if( !empty( $attachment_src ) ) {
            ?>
                    <div class="entry_image">
                        <a href="<?php echo get_the_permalink(); ?>"><img src="<?php echo $attachment_src[0]; ?>"></a>
                    </div>
            <?php } ?>

            <div class="entry-category"><?php echo get_the_category_list( ',' ); ?></div>

            <div class="entry-header">
                    <?php if ( is_sticky() && is_home() && ! is_paged() ) : ?>
                            <span class="sticky-post"><?php _e( 'Featured', 'crpking' ); ?></span>
                    <?php endif; ?>

                    <?php the_title( sprintf( '<h5 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h5>' ); ?>
            </div><!-- .entry-header -->

            <div class="entry-meta">
                <span>
                    <?php if( !empty( get_the_author() ) ) { ?>
                        <?php _e( 'By ', 'crpking' ); ?>
                        <a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) ;?>"><?php echo get_the_author(); ?></a>
                    <?php } ?>
                </span>
                
                <span><?php echo get_the_date('F jS, Y'); ?></span>
            </div>

    </article><!-- #post-## -->
    
</div>