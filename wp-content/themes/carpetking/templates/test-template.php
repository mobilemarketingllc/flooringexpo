<?php



// Exit if accessed directly

if ( !defined( 'ABSPATH' ) ) exit;



/**

 * Template Name: Test Template

 *

 * @package WordPress

 * @subpackage crpking

 * @since crpking 1.0

 */
get_header(); 
global $current_user;
//if($current_user->user_id == 4) {
//    error_reporting(E_ALL);
//    ini_set('display_errors', 1);
//}

?>

<div id="primary" class="content-area">
    <main id="main" class="site-main container" role="main">

            <?php
                // Start the loop.
                while ( have_posts() ) : the_post();

                        // Include the page content template.
                        get_template_part( 'template-parts/content', 'page' );
                
                // End of the loop.
                endwhile;
            ?>

    </main><!-- .site-main -->
</div><!-- .content-area -->

<?php
    get_footer();
?>