<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Template Name: Appointment Scheduled Template
 *
 * @package WordPress
 * @subpackage crpking
 * @since crpking 1.0
 */

get_header(); ?>

    <div id="primary" class="content-area">
	<main id="main" class="site-main container" role="main">

            <?php while( have_posts() ) { the_post(); ?>

                <?php the_content(); ?>

            <?php } wp_reset_query(); ?>
            
        </main>
    </div>

<?php
    get_footer();
?>