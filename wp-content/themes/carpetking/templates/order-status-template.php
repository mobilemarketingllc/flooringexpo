<?php
// Exit if accessed directly
if( ! defined ( 'ABSPATH' ) )
    exit;

/**
 * Template Name: Order Status Template
 *
 * @package WordPress
 * @subpackage zimit
 * @since zimit 1.0
 */
get_header ();
?>

<div class="order_status order_status_form">
    <div class="order_status_wrap">
        <h3>order status</h3>
        <p>Check the Status of your orders by entering your reference number below</p>
        <div class="submit_form">
            <form id="order_status_get" action="">
                <input type="input" name="reference" placeholder="Reference #" required="">
                <button type="">Submit</button>
            </form>
        </div>
    </div>
</div>
<?php
get_footer ();
?>