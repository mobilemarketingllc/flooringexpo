<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Template Name: Location Template
 *
 * @package WordPress
 * @subpackage crpking
 * @since crpking 1.0
 */

get_header(); ?>

<!--<form>-->
    <input type="text" name="search_zip" id="search_zip">
    <span class="search_loc">Enter</span>
<!--</form>-->

<?php

    $code = 55378;
    $answer = getLatLong($code);
    $lat1 = '';
    $lon1 = '';
    if( !empty( $answer ) ) {
        $lat1 = $answer['latitude'];
        $lon1 = $answer['longitude'];
    }

    $d = 5;
    //earth's radius in miles
    $r = 3959;

    //compute max and min latitudes / longitudes for search square
    $latN = rad2deg(asin(sin(deg2rad($lat1)) * cos($d / $r) + cos(deg2rad($lat1)) * sin($d / $r) * cos(deg2rad(0))));
    $latS = rad2deg(asin(sin(deg2rad($lat1)) * cos($d / $r) + cos(deg2rad($lat1)) * sin($d / $r) * cos(deg2rad(180))));
    $lonE = rad2deg(deg2rad($lon1) + atan2(sin(deg2rad(90)) * sin($d / $r) * cos(deg2rad($lat1)), cos($d / $r) - sin(deg2rad($lat1)) * sin(deg2rad($latN))));
    $lonW = rad2deg(deg2rad($lon1) + atan2(sin(deg2rad(270)) * sin($d / $r) * cos(deg2rad($lat1)), cos($d / $r) - sin(deg2rad($lat1)) * sin(deg2rad($latN))));
        
    $args = array(
                    'post_type' => 'crpking_location',
                    'meta_query' => array(
                                    'relation' => 'AND',
                                    array(
                                            'key'     => 'crpking_loc_latitude',
                                            'value'   => array( $latS, $latN ),
                                            'compare' => 'BETWEEN',
                                    ),
                                    array(
                                            'key'     => 'crpking_loc_longitude',
                                            'value'   => array( $lonE, $lonW ),
                                            'compare' => 'BETWEEN',
                                    ),
                    ),
    );
    $loc_query = new WP_Query( $args );
    echo "<pre>" ;
    print_r( $loc_query->request ) ;
    print_r( $loc_query->posts ) ;
    echo "</pre>" ;
?>

<?php
    get_footer();
?>