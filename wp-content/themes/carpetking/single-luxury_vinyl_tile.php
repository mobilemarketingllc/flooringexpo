<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage CRPKING
 * @since Crp King 1.0
 */

get_header(); ?>
 <link rel="stylesheet" href= "<?php echo get_template_directory_uri();?>/swiper/dist/css/swiper.min.css" type="text/css" media="all" />
 <script type="text/javascript" src="<?php echo get_template_directory_uri();?>/swiper/dist/js/swiper.min.js"></script>
  <style type="text/css">
 .swiper-button-next.swiper-button-disabled, .swiper-button-prev.swiper-button-disabled{ opacity:0;}
 </style>
    <div class="container">
<div class="featured_page_main">
<div id="primary" class="content-area">
<main id="main" class="site-main" role="main">
    <div class="product-wrap">
            <div class="container product-box">
                <div class="row">

                    <div class="col-sm-6">

                        <div class="product-img-wrapper">
                        <img src="https://mobilem.liquifire.com/mobilem?source=url[<?php the_field('swatch_image_link') ?>]&scale=size[400x400]&sink" class="img-responsive center" alt="<?php the_title(); ?>" />
                        </div>
                    </div>    
                    <div class="col-sm-6">

                        <?php
                            $brand = get_field( 'brand' );
                            if ( $brand == 'Karastan'){ ?>
                            <img src="<?php echo get_template_directory_uri(); ?>/images/logo_karastan.png" class="product-logo" />

                            <?php } elseif ( $brand == 'SmartSolutions') { ?>
                            <img src="<?php echo get_template_directory_uri(); ?>/images/mohawk-logo.png" class="product-logo" />

                            <?php } elseif ( $brand == 'STAINMASTER') { ?>
                            <img src="<?php echo get_template_directory_uri(); ?>/images/stainmaster_logo.png" class="product-logo" />

                            <?php } elseif ( $brand == 'Floorscapes') { ?>
                            <img src="<?php echo get_template_directory_uri(); ?>/images/mohawk-logo.png" class="product-logo" />

                            <?php } elseif ( $brand == 'Armstrong') { ?>
                            <img src="<?php echo get_template_directory_uri(); ?>/images/armstronglogo.png" class="product-logo" />

                            <?php } elseif ( $brand == 'QuickStep') { ?>
                            <img src="<?php echo get_template_directory_uri(); ?>/images/Quick-step-logo.png" class="product-logo" />
                            
                            <?php } elseif ( $brand == 'Daltile') { ?>
                            <img src="<?php echo get_template_directory_uri(); ?>/images/daltile-logo.gif" class="product-logo" />

                            <?php } else { ?>
                            <?php the_field('brand'); ?>
                            <?php } 
                        ?>
                        <div class="product-brand">
                            <?php if (get_field('collection')){
                                the_field('collection');}
                            ?>                       
                        </div>
                        <h1 itemprop="name" class="product-name"><?php the_title(); ?> </h1>
                        <div class="product-colors">
                           <?php
                               $familysku = get_post_meta($post->ID, 'collection', true);
                               $args = array(
                               'post_type'      => 'luxury_vinyl_tile',
                               'posts_per_page' => -1,
                               'post_status'    => 'publish',
                               'meta_query'     => array(
                                   array(
                                       'key'     => 'collection',
                                       'value'   => $familysku,
                                       'compare' => '='
                                       )
                                       )
                                   );
                                   $the_query = new WP_Query( $args );
                                   $title_param= get_the_title();
                                   $title_param1= get_field('brand');
                                   $title_param2= get_field('collection');
                                   $actual_link = get_permalink();
                           ?>
                           <ul>
                                <li class="found"><?php  echo $the_query ->found_posts; ?></li>
                                <li class="colors">Colors<br/>Available</li>
                           </ul>
                        </div>
                        <!-- ?brandname=<?php //echo $title_param1; ?>&productname=<?php //echo $title_param; ?>&collectionname=<?php //echo $title_param2; ?>&prolink=<?php //echo $actual_link; ?> -->
                        <div class="banner-btn"><a href="/products/lvt-flooring-minnesota/"  style="margin-top:5px;"><strong>SCHEDULE NOW</strong></a></div>
                        <div class="banner-btn wheretobuy"><a href="/find-a-location/"><strong>&nbsp;WHERE TO BUY &nbsp;</strong></a></div>
                    </div>                
                </div>
            </div>
            <div class="container product-box">
                <div class="row">
        <!-- Swiper -->
        <div class="swiper-container" style="margin-top: 40px; margin-bottom: 10px;">
            <div class="swiper-wrapper">
                <?php  while ( $the_query->have_posts() ) {
                    $the_query->the_post(); ?>
                    <div class="swiper-slide">
                       <?php
                        if ( get_field('swatch_image_link')) { ?>
                            <a  href="<?php the_permalink(); ?>">
                            <img src="https://mobilem.liquifire.com/mobilem?source=url[<?php the_field('swatch_image_link'); ?>]&scale=size[70x70]&sink" class="swatch-img tooltipped" data-position="top" data-delay="50" data-tooltip="<?php the_title(); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" width="100" height="100" /></a>
                         <?php } ?>                       
                    </div>
                <?php } ?>
            </div>
            <div class="swiper-pagination"></div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
        </div>
        <?php wp_reset_postdata(); ?>
        </div>
        </div>

        <div class="productDetailTable">
            <div class="tab-wrap text-center">
                <div class="container detailedspecs">                    
                    Detailed Specs                        
                </div>
            </div>
            <div class="container">
                <div class="row tab-row ">
                    <div class="col-sm-12">
                        <div id="prod-attribute" class="col-sm-12">
                            <div class="prod-fields">     
                                <?php
                                
                                $tile_application = get_field('application');
                                $fiber_brand = get_field('fiber_brand');
                                $fiber_type = get_field('fiber_type');
                                $style = get_field('style');  
                                $color_tones =  get_field('color_tones');
                                $color_name =  get_field('color');
                                $shade = get_field('shade');
                                $look = get_field('look');
                                $surface_type = get_field('surface_type');
                                $size = get_field('size');
                                $shape = get_field('shape');
                                
                                ?>
                                
                                <table class="bordered">
                                    <tbody>
                                    <tr>
                                    <td><span class="att-title">Category:</span></td>
                                    <td>Waterproof Flooring</td>
                                </tr>
                                    <?php if(get_field('brand')) {?>
                                    <tr>
                                        <td><span class="att-title">Brand:</span></td>
                                        <td><?php the_field('brand'); ?></td>
                                    </tr>
                                    <?php } ?>
                                    <?php if(get_field('collection')) {?>
                                        <tr>
                                            <td><span class="att-title">Collection:</span></td>
                                            <td><?php the_field('collection'); ?></td>
                                        </tr>
                                    <?php } ?>

                                        <tr>
                                            <td><span class="att-title">Color Name:</span></td>
                                            <td><?php the_title(); ?> <?php if($color_name){ echo " - " . $color_name;} ?></td>
                                        </tr>

                                    <tr>
                                        <td><span class="att-title">SKU:</span></td>
                                        <td><?php  the_field('sku') ?></td>
                                    </tr>
                                    <?php if($shade) {?>
                                        <tr>
                                            <td><span class="att-title">Shade:</span> </td>
                                            <td><?php echo $shade ?></td>
                                        </tr>
                                    <?php } ?>

                                    <?php if($look) {?>
                                        <tr>
                                            <td><span class="att-title">Look:</span> </td>
                                            <td><?php echo $look ?> </td>
                                        </tr>
                                    <?php } ?>
                                    <?php if($shape) {?>
                                        <tr>
                                            <td><span class="att-title">Shape:</span> </td>
                                            <td><?php echo $shape ?> </td>
                                        </tr>
                                    <?php } ?>   
                                    <?php if($surface_type) {?>
                                        <tr>
                                            <td><span class="att-title">Surface:</span> </td>
                                            <td><?php echo $surface_type ?> </td>
                                        </tr>
                                    <?php } ?>       
                                
                                
                                    <?php if($size) {?>
                                        <tr>
                                            <td><span class="att-title">Size:</span> </td>
                                            <td><?php echo $size ?> </td>
                                        </tr>
                                    <?php } ?>
                                    <tr>
                                        <td><span class="att-title">Application:</span> </td>
                                        <td><?php the_field('application'); ?> </td>
                                    </tr>

                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <div class="room-slide" style="background: url('https://mobilem.liquifire.com/mobilem?source=url[<?php the_field('room_scene_image_link') ?>?wid=1400]&scale=size[1400x1400]&sink') no-repeat center bottom; background-size: cover;">
</div>

</main><!-- .site-main -->
</div><!-- .content-area -->
</div>
</div>
<script>
       var slidesPerViewVal;
if(jQuery('.product-box .swiper-container .swiper-slide').length > 10){
    slidesPerViewVal = 10;
}
else{
    slidesPerViewVal = jQuery('.product-box .swiper-container .swiper-slide').length;
}
        var swiper = new Swiper('.swiper-container', {
            //pagination: '.swiper-pagination',
            slidesPerView: slidesPerViewVal,
            paginationClickable: true,
            spaceBetween: 15,
            nextButton: '.swiper-button-next',
            prevButton: '.swiper-button-prev'
            //loop: true,
            //mousewheelControl: true
        });
        jQuery('.swiper-slide a').hover(function(){
var toolTip = jQuery(this).children('img').attr('alt');
jQuery(this).attr('data-tooltip', toolTip);
console.log(toolTip)


})
    </script>
<?php get_footer(); ?>
 