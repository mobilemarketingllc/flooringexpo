<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// Include the TGM_Plugin_Activation class.
require get_stylesheet_directory() . '/lib/class-tgm-plugin-activation.php';
 
/**
 * Register the required plugins for this theme.
 *
 * This function is hooked into tgmpa_init, which is fired within the
 * TGM_Plugin_Activation class constructor.
 */
function crpking_register_required_plugins() {
    
    /*
     * Array of plugin arrays. Required keys are name and slug.
     * If the source is NOT from the .org repo, then source is also required.
     */
    $plugins = array(

            // This is an example of how to include a plugin from the WordPress Plugin Repository.
            array(
                    'name'      => 'Advanced Access Manager',
                    'slug'      => 'advanced-access-manager',
                    'required'  => true,
            ),

            // This is an example of how to include a plugin from the WordPress Plugin Repository.
            array(
                    'name'      => 'Contact Form 7',
                    'slug'      => 'contact-form-7',
                    'required'  => false,
            ),

            // This is an example of how to include a plugin from the WordPress Plugin Repository.
            array(
                    'name'      => 'Google Analytics by Yoast',
                    'slug'      => 'google-analytics-for-wordpress',
                    'required'  => false,
            ),

            // This is an example of how to include a plugin from the WordPress Plugin Repository.
            array(
                    'name'      => 'Google Captcha (reCAPTCHA) by BestWebSoft',
                    'slug'      => 'google-captcha',
                    'required'  => false,
            ),

            // This is an example of how to include a plugin from the WordPress Plugin Repository.
            array(
                    'name'      => 'W3 Total Cache',
                    'slug'      => 'w3-total-cache',
                    'required'  => false,
            ),
        
            // This is an example of how to include a plugin from the WordPress Plugin Repository.
            array(
                    'name'      => 'Yoast SEO',
                    'slug'      => 'wordpress-seo',
                    'required'  => true,
            ),
        
            // This is an example of how to include a plugin bundled with a theme.
            array(
                    'name'               => 'Advanced Custom Fields Pro', // The plugin name.
                    'slug'               => 'advanced-custom-fields-pro', // The plugin slug (typically the folder name).
                    'source'             => get_stylesheet_directory() . '/lib/plugins/advanced-custom-fields-pro.zip', // The plugin source.
                    'required'           => true, // If false, the plugin is only 'recommended' instead of required.
                    'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
                    'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
                    'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
                    'external_url'       => '', // If set, overrides default API URL and points to an external URL.
                    'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
            ),
        
            // This is an example of how to include a plugin bundled with a theme.
            array(
                    'name'               => 'Contact Form 7 Storage', // The plugin name.
                    'slug'               => 'cf7-storage', // The plugin slug (typically the folder name).
                    'source'             => get_stylesheet_directory() . '/lib/plugins/storage-for-contact-form-7.zip', // The plugin source.
                    'required'           => false, // If false, the plugin is only 'recommended' instead of required.
                    'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
                    'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
                    'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
                    'external_url'       => '', // If set, overrides default API URL and points to an external URL.
                    'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
            ),
        
            // This is an example of how to include a plugin bundled with a theme.
            array(
                    'name'               => 'WPBakery Visual Composer', // The plugin name.
                    'slug'               => 'js_composer', // The plugin slug (typically the folder name).
                    'source'             => get_stylesheet_directory() . '/lib/plugins/js_composer.zip', // The plugin source.
                    'required'           => false, // If false, the plugin is only 'recommended' instead of required.
                    'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
                    'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
                    'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
                    'external_url'       => '', // If set, overrides default API URL and points to an external URL.
                    'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
            ),

    );

    /*
     * Array of configuration settings. Amend each line as needed.
     *
     * TGMPA will start providing localized text strings soon. If you already have translations of our standard
     * strings available, please help us make TGMPA even better by giving us access to these translations or by
     * sending in a pull-request with .po file(s) with the translations.
     *
     * Only uncomment the strings in the config array if you want to customize the strings.
     */
    $config = array(
            'id'           => 'crpking',                 // Unique ID for hashing notices for multiple instances of TGMPA.
            'default_path' => '',                      // Default absolute path to bundled plugins.
            'menu'         => 'crpking-install-plugins', // Menu slug.
            'parent_slug'  => 'themes.php',            // Parent menu slug.
            'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
            'has_notices'  => true,                    // Show admin notices or not.
            'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
            'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
            'is_automatic' => false,                   // Automatically activate plugins after installation or not.
            'message'      => '',                      // Message to output right before the plugins table.
            /*
            'strings'      => array(
                    'page_title'                      => __( 'Install Required Plugins', 'theme-slug' ),
                    'menu_title'                      => __( 'Install Plugins', 'theme-slug' ),
                    // <snip>...</snip>
                    'nag_type'                        => 'updated', // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
            )
            */
    );

    tgmpa( $plugins, $config );

}

add_action( 'tgmpa_register', 'crpking_register_required_plugins' );
?>