<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage CRPKING
 * @since Crp King 1.0
 */
get_header();
?>

<div class="container">
    <div class="featured_page_main">

        <div id="primary" class="content-area">
            <main id="main" class="site-main" role="main">

                <div class="col-md-8 posts_section">
                    <?php
                    // Start the loop.
                    while ( have_posts() ) : the_post();

                        // Include the single post content template.
                        get_template_part( 'template-parts/content', 'single' );

                        // If comments are open or we have at least one comment, load up the comment template.
                        if ( comments_open() || get_comments_number() ) {
                            comments_template();
                        }

                        if ( is_singular( 'attachment' ) ) {
                            // Parent post navigation.
//                                        the_post_navigation( array(
//                                                'prev_text' => _x( '<span class="meta-nav">Published in</span><span class="post-title">%title</span>', 'Parent post link', 'crpking' ),
//                                        ) );
                        } elseif ( is_singular( 'post' ) ) {
                            // Previous/next post navigation.
//                                        the_post_navigation( array(
//                                                'next_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Next', 'crpking' ) . '</span> ' .
//                                                        '<span class="screen-reader-text">' . __( 'Next post:', 'crpking' ) . '</span> ' .
//                                                        '<span class="post-title">%title</span>',
//                                                'prev_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Previous', 'crpking' ) . '</span> ' .
//                                                        '<span class="screen-reader-text">' . __( 'Previous post:', 'crpking' ) . '</span> ' .
//                                                        '<span class="post-title">%title</span>',
//                                        ) );
                        }

                    // End of the loop.
                    endwhile;
                    ?>
                </div>
                <script type="application/ld+json">
                    {
                    "@context": "http://schema.org",
                    "@type": "BreadcrumbList",
                    "itemListElement": [{
                    "@type": "ListItem",
                    "position": 1,
                    "item": {
                    "@id": "https://www.carpetking.com/",
                    "name": "Home",
                    "image": "https://www.carpetking.com/wp-content/uploads/2017/03/logo.png"
                    }
                    },{
                    "@type": "ListItem",
                    "position": 2,
                    "item": {
                    "@id": "https://www.carpetking.com/blog/",
                    "name": "Blog",
                    "image": "https://www.carpetking.com/wp-content/uploads/2017/03/logo.png"
                    }
                    },{
                    "@type": "ListItem",
                    "position": 3,
                    "item": {
                    "@id": "<?php echo 'http://' . $_SERVER[ 'HTTP_HOST' ] . $_SERVER[ 'REQUEST_URI' ]; ?>",
                    "name": "<?php the_title(); ?>",
                    "image": "https://www.carpetking.com/wp-content/uploads/2017/03/logo.png"
                    }
                    }]
                    }
                </script>                
                <div class="col-md-4 sidebar_section">
                    <?php dynamic_sidebar( 'blog-sidebar' ); ?>
                </div>

            </main><!-- .site-main -->
        </div><!-- .content-area -->

    </div>
</div>

<?php get_footer(); ?>