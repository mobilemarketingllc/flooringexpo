<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage CRPKING
 * @since Crp King 1.0
 */
get_header();
?>
<div class="intro-wrap_blog">
    <div class="intro-wrap_div">
        <div class="post-titles"> <h1><?php echo _e('Blogs','carpetking'); ?></h1> </div>
    </div>
</div>
<div class="blog_main-wrap">
    <?php
    $args  = array (
        'post_type'      => 'post',
        'post_status'    => 'publish',
        'posts_per_page' => -1,
        'order'          => 'ASC'
    );
    $query = new WP_Query( $args );
    if ( $query->have_posts() ) {
        while ( $query->have_posts() ) {
            $query->the_post();
            $post           = get_the_ID();
            $post_title     = get_the_title();
            $post_link      = get_permalink();
            $post_content   = get_the_content();
            $post_thumbnail = get_the_post_thumbnail_url();
            $author_id      = get_the_author_meta( 'ID' );
            $author         = get_the_author_meta( 'display_name', $author_id );
            $post_date      = get_the_date( 'j M Y' );
            ?>
            <div class="content-wrap">
                <div class="content_section">
                    <?php if ( ! empty( $post_title ) || ! empty($post_link)) {
                        ?>
                        <h2 class="entry-title"><a href="<?php echo $post_link; ?>"><?php echo $post_title; ?></a></h2>
                    <?php }
                    ?>
                    <div class="blog-media-wrap">
                        <div class="left">
                            <div class="img">
                                <?php if ( ! empty( $post_thumbnail ) ) {
                                    ?>
                                    <img class="vc_single_image-img " src="<?php echo $post_thumbnail; ?>" width="160" height="160" alt="judge-final" title="judge-final">
                                    <?php
                                }
                                if ( ! empty( $post_link ) ) {
                                    ?>
                                    <a href="<?php echo $post_link; ?>" class="action-icons link-icon " target="_self">
                                        <div class="icon">
                                            <i class="fa fa-link fa-lg"></i>
                                        </div>
                                    </a>
                                <?php }
                                ?>
                            </div>
                        </div>
                        <div class="right">
                            <div class="content_in">
                                <?php
                                if ( ! empty( $post_content ) ) {
                                    echo wp_trim_words( $post_content, 40, '...' );
                                }
                                if ( ! empty( $post_link ) ) {
                                    ?>
                                    <a class="read-more" href="<?php echo $post_link; ?>"><?php echo _e( 'Continue reading', 'carpetking' ); ?> <i class="fa fa-angle-right fa-lg"></i></a>
                                <?php }
                                ?>
                                <ul class="post-metadata-wrap">
                                    <?php if ( ! empty( $post_date ) ) {
                                        ?>
                                        <li><i class="fa fa-calendar-o"></i> <?php echo $post_date; ?></li>
                                        <?php
                                    }
                                    if ( ! empty( $author ) ) {
                                        ?>
                                        <li><i class="fa fa-user"></i><?php echo $author; ?></li>
                                    <?php }
                                    ?>
        <!--                                    <li><a href="#"><i class="fa fa-list-alt"></i> News</a></li>
        <li><a href="#"><i class="icon-unlike fa fa-heart-o"></i>0 Comments</li></a>
        <li><a href="#"><i class="fa fa-comment-o"></i>Like</li></a>-->
                                </ul>
                            </div>
                        </div>
                        <ul class="post-metadata-wrap_in">
                            <?php
                            $terms_array = array ();
                            $terms       = get_the_terms( $post, 'category' );
                            if ( ! empty( $terms ) ) {
                                foreach ( $terms as $term ) {
                                    $terms_array[] = '<li> <a href=' . get_category_link( $term->term_id ) . '><i class="fa fa-tag"></i>' . $term->name . '</a></li>';
                                }
                            }
                            echo implode( ',', $terms_array );
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
            <?php
        }
        wp_reset_query();
        wp_reset_postdata();
    }
    ?>
</div>
<?php get_footer(); ?>