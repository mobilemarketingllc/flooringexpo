<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage CRPKING
 * @since Crp King 1.0
 */
global $post;
$post_id         = $post->ID;
?>

</div><!--.site-content-->

<?php
$show_ftr_btns   = '';
$first_btn_text  = '';
$first_btn_link  = '';
$second_btn_text = '';
$second_btn_link = '';
$tnc_text        = '';
$show_testi      = '';
if ( crpking_check_acf_activation() ) {
    if ( is_singular( CRPKING_LOCATION_POST_TYPE ) ) {
        $show_ftr_btns = get_field( 'crpking_page_show_ftr_btns', CRPKING_LOCATION_PAGE_ID );
    } else {
        $show_ftr_btns = get_field( 'crpking_page_show_ftr_btns', $post_id );
    }

    $first_btn_text    = get_field( 'crpking_footer_first_btn_text', 'option' );
    $first_btn_link    = get_field( 'crpking_footer_first_btn_link', 'option' );
    $first_btn_link    = ! empty( $first_btn_link ) ? $first_btn_link : '#';
    $second_btn_text   = get_field( 'crpking_footer_second_btn_text', 'option' );
    $second_btn_link   = get_field( 'crpking_footer_second_btn_link', 'option' );
    $second_btn_link   = ! empty( $second_btn_link ) ? $second_btn_link : '#';
    $tnc_text          = get_field( 'crpking_options_tnc_text', 'option' );
    $copyright_text    = get_field( 'crpking_options_copyright_text', 'option' );
    $menu_bbb_logo     = get_field( 'crpking_options_menu_bbb_logo', 'option' );
    $menu_bbb_logo_src = ! empty( $menu_bbb_logo ) ? $menu_bbb_logo[ 'sizes' ][ 'bbb-thumbnail' ] : '';
    $menu_bbb_logo_alt = ! empty( $menu_bbb_logo ) ? $menu_bbb_logo[ 'alt' ] : '';
    $menu_bbb_logo_alt = ! empty( $menu_bbb_logo_alt ) ? $menu_bbb_logo_alt : $menu_bbb_logo[ 'name' ];
    $first_link_text   = get_field( 'crpking_footer_first_link_text', 'option' );
    $first_link_url    = get_field( 'crpking_footer_first_link_url', 'option' );
    $first_link_url    = ! empty( $first_link_url ) ? $first_link_url : '#';
    $second_link_text  = get_field( 'crpking_footer_second_link_text', 'option' );
    $second_link_url   = get_field( 'crpking_footer_second_link_url', 'option' );
    $second_link_url   = ! empty( $second_link_url ) ? $second_link_url : '#';

    $show_testi = get_field( 'crpking_page_show_testi_slider', $post_id );
}
?>

<?php
if ( $show_testi == '1' ) {
    echo do_shortcode( '[crpking-testimonial-slider]' );
}
?>

<section class="footer">
    <?php if ( $show_ftr_btns == '1' ) { ?>
        <?php if ( ! empty( $first_btn_text ) ) { ?>
            <div class="footer-appoiment same-height">
                <a href="<?php echo $first_btn_link; ?>"><?php echo $first_btn_text; ?></a>
            </div>
        <?php } ?>
        <?php if ( ! empty( $second_btn_text ) ) { ?>
            <div class="footer-offer same-height">
                <a href="<?php echo $second_btn_link; ?>"><?php echo $second_btn_text; ?></a>
            </div>
        <?php } ?>
    <?php } ?>
    <div class="footer-box-row-sec">
        <div class="container">
            <div class="row">
                <?php
                if ( is_active_sidebar( 'footer-sidebar-1' ) ) {
                    dynamic_sidebar( 'footer-sidebar-1' );
                }
                if ( is_active_sidebar( 'footer-sidebar-2' ) ) {
                    dynamic_sidebar( 'footer-sidebar-2' );
                }
                if ( is_active_sidebar( 'footer-sidebar-3' ) ) {
                    dynamic_sidebar( 'footer-sidebar-3' );
                }
                if ( is_active_sidebar( 'footer-sidebar-4' ) ) {
                    dynamic_sidebar( 'footer-sidebar-4' );
                }
                ?>


                <?php if ( ! empty( $menu_bbb_logo ) ) { ?>
                    <span class="top-bbb-logo">
                        <img src="<?php echo $menu_bbb_logo_src; ?>" alt="<?php echo $menu_bbb_logo_alt; ?>" title="<?php echo $menu_bbb_logo[ 'name' ]; ?>">
                    </span>
                <?php } ?>
            </div>
        </div>
    </div>
    <?php if ( ! empty( $copyright_text ) || ! empty( $first_link_text ) || ! empty( $second_link_text ) ) { ?>
        <div class="footer-job-sec">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-6 job-txt">
                            <?php echo $copyright_text; ?>
                        </div>
                        <div class="col-md-6 job-txt">
                            <a href="<?php echo $first_link_url; ?>"><?php echo $first_link_text; ?></a>
                        </div>
                    </div>
                    <div class="col-md-6 alignright">
                        <a href="<?php echo $second_link_url; ?>"><?php echo $second_link_text; ?></a>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <?php if ( ! empty( $tnc_text ) ) { ?>
        <div class="footer-txt-sec">
            <?php echo $tnc_text; ?>
        </div>
    <?php } ?>
</section>

<?php if ( $post_id == CRPKING_FINANCING_PAGE_ID ) { ?>
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><?php _e( 'Enter the following details' ); ?></h4>
                </div>
                <div class="modal-body">
                    <?php echo do_shortcode( '[contact-form-7 id="837" title="Financing form"]' ); ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
</div><!--.site-inner--> 
</div><!--.site-->

<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 1067830285;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
</script>

<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>

<noscript>
<div style="display:inline;">
    <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1067830285/?guid=ON&amp;script=0"/>
</div>
</noscript>

<?php wp_footer(); ?>

</body>
</html>
