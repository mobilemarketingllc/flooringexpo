<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

add_theme_support( 'woocommerce' );

/*
 * Display breadcrumb action removed
 */
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );

/*
 * Display product count action removed
 */
//remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );

/*
 * Remove Data tabs from single product page
 */
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );

/*
 * Remove Data tabs from single product page
 */
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );

/*
 * Action to add 
 */
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_product_code', 6 );

if ( ! function_exists( 'woocommerce_template_single_product_code' ) ) {

	/**
	 * Output the product title.
	 *
	 * @subpackage	Product
	 */
	function woocommerce_template_single_product_code() {
		wc_get_template( 'single-product/code.php' );
	}
}

/*
 * Action to add product thumbnail images in shop page
 */
add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnails', 10 );

if ( ! function_exists( 'woocommerce_template_loop_product_thumbnails' ) ) {

	/**
	 * Output the product title.
	 *
	 * @subpackage	Product
	 */
	function woocommerce_template_loop_product_thumbnails() {
		?>
                    <div class="product_other_images">
                        <?php do_action( 'woocommerce_product_thumbnails' ); ?>
                    </div>
                <?php
	}
}
?>