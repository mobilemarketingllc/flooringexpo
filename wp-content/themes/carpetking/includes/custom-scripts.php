<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Enqueue scripts and styles for the back end.
 */
function crpking_admin_scripts() {
    
        global $wp_version;
    
        // Load our admin stylesheet.
	wp_enqueue_style( 'crpking-admin-style', get_template_directory_uri() . '/css/admin-style.css' );
        
        // Load our admin script.
	wp_enqueue_script( 'crpking-admin-script', get_template_directory_uri() . '/js/admin-script.js',array(),time());

        //localize script
        $newui = $wp_version >= '3.5' ? '1' : '0'; //check wp version for showing media uploader
        wp_localize_script( 'crpking-admin-script', 'CRPKINGADMIN', array(
            'new_media_ui'	=>  $newui,
            'ajaxurl'   => admin_url( 'admin-ajax.php', ( is_ssl() ? 'https' : 'http' ) ),
        ));
        wp_enqueue_media();
}

/**
 * Enqueue scripts and styles for the front end.
 */
function crpking_public_scripts() {
    
    global $post;
    $post_id = $post->ID;


    // Load bootstrap style stylesheet.
    wp_enqueue_style( 'crpking-bootstrap-style', get_template_directory_uri() . '/css/bootstrap.css', array(), NULL );
	
    // Load font-awesome.min stylesheet.
    wp_enqueue_style( 'crpking-font-awesome-min-style', get_template_directory_uri() . '/css/font-awesome.min.css', array(), NULL );
	
    // Load sweetalert stylesheet.
    wp_enqueue_style( 'crpking-sweetalert-style', get_template_directory_uri() . '/css/sweetalert.css', array(), NULL );
        
    // Load our main stylesheet.
    wp_enqueue_style( 'crpking-style', get_stylesheet_uri(), array(), NULL );

    // Load main jquery
    wp_enqueue_script( 'jquery', array(), NULL );
        
    // Load bootstrap script
    wp_enqueue_script( 'crpking-bootstrap-script', get_template_directory_uri() . '/js/bootstrap.js', array(), NULL );
        
    // Load slick script
    wp_enqueue_script( 'crpking-slick-script', get_template_directory_uri() . '/js/slick.js', array(), NULL );
        
    // Load sweetalert script
    wp_enqueue_script( 'crpking-sweetalert-script', get_template_directory_uri() . '/js/sweetalert.min.js', array(), NULL );
        
    // Load public script
    wp_enqueue_script( 'crpking-public-script', get_template_directory_uri() . '/js/public-script.js', array(), NULL );
        
    // Load script
    wp_enqueue_script( 'crpking-script', get_template_directory_uri() . '/js/script.js', array(), time() );
    $localized_vars = array(
                        'url' =>  get_stylesheet_directory_uri().'/includes/',
                        'ajaxurl'   => admin_url( 'admin-ajax.php', ( is_ssl() ? 'https' : 'http' ) ),
                        'site_url' => home_url(),
    );
    if( $post_id == 53 || $post_id == 2270 || $post_id == 2289 || $post_id == 2697 || $post_id == 1701 || $post_id == 1210 || $post_id == 1320 || $post_id == 1175 ) {
        $localized_vars['temp'] = update_datepicker_timeslot();
    }
    wp_localize_script( 'crpking-script', 'CRPKING', $localized_vars);
    
}

//add action to load scripts and styles for the back end
add_action( 'admin_enqueue_scripts', 'crpking_admin_scripts' );

//add action load scripts and styles for the front end
add_action( 'wp_enqueue_scripts', 'crpking_public_scripts' );
?>