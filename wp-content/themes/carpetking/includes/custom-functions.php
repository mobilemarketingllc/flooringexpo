<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if( !defined( 'CRPKING_POST_POST_TYPE' ) ) {
    define( 'CRPKING_POST_POST_TYPE', 'post' );
}
if( !defined( 'CRPKING_PAGE_POST_TYPE' ) ) {
    define( 'CRPKING_PAGE_POST_TYPE', 'page' );
}
if( !defined( 'CRPKING_TESTIMONIAL_POST_TYPE' ) ) {
    define( 'CRPKING_TESTIMONIAL_POST_TYPE', 'crpking_testimonial' );
}
if( !defined( 'CRPKING_LOCATION_POST_TYPE' ) ) {
    define( 'CRPKING_LOCATION_POST_TYPE', 'crpking_location' );
}
if( !defined( 'CRPKING_PRODUCT_POST_TYPE' ) ) {
    define( 'CRPKING_PRODUCT_POST_TYPE', 'product' );
}
if( !defined( 'CRPKING_ORDER_POST_TYPE' ) ) {
    define( 'CRPKING_ORDER_POST_TYPE', 'orders' );
}
if( !defined( 'CRPKING_CONTACT_FORM_ENTRY_POST_TYPE' ) ) {
    define( 'CRPKING_CONTACT_FORM_ENTRY_POST_TYPE', 'cf7_entry' );
}
if( !defined( 'CRPKING_PRODUCT_POST_CAT' ) ) {
    define( 'CRPKING_PRODUCT_POST_CAT', 'product_cat' );
}
if( !defined( 'CRPKING_LOCATION_PAGE_ID' ) ) {
    define( 'CRPKING_LOCATION_PAGE_ID', '49' );
}
if( !defined( 'CRPKING_PRODUCT_PAGE_ID' ) ) {
    define( 'CRPKING_PRODUCT_PAGE_ID', '39' );
}
if( !defined( 'CRPKING_SCHEDULE_APPOINTMENT_PAGE_ID' ) ) {
    define( 'CRPKING_SCHEDULE_APPOINTMENT_PAGE_ID', '53' );
}
if( !defined( 'CRPKING_FINANCING_PAGE_ID' ) ) {
    define( 'CRPKING_FINANCING_PAGE_ID', '820' );
}
if( !defined( 'CRPKING_SCHEDULE_APPOINTMENT_FORM_ID' ) ) {
    define( 'CRPKING_SCHEDULE_APPOINTMENT_FORM_ID', '408' );
}
if( !defined( 'CRPKING_PAGE_SCHEDULE_APPOINTMENT_FORM_ID' ) ) {
    define( 'CRPKING_PAGE_SCHEDULE_APPOINTMENT_FORM_ID', '1703' );
}
if( !defined( 'CRPKING_SALES_PAGE_ID' ) ) {
    define( 'CRPKING_SALES_PAGE_ID', 998 );
}
if( !defined( 'CRPKING_META_PREFIX' ) ) {
    define( 'CRPKING_META_PREFIX', '_crpking_' );
}

// Include custom register required plugins
require get_stylesheet_directory() . '/lib/custom-register-required-plugins.php';
 
// Include custom post types & taxonomies
require get_stylesheet_directory() . '/includes/custom-posttypes.php';
 
// Include custom shortcodes
require get_stylesheet_directory() . '/includes/custom-shortcodes.php';

//include custom scripts file
require get_stylesheet_directory() . '/includes/custom-scripts.php';

//include custom ajax file
require get_stylesheet_directory() . '/includes/custom-ajax.php';

//include custom woocommerce file
require get_stylesheet_directory() . '/includes/custom-woocommerce.php';

//include custom widgets file
require get_stylesheet_directory() . '/widgets/class-custom-followus-widget.php';
require get_stylesheet_directory() . '/widgets/class-custom-product-megamenu-widget.php';
require get_stylesheet_directory() . '/widgets/class-custom-footer-services-widget.php';
require get_stylesheet_directory() . '/widgets/class-custom-footer-benefits-widget.php';
require get_stylesheet_directory() . '/widgets/class-custom-footer-subscribe-widget.php';
require get_stylesheet_directory() . '/widgets/class-custom-search-widget.php';
require get_stylesheet_directory() . '/widgets/class-custom-testimonial-widget.php';
require get_stylesheet_directory() . '/widgets/class-custom-special-offers-widget.php';

/**
* Escape Tags & Slashes
*
* Handles escapping the slashes and tags
*/
function crpking_escape_attr($data) {
    return !empty( $data ) ? esc_attr( stripslashes( $data ) ) : '';
}

/**
* Strip Slashes From Array
*/
function crpking_escape_slashes_deep($data = array(),$flag=true) {

    if($flag != true) {
         $data = crpking_nohtml_kses($data);
    }
    $data = stripslashes_deep($data);
    return $data;
}

/**
* Strip Html Tags 
* 
* It will sanitize text input (strip html tags, and escape characters)
*/
function crpking_nohtml_kses($data = array()) {

    if ( is_array($data) ) {
        $data = array_map(array($this,'crpking_nohtml_kses'), $data);
    } elseif ( is_string( $data ) ) {
        $data = wp_filter_nohtml_kses($data);
    }
   return $data;
}

/**
 * Display Short Content By Character
 */
function crpking_excerpt_char( $content, $length = 40, $read_more = '' ) {
    
    $text = '';
    if( !empty( $content ) ) {
        $text = strip_shortcodes( $content );
        $text = str_replace(']]>', ']]&gt;', $text);
        $text = strip_tags($text);
//        if( !empty( $read_more ) ) {
            $excerpt_more = apply_filters( 'excerpt_more', $read_more );
//        } else {
//            $excerpt_more = apply_filters( 'excerpt_more', ' ' . ' ...' );
//        }
        $text = substr($text, 0, $length);
        $text = $text . $read_more;
    }
    return $text;
}

/**
 * search in posts and pages
 */
function crpking_filter_search( $query ) {
    if( !is_admin() && $query->is_search ) {
        $query->set( 'post_type', array( CRPKING_POST_POST_TYPE, CRPKING_PAGE_POST_TYPE ) );
    }
    return $query;
}

/*
 * Custom Theme Options
 */
if( function_exists('acf_add_options_page') ) {
    
    // Theme General Settings
    $general_settings   = array(
                                'page_title' 	=> __( 'Theme General Settings', 'crpking' ),
                                'menu_title'	=> __( 'Theme Settings', 'crpking' ),
                                'menu_slug' 	=> 'theme-general-settings',
                                'capability'	=> 'edit_posts',
                                'redirect'	=> false
                            );
    acf_add_options_page( $general_settings );

    // Theme Social Settings
    $social_settings    = array(
                                    'page_title'    => __( 'Theme Social Settings', 'crpking' ),
                                    'menu_title'    => __( 'Social', 'crpking' ),
                                    'parent_slug'   => 'theme-general-settings',
                            );
    acf_add_options_sub_page( $social_settings );

    // Theme Footer Settings
    $footer_settings    = array(
                                    'page_title'    => __( 'Theme Footer Settings', 'crpking' ),
                                    'menu_title'    => __( 'Footer', 'crpking' ),
                                    'parent_slug'   => 'theme-general-settings',
                            );
    acf_add_options_sub_page( $footer_settings );
}

/*
 * Remove wp logo from admin bar
 */
function crpking_remove_wp_logo() {
    global $wp_admin_bar;
    
    if( crpking_check_acf_activation() ) {
        $wp_help  = get_field( 'crpking_options_wp_help', 'option' );
        if( empty( $wp_help ) ) {
            $wp_admin_bar->remove_menu('wp-logo');
        }
    }
}

/*
 * Custom login logo
 */
function crpking_custom_login_logo() {
    if( crpking_check_acf_activation() ) {
        $wp_login_logo  = get_field( 'crpking_options_admin_logo', 'option' );

        if( !empty( $wp_login_logo ) ) {
?>
        <style type="text/css">
            .login h1 a {
                background-image: url('<?php echo $wp_login_logo['url']; ?>') !important;
                background-size: 203px auto !important;
                height: 66px !important;
                width: 203px !important;
            }
        </style>
<?php
        }
    }
}

/*
 * Change custom login page url
 */
function crpking_loginpage_custom_link() {
    $site_url = esc_url( home_url( '/' ) );
    return $site_url;
}

/*
 * Change title on logo
 */
function crpking_change_title_on_logo() {
    $site_title = get_bloginfo( 'name' );
    return $site_title;
}

/*
 * check acf plugin is activated or not
 */
function crpking_check_acf_activation() {
    
    if( !function_exists( 'is_plugin_active' ) ) {
        include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
    }
    if( is_plugin_active( 'advanced-custom-fields-pro/acf.php' ) ){
        return true;
    }
    return false;
}

//add filter to search in posts and pages
//add_filter( 'pre_get_posts', 'crpking_filter_search' );

//add filter to add shortcode in widget
add_filter( 'widget_text', 'do_shortcode' );

//add action to modify the wp admin bar
add_action( 'wp_before_admin_bar_render', 'crpking_remove_wp_logo' );

//add action to change login logo
add_action( 'login_enqueue_scripts', 'crpking_custom_login_logo' );

//add filter to change custom login page url
add_filter( 'login_headerurl', 'crpking_loginpage_custom_link' );

//add filter to change title on logo
add_filter( 'login_headertitle', 'crpking_change_title_on_logo' );


/*
 * Function to remove Miles dropdown from Location search
 */
function hps_slp_change_ui_radius_selector ($HTML) { 
  
    $HTML =
            "<div id='addy_in_radius'>" .
            "<input type='hidden' id='radiusSelect' name='radiusSelect' value='200'>".
            "</div>";
    return $HTML; 
}
//add filter to search in posts and pages
add_filter( 'slp_change_ui_radius_selector', 'hps_slp_change_ui_radius_selector' );


function getLatLong($code){

    // Get JSON results from this request
    $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode( $code ).'&sensor=false');

    // Convert the JSON to an array
    $geo = json_decode($geo, true);
    
    $latitude = '';
    $longitude = '';
    
    if ($geo['status'] == 'OK') {
        // Get Lat & Long
        $latitude = $geo['results'][0]['geometry']['location']['lat'];
        $longitude = $geo['results'][0]['geometry']['location']['lng'];
    }
    
    $result = array(
                    'latitude'  => $latitude,
                    'longitude' => $longitude,
    );
    
    return $result;
}

/*
 * Custom Validations for Contact Form 7
 */
add_filter( 'wpcf7_validate_date', 'crpking_date_validation_filter', 10, 2 );
function crpking_date_validation_filter( $result, $tag ) {
    $tag = new WPCF7_FormTag( $tag );
    if( $tag->name == 'appointment_date' ) {
        $project_type = isset( $_REQUEST['appointment_project'][0] ) ? trim( $_REQUEST['appointment_project'][0] ) : '';
        if( !empty( $project_type ) ) {
            $project_date = isset( $_REQUEST['appointment_date'] ) ? trim( $_REQUEST['appointment_date'] ) : '';
            if( $project_type == 'Home' ) {
                if( empty( $project_date ) ) {
                    $result->invalidate( $tag, "Date cannot be empty." );
                }
            }
        }
    }
    if( $tag->name == 'app_date' ) {
        $project_type = isset( $_REQUEST['app_project'][0] ) ? trim( $_REQUEST['app_project'][0] ) : '';
        if( !empty( $project_type ) ) {
            $project_date = isset( $_REQUEST['app_date'] ) ? trim( $_REQUEST['app_date'] ) : '';
            if( $project_type == 'Home' ) {
                if( empty( $project_date ) ) {
                    $result->invalidate( $tag, "Date cannot be empty." );
                }
            }
        }
    }
    return $result;
}

add_filter( 'wpcf7_validate_dynamicselect', 'crpking_time_validation_filter', 10, 2 );
function crpking_time_validation_filter( $result, $tag ) {
    $tag = new WPCF7_FormTag( $tag );
    if( $tag->name == 'appointment_time' ) {
        $project_type = isset( $_REQUEST['appointment_project'][0] ) ? trim( $_REQUEST['appointment_project'][0] ) : '';
        if( !empty( $project_type ) ) {
            $project_time = isset( $_REQUEST['appointment_time'] ) ? trim( $_REQUEST['appointment_time'] ) : '';
            if( $project_type == 'Home' ) {
                if( empty( $project_time ) ) {
                    $result->invalidate( $tag, "Time cannot be empty." );
                }
            }
        }
    }
    if( $tag->name == 'app_time' ) {
        $project_type = isset( $_REQUEST['app_project'][0] ) ? trim( $_REQUEST['app_project'][0] ) : '';
        if( !empty( $project_type ) ) {
            $project_time = isset( $_REQUEST['app_time'] ) ? trim( $_REQUEST['app_time'] ) : '';
            if( $project_type == 'Home' ) {
                if( empty( $project_time ) ) {
                    $result->invalidate( $tag, "Time cannot be empty." );
                }
            }
        }
    }
    return $result;
}

add_filter( 'wpcf7_validate_dynamictext*', 'crpking_text_validation_filter', 10, 2 );
function crpking_text_validation_filter( $result, $tag ) {
    $tag = new WPCF7_FormTag( $tag );
    $zipcode = $distance = $cen_zip = '';
    if ( 'appointment_zipcode' == $tag->name ) {
        $zipcode = isset( $_POST['appointment_zipcode'] ) ? trim( $_POST['appointment_zipcode'] ) : '';
        if( !empty( $zipcode ) ) {
//            if(preg_match("/^([0-9]{5})/i",$zip_code)) {
            if(!preg_match('/^[0-9]{5}$/', $zipcode)) {
                $result->invalidate( $tag, "Please enter a valid zipcode." );
            } else {
                $answer = getLatLong( $zipcode );
                $lat1 = '';
                $lon1 = '';
                if( !empty( $answer ) ) {
                    $lat1 = $answer['latitude'];
                    $lon1 = $answer['longitude'];
                    
                    $distance = '';
                    if( crpking_check_acf_activation() ) {
                        $distance = get_field( 'crpking_appointment_distance', CRPKING_SCHEDULE_APPOINTMENT_PAGE_ID );
                        $cen_zip  = get_field( 'crpking__center_zipcode', CRPKING_SCHEDULE_APPOINTMENT_PAGE_ID );
                    }
                    $d = !empty( $distance ) ? $distance : 30;
                    $cen_zip = !empty( $cen_zip ) ? $cen_zip : 55411;
                    //earth's radius in miles
                    $r = 3959;
                    $difference = getDistanceBetweenZipcodes( $cen_zip, $zipcode );
                    if( !empty( $difference ) ) {
                        if( $difference > $d ) {
                            $result->invalidate( $tag, "<script>mytest();</script>");
                        }
                    }

                } else {
                    $result->invalidate( $tag, "Cannot retrive a valid location." );
                }
            }
        }
    }
    
    if ( 'app_zipcode' == $tag->name ) {
        
        $zipcode = isset( $_POST['app_zipcode'] ) ? trim( $_POST['app_zipcode'] ) : '';
        if( !empty( $zipcode ) ) {
//            if(preg_match("/^([0-9]{5})/i",$zip_code)) {
            if(!preg_match('/^[0-9]{5}$/', $zipcode)) {
                $result->invalidate( $tag, "Please enter a valid zipcode." );
            } else {
                $answer = getLatLong( $zipcode );
                $lat1 = '';
                $lon1 = '';
                if( !empty( $answer ) ) {
                    $lat1 = $answer['latitude'];
                    $lon1 = $answer['longitude'];
                    
                    $distance = '';
                    if( crpking_check_acf_activation() ) {
                        $distance = get_field( 'crpking_appointment_distance', CRPKING_SCHEDULE_APPOINTMENT_PAGE_ID );
                        $cen_zip  = get_field( 'crpking__center_zipcode', CRPKING_SCHEDULE_APPOINTMENT_PAGE_ID );
                    }
                    $d = !empty( $distance ) ? $distance : 30;
                    $cen_zip = !empty( $cen_zip ) ? $cen_zip : 55411;
                    //earth's radius in miles
                    $r = 3959;
                    $difference = getDistanceBetweenZipcodes( $cen_zip, $zipcode );
                    if( !empty( $difference ) ) {
                        if( $difference > $d ) {
                            $result->invalidate( $tag, "<script>form_zipcode_test();</script>");
                        }
                    }

                } else {
                    $result->invalidate( $tag, "Cannot retrive a valid location." );
                }
            }
        }
    }
    
    return $result;
}

function getDistanceBetweenZipcodes( $cen_zip, $zipcode ) {

    // Get JSON results from this request
    $geo = file_get_contents('https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=' . urlencode( $zipcode ) .'&destinations=' . urlencode( $cen_zip ) .'&key=AIzaSyB7Uq5o2oorc1o5VQqStXxryikBgk-i2NQ');
    // Convert the JSON to an array
    $geo = json_decode($geo, true);
    $distance = '';
    $result = '';
    if ($geo['status'] == 'OK') {
        // Get Distance between two zipcodes
        $distance = $geo['rows'][0]['elements'][0]['distance']['text'];
        if( !empty( $distance ) ) {
            $result = str_replace( " mi", "", $distance );
        }
    }
    return $result;
}

/*
 * Dynamically populate select field in contact form 7
 * 
 * Schedule Appointment Time Field
 */
function crpking_plugin_filter ( $tag, $unused ) {
    global $wpdb;
    $table = 'wp_timing';
    $choices = array();
    $check_data = '';
    $current_date = current_time('Y-m-d');
    $date = isset( $_POST['appointment_date'] ) ? trim( $_POST['appointment_date'] ) : $current_date;
    
    $previous_entries = new WP_Query( array(
                                            'post_type'         => CRPKING_CONTACT_FORM_ENTRY_POST_TYPE,
                                            'post_parent__in'   => array( CRPKING_SCHEDULE_APPOINTMENT_FORM_ID, CRPKING_PAGE_SCHEDULE_APPOINTMENT_FORM_ID),
                                            'posts_per_page'    => '-1',
                                            'fields'            => 'ids',
    ) );
    
    $entry_time_details = array();
    if( !empty( $previous_entries->posts ) ) {
        foreach( $previous_entries->posts as $key => $value ) {
            $temp_details = get_post_meta( $value, 'form_fields', false );
            $entry_time_details[$value]['appointment_date'] = '';
            $entry_time_details[$value]['appointment_time'] = '';
            
            if( isset($temp_details[0]['appointment_date']) && strtotime( $temp_details[0]['appointment_date'] ) == strtotime( $date ) ) {
                if(isset($temp_details[0]['appointment_date'])) {
                    $entry_time_details[$value]['appointment_date'] = $temp_details[0]['appointment_date'];
                }
                
                if(isset($temp_details[0]['appointment_time'])) {
                    $entry_time_details[$value]['appointment_time'] = $temp_details[0]['appointment_time'];
                }
            }
        }
    }

    if( !empty( $date ) ) {
        $check_data .= " for_date = '" . $date . "'";
        $query = 'SELECT * FROM ' . $table . ' where ' . $check_data . ' ORDER BY start_meridian asc, start_time asc, end_time asc';
        $timings = $wpdb->get_results($query);
        if( !empty( $timings ) ) {
            foreach( $timings as $key => $value ) {
                $start_time = $value->start_time;
                $start_meri = $value->start_meridian;
                $end_time   = $value->end_time;
                $end_meri   = $value->end_meridian;
                $slots      = $value->slots;
                $fmt_value  = date("H:i:s", strtotime( $start_time . ' ' . $start_meri ));
                $end_value  = date("H:i:s", strtotime( $end_time . ' ' . $end_meri ));
                $flag       = 0;
                foreach( $entry_time_details as $key => $value ) {
                    if( strtotime( $value['appointment_time'] ) == strtotime( $fmt_value ) ) {
                        $flag++;
                    }
                }
                if( $flag < $slots ) {
                    $choices[ $start_time . ' ' . $start_meri . ' - ' . $end_time . ' ' . $end_meri ] = array(
                                                                    'start_time' => $fmt_value,
                                                                    'end_time'   => $end_value,
                    );
                }
            }
        }
    }
    return $choices;
}
add_filter('crpking_appointment_time', 'crpking_plugin_filter', 10, 2);

/*
 * Filter for disabling plugin update notification for VC
 */
function crpking_disable_plugin_updates( $value ) {
    unset( $value->response['js_composer/js_composer.php'] );
    return $value;
}
add_filter( 'site_transient_update_plugins', 'crpking_disable_plugin_updates' );

function update_datepicker_timeslot() {
    
    /* 27/06 */
    global $wpdb;
    $table = 'wp_timing';
    $query = 'SELECT * FROM ' . $table .  ' ORDER BY start_meridian asc, start_time asc, end_time asc';
    $timings = $wpdb->get_results($query, ARRAY_A);
    $start_time_date = array();
    
    $filtered_for_date = array_filter( $timings, function( $value ) {
        $today_date = date('Y-m-d');
	if( strtotime($today_date) < strtotime( $value['for_date'] ) ) {
		return $value;
	}
    });
    
    if( count( $filtered_for_date ) ) {
        foreach( $filtered_for_date as $key => $value ) {
            $c_date         = $value['for_date'];
            $check_date     = date('j-n-Y',  strtotime($c_date));
            if( !in_array( $check_date, $start_time_date ) ) {
                $start_time     = $value['start_time'];
                $start_meri     = $value['start_meridian'];
                $slots          = $value['slots'];
                $fmt_value      = date("H:i:s", strtotime( $start_time . ' ' . $start_meri ));
                $slot_count     = 0;
                $fmt_data       = date( 'm/d/Y',  strtotime( $c_date ) );

                $previous_entries = new WP_Query( array(
                                            'post_type'         => CRPKING_CONTACT_FORM_ENTRY_POST_TYPE,
                                            'post_parent__in'   => array( CRPKING_SCHEDULE_APPOINTMENT_FORM_ID, CRPKING_PAGE_SCHEDULE_APPOINTMENT_FORM_ID),
                                            'posts_per_page'    => '-1',
                                            'fields'            => 'ids',
                                            'meta_query'        => array(
                                                                array(
                                                                    'key'       => 'form_fields',
                                                                    'value'     => $fmt_data,
                                                                    'compare'   => 'LIKE',
                                                                ),
                                                                array(
                                                                    'key'       => 'form_fields',
                                                                    'value'     => $fmt_value,
                                                                    'compare'   => 'LIKE',
                                                                )
                                            ),
                ) );
                if( $previous_entries->have_posts() ) {
                    $slot_count = count( $previous_entries->posts );
                }
                if( $slot_count < $slots ) {
                    $start_time_date[] = date('j-n-Y',  strtotime($c_date));
                }
            }
        }
    }    
    return $start_time_date;
    
    
    
    /*global $wpdb;
    $table = 'wp_timing';
    $query = 'SELECT * FROM ' . $table .  ' ORDER BY start_meridian asc, start_time asc, end_time asc';
    $timings = $wpdb->get_results($query);

    $previous_entries = new WP_Query( array(
                                                'post_type'         => CRPKING_CONTACT_FORM_ENTRY_POST_TYPE,
                                                'post_parent__in'   => array( CRPKING_SCHEDULE_APPOINTMENT_FORM_ID, CRPKING_PAGE_SCHEDULE_APPOINTMENT_FORM_ID),
                                                'posts_per_page'    => '-1',
                                                'fields'            => 'ids',
    ) );
    
    
        $entry_time_details = array();
        if( !empty( $previous_entries->posts ) ) {
            foreach( $previous_entries->posts as $key => $value ) {
                $temp_details = get_post_meta( $value, 'form_fields', false );
                if(isset($temp_details[0]['appointment_date'])) {
                    $entry_time_details[$value]['appointment_date'] = $temp_details[0]['appointment_date'];
                } else {
                    $entry_time_details[$value]['appointment_date'] = "";
                }
                
                if(isset($temp_details[0]['appointment_time'])) {
                    $entry_time_details[$value]['appointment_time'] = $temp_details[0]['appointment_time'];
                } else {
                    $entry_time_details[$value]['appointment_time'] = "";
                }
            }
        }
        

       
        $current_date = date('j-n-Y');
        $time_json  = json_encode($timings);
        $time_array = json_decode($time_json, true);
        $time_date = array_column($time_array, 'for_date');
        if( !empty( $timings ) ) {
            
            foreach( $timings as $key => $value ) {

                $c_date = $value->for_date;
                $start_time = $value->start_time;
                $start_meri = $value->start_meridian;
                $slots      = $value->slots;
                $fmt_value  = date("H:i:s", strtotime( $start_time . ' ' . $start_meri ));
                $appointed_slot       = 0;
                $tem_data = date('Y-m-d',  strtotime($c_date));
                foreach( $entry_time_details as $key => $appointment_value ) {
                    
                    if( strtotime( $appointment_value['appointment_time'] ) == strtotime( $fmt_value ) && (strtotime( $appointment_value['appointment_date'] ) == strtotime( $c_date ))) {
                        $appointed_slot++;
                    }
                }

                
                if( $appointed_slot < $slots ) {
                    $start_time_date[] = date('j-n-Y',  strtotime($c_date));
                }

            }
            return $start_time_date;
        }*/
}

/*
 *   Define the wp_redirect callback for sales page
 */
function crpking_sales_page_wp_redirect( $location, $status ) { 
        global $post;
        $post_id    = $post->ID;
        $sale_pg_id = CRPKING_SALES_PAGE_ID;
        if( $post_id == $sale_pg_id ){
            $location = 'https://accounts.zoho.com/login?servicename=ZohoCreator&serviceurl=https://creator.zoho.com%2Fjsp%2Fshowapp.jsp%3Fappurl%3D%2Fcarpetking%2Fcontacts%2Fview-perma%2FSales_Dashboardfinal%2F&hide_signup=true&css=https://creator.zoho.com/css/newlogin.css&load_ifr=true';
        }
        return $location; 
}; 
         
// add the wp_redirect filter 
add_filter( 'wp_redirect', 'crpking_sales_page_wp_redirect', 10, 2 ); 

//function custom_validation( $result ) {
//   
//    if ( $result['is_valid'] ) {
//        $survey_code = rgpost( 'input_1_1' );
////        if( preg_match("/^[a-zA-Z ]*$/", $survey_code)){
////            $result['is_valid'] = false;
////            $result['message']  = 'Please Enter Only numeric values.';
////        }
//        
//        foreach( $form['fields'] as &$field ) {
//
//            //NOTE: replace 1 with the field you would like to validate
//            if ( $field->id == '1' ) {
//                if(!preg_match('/^[0-9]{6}$/', $survey_code)) {
//                    $result['is_valid'] = false;
//                    $result['message']  = 'Please Enter Minimum 6 numeric values.';
//        }
//                $field->failed_validation = true;
//                $field->validation_message = 'This field is invalid!';
//                break;
//            }
//        }
//    }
//    
//    return $result;
//
//}
//add_filter( 'gform_validation_1', 'custom_validation' );

add_filter( 'gform_field_validation_1_1', 'crptking_survey_code_custom_validation', 10,4);
function crptking_survey_code_custom_validation( $result, $value, $form, $field ) {
//    $master = rgpost( 'input_1' );
    if ( !preg_match('/^[0-9]{6}$/', $value) ) {
        $result['is_valid'] = false;
        $result['message']  = 'Please Enter Only 6 Digit.';
    }

    return $result;
}

function my_admin_menu() {
    add_menu_page (
            __ ( 'Order Import (CSV)', 'carpetking' ), __ ( 'Order Import', 'carpetking' ), 'manage_options', 'order_import', 'carpetking_order_import_page', 'dashicons-upload', 32
    );
}

add_action ( 'admin_menu', 'my_admin_menu' );

function carpetking_order_import_page() {
    ?>
    <div class="container">
        <h3>Order Status Import</h3>
        <a href="#" id="img-upload">Add CSV</a>
        <form id="order_csv_import_form" method="post">
            <input type="text" id="csv_url" name="csv_url" value="" required readonly><br/>
            <input type="checkbox" id="delete_existing" name="delete_existing" value="delete">
            <label for="delete_existing"> Delete All Existing Orders</label><br>
            <input type="submit" value="Upload" />
        </form>
        <div class="form_loader_order" style="display: none;"></div>
    </div>
    <?php
}
?>