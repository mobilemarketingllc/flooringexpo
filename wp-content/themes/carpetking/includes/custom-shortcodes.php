<?php

/**
 * Register [crpking-home-products] shortcode
 */
function crpking_home_products($atts) {
    global $post;

    extract( shortcode_atts( array (
        'per_page' => '-1'
                    ), $atts ) );

    ob_start();

    $args = array (
        'number'     => $number,
        'orderby'    => $orderby,
        'order'      => $order,
        'hide_empty' => $hide_empty,
        'include'    => $ids
    );

    $product_categories = get_terms( CRPKING_PRODUCT_POST_CAT, $args );
    $home_terms         = '';
    $top_terms          = '';
    $bottom_terms       = '';
    if ( ! empty( $product_categories ) ) {
        foreach ( $product_categories as $key => $value ) {
            $display_home = get_field( 'crpking_cat_display_home', CRPKING_PRODUCT_POST_CAT . '_' . $value->term_id );
            if ( $display_home == 1 ) {
                $home_terms[] = $value;
            }
        }
        if ( ! empty( $home_terms ) ) {
            foreach ( $home_terms as $key => $value ) {
                $display_top = get_field( 'crpking_cat_display_top', CRPKING_PRODUCT_POST_CAT . '_' . $value->term_id );
                if ( $display_top == 1 ) {
                    $top_terms[] = $value;
                } else {
                    $bottom_terms[] = $value;
                }
            }
        }
        ?>
        <section class="home-product-sec">
            <?php if ( ! empty( $top_terms ) || ! empty( $bottom_terms ) ) { ?>
                <h2><?php _e( 'Our Products', 'crpking' ); ?></h2>
            <?php } ?>
            <?php
            if ( ! empty( $top_terms ) ) {
                foreach ( $top_terms as $key => $value ) {
                    $bg_img = get_field( 'crpking_cat_bg_image', CRPKING_PRODUCT_POST_CAT . '_' . $value->term_id );
                    if ( ! empty( $bg_img ) ) {
                        $bg_img_src = is_array( $bg_img ) ? $bg_img[ 'url' ] : '';
                        $bg_img_alt = is_array( $bg_img ) ? $bg_img[ 'alt' ] : $bg_img[ 'name' ];
                        ?>
                        <div class="pro-2col-box bg_img" style="background:url('<?php echo $bg_img_src; ?>') no-repeat; ">
                            <span><a href="<?php echo get_the_permalink( CRPKING_PRODUCT_PAGE_ID ); ?>"><?php echo $value->name; ?></a></span>
                        </div>
                        <?php
                    }
                }
            }
            ?>
            <?php
            if ( ! empty( $bottom_terms ) ) {
                foreach ( $bottom_terms as $key => $value ) {
                    $bg_img = get_field( 'crpking_cat_bg_image', CRPKING_PRODUCT_POST_CAT . '_' . $value->term_id );
                    if ( ! empty( $bg_img ) ) {
                        $bg_img_src = is_array( $bg_img ) ? $bg_img[ 'url' ] : '';
                        $bg_img_alt = is_array( $bg_img ) ? $bg_img[ 'alt' ] : $bg_img[ 'name' ];
                        ?>
                        <div class="pro-4col-box bg_img" style="background:url('<?php echo $bg_img_src; ?>') no-repeat; ">
                            <span><a href="<?php echo get_the_permalink( CRPKING_PRODUCT_PAGE_ID ); ?>"><?php echo $value->name; ?></a></span>
                        </div>
                        <?php
                    }
                }
            }
            ?>
        </section>
        <?php
    }


    $html = ob_get_clean();
    return $html;
}

/**
 * Register [crpking-ajax-products] shortcode
 */
function crpking_ajax_products($atts) {
    global $post;

    extract( shortcode_atts( array (
        'per_page'   => '-1',
        'ajax_query' => ''
                    ), $atts ) );

    ob_start();
    echo "<pre>";
    print_r( $ajax_query );
    echo "</pre>";

    if ( ! empty( $ajax_query ) ) :
        ?>

        <?php if ( $ajax_query->have_posts() ) : ?>

            <?php
            /**
             * woocommerce_before_shop_loop hook.
             *
             * @hooked woocommerce_result_count - 20
             * @hooked woocommerce_catalog_ordering - 30
             */
            do_action( 'woocommerce_before_shop_loop' );
            ?>

            <?php woocommerce_product_loop_start(); ?>

            <?php woocommerce_product_subcategories(); ?>

            <?php while ( $ajax_query->have_posts() ) : $ajax_query->the_post(); ?>

                <?php wc_get_template_part( 'content', 'product' ); ?>

            <?php endwhile; // end of the loop.  ?>

            <?php woocommerce_product_loop_end(); ?>

            <?php
            /**
             * woocommerce_after_shop_loop hook.
             *
             * @hooked woocommerce_pagination - 10
             */
            do_action( 'woocommerce_after_shop_loop' );
            ?>

        <?php elseif ( ! woocommerce_product_subcategories( array ( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

            <?php wc_get_template( 'loop/no-products-found.php' ); ?>

        <?php endif; ?>

        <?php
    endif;

    $html = ob_get_clean();
    return $html;
}

/**
 * Register [crpking-home-search-zip] shortcode
 */
function crpking_home_search_zip($atts) {
    global $post;

    extract( shortcode_atts( array (
        'per_page'   => '-1',
        'ajax_query' => ''
                    ), $atts ) );

    ob_start();
    ?>

    <div class="search_location">
        <form action="<?php echo get_the_permalink( CRPKING_SCHEDULE_APPOINTMENT_PAGE_ID ); ?>" method="post" class="home-zipcode-form">
            <input type="text" name="home_zipcode" id="home_zipcode" placeholder="Enter Your Zip Code Here">
            <input type="submit" name="home_submit" id="home_submit" placeholder="" value="Submit">
            <!--<span class="home_search" id="home_search"><i class="fa fa-arrow-right" aria-hidden="true"></i></span>-->
        </form>
        <div class="zipcode-form-loader" style="display:none;">
            <img src="<?php echo get_template_directory_uri() . '/images/loader.gif' ?>">
        </div>
    </div>

    <?php
    $html = ob_get_clean();
    return $html;
}

/**
 * Register [crpking-locations] shortcode
 */
function crpking_locations($atts) {
    global $post;
    $page_id = $post->ID;

    extract( shortcode_atts( array (
        'per_page'   => '-1',
        'ajax_query' => ''
                    ), $atts ) );

    ob_start();

    if ( ! empty( $_REQUEST ) ) {
        $zipcode = $_REQUEST[ 'home_zipcode' ];
        if ( ! empty( $zipcode ) ) {
            if ( preg_match( "/^([0-9]{5})/i", $zip_code ) ) {
                $loc_query = 'invalid';
            } else {
                $answer = getLatLong( $zipcode );
                $lat1   = '';
                $lon1   = '';
                if ( ! empty( $answer ) ) {
                    $lat1 = $answer[ 'latitude' ];
                    $lon1 = $answer[ 'longitude' ];

                    $distance = '';
                    if ( crpking_check_acf_activation ) {
                        $distance = get_field( 'crpking_loc_distance', $page_id );
                    }
                    $d = ! empty( $distance ) ? $distance : 7;
                    //earth's radius in miles
                    $r = 3959;

                    //compute max and min latitudes / longitudes for search square
                    $latN = rad2deg( asin( sin( deg2rad( $lat1 ) ) * cos( $d / $r ) + cos( deg2rad( $lat1 ) ) * sin( $d / $r ) * cos( deg2rad( 0 ) ) ) );
                    $latS = rad2deg( asin( sin( deg2rad( $lat1 ) ) * cos( $d / $r ) + cos( deg2rad( $lat1 ) ) * sin( $d / $r ) * cos( deg2rad( 180 ) ) ) );
                    $lonE = rad2deg( deg2rad( $lon1 ) + atan2( sin( deg2rad( 90 ) ) * sin( $d / $r ) * cos( deg2rad( $lat1 ) ), cos( $d / $r ) - sin( deg2rad( $lat1 ) ) * sin( deg2rad( $latN ) ) ) );
                    $lonW = rad2deg( deg2rad( $lon1 ) + atan2( sin( deg2rad( 270 ) ) * sin( $d / $r ) * cos( deg2rad( $lat1 ) ), cos( $d / $r ) - sin( deg2rad( $lat1 ) ) * sin( deg2rad( $latN ) ) ) );

                    $args      = array (
                        'post_type'  => 'crpking_location',
                        'meta_query' => array (
                            'relation' => 'AND',
                            array (
                                'key'     => 'crpking_loc_latitude',
                                'value'   => array ( $latS, $latN ),
                                'compare' => 'BETWEEN',
                            ),
                            array (
                                'key'     => 'crpking_loc_longitude',
                                'value'   => array ( $lonE, $lonW ),
                                'compare' => 'BETWEEN',
                            ),
                        ),
                    );
                    $loc_query = new WP_Query( $args );
                }
            }
        }
    }

    if ( empty( $loc_query ) ) {
        $loc_args  = array (
            'post_type'      => CRPKING_LOCATION_POST_TYPE,
            'posts_per_page' => '-1',
            'orderby'        => 'date',
            'order'          => 'ASC',
        );
        $loc_query = new WP_Query( $loc_args );
    }

    $name_text       = get_field( 'crpking_loc_name_text', $page_id );
    $address_text    = get_field( 'crpking_loc_address_text', $page_id );
    $phone_text      = get_field( 'crpking_loc_phone_text', $page_id );
    $directions_text = get_field( 'crpking_loc_directions_text', $page_id );
    $details_text    = get_field( 'crpking_loc_details_text', $page_id );

    if ( $loc_query != 'invalid' ) {
        if ( $loc_query->have_posts() ) {
            ?>
            <ul  class="loc-title">
                <li><?php echo $name_text; ?></li>
                <li><?php echo $address_text; ?></li>
                <li><?php echo $phone_text; ?></li>
                <li><?php echo $directions_text; ?></li>
            </ul>
            <div id="location_list">
                <?php
                while ( $loc_query->have_posts() ) {
                    $loc_query->the_post();
                    ?>
                    <?php
                    $post_id  = get_the_ID();
                    $address  = get_field( 'crpking_loc_address', $post_id );
                    $city     = get_field( 'crpking_loc_city', $post_id );
                    $city     = ! empty( $city ) ? $city : '';
                    $state    = get_field( 'crpking_loc_state', $post_id );
                    $state    = ! empty( $state ) ? $state : '';
                    $zipcode  = get_field( 'crpking_loc_zipcode', $post_id );
                    $zipcode  = ! empty( $zipcode ) ? $zipcode : '';
                    $extracon = get_field( 'crpking_extra_content', $post_id );
                    $extracon = ! empty( $extracon ) ? $extracon : '';
                    $phone    = get_field( 'crpking_loc_phone', $post_id );
                    $title    = get_the_title();
                    if ( ! empty( $title ) && ! empty( $address ) ) {
                        ?>
                        <div class="loc-cont-sec">
                            <ul class="loc-txt">
                                <li><strong><?php echo $title; ?></strong></li>
                                <li>
                                    <?php echo $address; ?><br>
                                    <?php
                                    echo $city;
                                    if ( ! empty( $city ) && ! empty( $state ) ) {
                                        _e( ', ', 'crpking' );
                                    }
                                    echo $state . ' ' . $zipcode;
                                    ?><br>
                                    <?php
                                    if ( ! empty( $extracon ) ) {
                                        echo __( '(', 'crpking' ) . $extracon . __( ')', 'crpking' );
                                    }
                                    ?>
                                </li>
                                <li>
                                    <?php if ( ! empty( $phone ) ) { ?>
                                        <a href="<?php echo __( 'tel:', 'crpking' ) . $phone; ?>"><?php echo $phone; ?></a>
                                    <?php } ?>
                                </li>
                                <li><a href="<?php echo get_the_permalink(); ?>"><?php echo $details_text; ?></a></li>
                            </ul>
                        </div>
                    <?php } ?>
                <?php } wp_reset_query(); ?>
            </div>
            <?php
        } else {
            ?>
            <div class="loc_no_results"><?php _e( 'No results found!', 'crpking' ); ?></div>
            <?php
        }
    } else {
        ?>
        <div class="loc_no_results"><?php _e( 'Please enter a valid zipcode!', 'crpking' ); ?></div>
        <?php
    }

    $html = ob_get_clean();
    return $html;
}

/**
 * Register [crpking_location_advice] shortcode
 */
//add shortcode to display location advices
add_shortcode( 'crpking_location_advice', 'crpking_location_advice_details' );

function crpking_location_advice_details($atts) {
    if ( class_exists( 'acf' ) ) {
        $crpking_loc_advice_sec_rep = get_field( 'crpking_loc_advice_sec_rep' );
    }
    ob_start();
    ?>
    <div class="location-inner-info-sec">
        <?php if ( ! empty( $crpking_loc_advice_sec_rep ) ) {
            ?>
            <div class="container">
                <?php
                foreach ( $crpking_loc_advice_sec_rep as $crpking_loc_advice_sec_key => $crpking_loc_advice_sec_value ) {
//                    $advice_leftright_img = $crpking_loc_advice_sec_value[ 'advice_leftright' ];
                    $advice_image   = $crpking_loc_advice_sec_value[ 'advice_image' ];
                    $advice_title   = $crpking_loc_advice_sec_value[ 'advice_title' ];
                    $advice_cta     = $crpking_loc_advice_sec_value[ 'advice_cta' ];
                    $advice_content = $crpking_loc_advice_sec_value[ 'advice_content' ];
                    ?>
                    <div class="row">
                        <div class="col">
                            <?php if ( ! empty( $advice_image ) ) {
                                ?>
                                <div class="img-box">
                                    <img src="<?php echo $advice_image[ 'url' ]; ?>" alt="<?php echo $advice_image[ 'alt' ]; ?>">
                                </div>
                            <?php }
                            ?>
                        </div>
                        <div class="col">
                            <?php if ( ! empty( $advice_title ) || ! empty( $advice_content ) || ! empty( $advice_cta ) ) {
                                ?>
                                <div class="content-wrap">
                                    <h5><?php echo $advice_title; ?></h5>
                                    <?php echo $advice_content; ?>
                                    <a href="<?php echo $advice_cta[ 'url' ]; ?>"><?php echo $advice_cta[ 'title' ]; ?></a>
                                </div>
                            <?php }
                            ?>
                        </div>
                    </div>
                <?php }
                ?>
            </div>
        <?php }
        ?>
    </div>
    <?php
    $html = ob_get_clean();
    return $html;
}

/**
 * Register [crpking-location-details] shortcode
 */
function crpking_location_details($atts) {
    global $post;
    $page_id = $post->ID;

    extract( shortcode_atts( array (
        'per_page'   => '-1',
        'ajax_query' => ''
                    ), $atts ) );

    ob_start();

    $address       = get_field( 'crpking_loc_address', $post_id );
    $city          = get_field( 'crpking_loc_city', $post_id );
    $city          = ! empty( $city ) ? $city : '';
    $state         = get_field( 'crpking_loc_state', $post_id );
    $state         = ! empty( $state ) ? $state : '';
    $zipcode       = get_field( 'crpking_loc_zipcode', $post_id );
    $zipcode       = ! empty( $zipcode ) ? $zipcode : '';
    $extracon      = get_field( 'crpking_extra_content', $post_id );
    $extracon      = ! empty( $extracon ) ? $extracon : '';
    $phone         = get_field( 'crpking_loc_phone', $post_id );
    $email         = get_field( 'crpking_loc_email', $post_id );
    $loc_direction = get_field( 'crpking_loc_direction', $post_id );

    $hours_title = get_field( 'crpking_loc_hours_title', $post_id );
    $hours_rep   = get_field( 'crpking_loc_hours_rep', $post_id );

    $contact_rep = get_field( 'crpking_loc_contact_rep', $post_id );

    $title = get_the_title();

    if ( ! empty( $title ) && ! empty( $address ) ) {
        ?>
        <div class="left-content">
            <div class="address-wrap"><!-- maple-address -->
                <h6><?php echo $title; ?></h6>
                <p>
                    <?php echo $address; ?><br>
                    <?php
                    echo $city;
                    if ( ! empty( $city ) && ! empty( $state ) ) {
                        _e( ', ', 'crpking' );
                    }
                    echo $state . ' ' . $zipcode;
                    ?><br>
                    <?php
                    if ( ! empty( $extracon ) ) {
                        echo __( '(', 'crpking' ) . $extracon . __( ')', 'crpking' );
                    }
                    ?>
                </p>
                <?php if ( ! empty( $phone ) || ! empty( $email ) ) { ?>
                    <ul>
                        <li><a href="<?php echo __( 'mailto:', 'crpking' ) . $email; ?>"><?php echo $email; ?></a></li>
                        <li><a href="<?php echo __( 'tel:', 'crpking' ) . $phone; ?>"><?php echo $phone; ?></a></li>
                    </ul>
                <?php } ?>
            </div>
            <div class="hors-button-wrapper">
                <div class="hours-details-wrap">
                    <?php if ( ! empty( $hours_title ) ) {
                        ?>
                        <p><?php echo $hours_title; ?></p>
                        <?php
                    }
                    if ( ! empty( $hours_rep ) ) {
                        ?>
                        <ul>
                            <?php
                            foreach ( $hours_rep as $hours_rep_key => $hours_rep_value ) {
                                $hours_days = $hours_rep_value[ 'hours_days' ];
                                $hours_time = $hours_rep_value[ 'hours_time' ];
                                ?>
                                <li><?php echo $hours_days, ' : ' ?><?php echo $hours_time; ?></li>
                            <?php }
                            ?>
                        </ul>
                    <?php }
                    ?>
                </div>
                <?php if ( ! empty( $contact_rep ) ) {
                    ?>
                    <div class="contact-btns">
                        <?php if ( ! empty( $email ) || ! empty( $phone ) || ! empty( $loc_direction ) ) { ?>
                            <ul>
                                <?php if ( ! empty( $email ) ) {
                                    ?>
                                    <li><a href="<?php echo __( 'mailto:', 'crpking' ) . $email; ?>"><?php echo __( 'Email', 'crpking' ); ?></a></li>
                                    <?php
                                } if ( ! empty( $phone ) ) {
                                    ?>
                                    <li><a href="<?php echo __( 'tel:', 'crpking' ) . $phone; ?>"><?php echo __( 'Call', 'crpking' ); ?></a></li>
                                    <?php
                                } if ( ! empty( $loc_direction ) ) {
                                    ?>
                                    <li><a href="<?php echo $loc_direction; ?>" target="_blank"><?php echo __( 'Get Direction', 'crpking' ); ?></a></li>
                                <?php }
                                ?>
                            </ul>
                        <?php } ?>
                    </div>
                <?php }
                ?>
            </div>
        </div>
        <?php
    }
    $html = ob_get_clean();
    return $html;
}

/**
 * Register [crpking-appointment-confirmed] shortcode
 */
function crpking_appointment_confirmed($atts) {
    global $post;
    $page_id = $post->ID;

    extract( shortcode_atts( array (
        'per_page'   => '-1',
        'ajax_query' => ''
                    ), $atts ) );

    ob_start();

    if ( ! empty( $_REQUEST ) ) {

        $pro_date            = $_REQUEST[ 'pro_date' ];
        $pro_time            = date( 'h a', strtotime( $_REQUEST[ 'pro_time' ] ) );
        $pro_end_time        = date( 'h a', strtotime( $_REQUEST[ 'pro_end_time' ] ) );
        $pro_type            = $_REQUEST[ 'pro_type' ];
        $pro_floors          = $_REQUEST[ 'pro_floors' ];
        $pro_rooms           = $_REQUEST[ 'pro_rooms' ];
        $pro_firstname       = $_REQUEST[ 'pro_firstname' ];
        $pro_lastname        = $_REQUEST[ 'pro_lastname' ];
        $pro_address         = $_REQUEST[ 'pro_address' ];
        $pro_city            = $_REQUEST[ 'pro_city' ];
        $pro_state           = $_REQUEST[ 'pro_state' ];
        $pro_zipcode         = $_REQUEST[ 'pro_zipcode' ];
        $pro_email           = $_REQUEST[ 'pro_email' ];
        $pro_phone           = $_REQUEST[ 'pro_phone' ];
        $pro_time_after_twoh = date( 'H:i:s', strtotime( $pro_time . '+2 hour' ) );
        $format_date         = date( "m-d-Y", strtotime( $pro_date ) );
        ?>

        <?php if ( ! empty( $pro_date ) && ! empty( $pro_time ) ) { ?>
            <div class="thank-box-row">
                <h4><?php _e( 'Scheduled For', 'crpking' ); ?></h4>
                <div class="than-box">
                    <span><strong><?php _e( 'Date :', 'crpking' ); ?></strong><?php echo $format_date; ?></span>
                    <span><strong><?php _e( 'Time :', 'crpking' ); ?></strong><?php
                        echo $pro_time;
                        echo ! empty( $pro_end_time ) ? __( ' - ', 'crpking' ) . $pro_end_time : '';
                        ?></span>
                    <p><?php _e( 'We will arrive within a two-hour window', 'crpking' ); ?></p>
                </div>
            </div>
        <?php } ?>
        <div class="thank-box-row">
            <h4><?php _e( 'Your Project', 'crpking' ); ?></h4>
            <div class="than-box">
                <span><strong><?php _e( 'Project Type:', 'crpking' ); ?></strong><?php echo $pro_type; ?></span>
                <span><strong><?php _e( 'Flooring Types :', 'crpking' ); ?></strong><?php echo $pro_floors; ?></span>
                <?php if ( ! empty( $pro_rooms ) ) { ?>
                    <span><strong><?php _e( 'Rooms Selected :', 'crpking' ); ?></strong><?php echo $pro_rooms; ?></span>
                <?php } ?>
            </div>
        </div>
        <div class="thank-box-row">
            <h4><?php _e( 'Your Contact Info', 'crpking' ); ?></h4>
            <div class="than-box">
                <span><strong><?php _e( 'First & Last Name :', 'crpking' ); ?></strong><?php echo $pro_firstname . ' ' . $pro_lastname; ?></span>
                <span><strong><?php _e( 'Street Address:', 'crpking' ); ?></strong><?php echo $pro_address; ?></span>
                <span><strong><?php _e( 'City:', 'crpking' ); ?></strong><?php echo $pro_city; ?></span>
                <span><strong><?php _e( 'Zip code:', 'crpking' ); ?></strong><?php echo $pro_zipcode; ?></span>
                <span><strong><?php _e( 'Email Address:', 'crpking' ); ?></strong><?php echo $pro_email; ?></span>
                <span><strong><?php _e( 'Main Phone:', 'crpking' ); ?></strong><?php echo $pro_phone; ?></span>
            </div>
        </div>

        <?php
    }

    $html = ob_get_clean();
    return $html;
}

/**
 * Register [crpking-common-links] shortcode
 */
function crpking_common_links($atts) {
    global $post;

    extract( shortcode_atts( array (
        'per_page'   => '-1',
        'ajax_query' => ''
                    ), $atts ) );

    ob_start();

    $links = get_field( 'crpking_options_links', 'option' );
    if ( ! empty( $links ) ) {
        ?>
        <section>
            <div class="row">
                <?php foreach ( $links as $key => $value ) { ?>
                    <?php
                    $sch_icon     = $value[ 'crpking_options_link_icon' ];
                    $sch_icon_src = ! empty( $sch_icon ) ? $sch_icon[ 'url' ] : '';
                    $sch_icon_alt = ! empty( $sch_icon ) ? $sch_icon[ 'alt' ] : '';
                    $sch_icon_alt = ! empty( $sch_icon_alt ) ? $sch_icon_alt : $sch_icon[ 'name' ];
                    $sch_text     = $value[ 'crpking_options_link_text' ];
                    $sch_content  = $value[ 'crpking_options_link_content' ];
                    $sch_url      = $value[ 'crpking_options_link_url' ];
                    $sch_url      = ! empty( $sch_url ) ? $sch_url : '#';
                    if ( ! empty( $sch_text ) || ! empty( $sch_content ) ) {
                        ?>
                        <div class="col-md-3 home-schedul-box-wrap">
                            <a href="<?php echo $sch_url; ?>" class="home-schedul-box">
                                <?php if ( ! empty( $sch_icon_src ) ) { ?>
                                    <div class="home-schedule-img">
                                        <img src="<?php echo $sch_icon_src; ?>" alt="<?php echo $sch_icon_alt; ?>" title="<?php echo $sch_icon[ 'name' ]; ?>">
                                    </div>
                                <?php } ?>
                                <?php if ( ! empty( $sch_text ) ) { ?>
                                    <h5><?php echo $sch_text; ?></h5>
                                <?php } ?>
                                <?php if ( ! empty( $sch_content ) ) { ?>
                                    <p><?php echo $sch_content; ?></p>
                                <?php } ?>
                            </a>
                        </div>  
                    <?php } ?>
                <?php } ?>
            </div>
        </section>
        <?php
    }

    $html = ob_get_clean();
    return $html;
}

/**
 * Register [crpking-testimonials] shortcode
 */
function crpking_testimonials($atts) {

    extract( shortcode_atts( array (
        'per_page'   => '-1',
        'ajax_query' => ''
                    ), $atts ) );

    ob_start();

    $testi_left_query = new WP_Query(
            array (
        'post_type'      => CRPKING_TESTIMONIAL_POST_TYPE,
        'posts_per_page' => -1,
        'meta_query'     => array (
            array (
                'key'     => 'crpking_testi_display_section',
                'value'   => 'left',
                'compare' => 'LIKE',
            ),
        ),
            )
    );

    $testi_right_query = new WP_Query(
            array (
        'post_type'      => CRPKING_TESTIMONIAL_POST_TYPE,
        'posts_per_page' => -1,
        'meta_query'     => array (
            array (
                'key'     => 'crpking_testi_display_section',
                'value'   => 'right',
                'compare' => 'LIKE',
            ),
        ),
            )
    );

    if ( ( $testi_left_query->have_posts() ) && ( $testi_right_query->have_posts() ) ) {
        ?>
        <section>
            <div class="row">
                <div class="testimonial-row">
                    <div class="col-md-6 test-left">
                        <?php
                        while ( $testi_left_query->have_posts() ) {
                            $testi_left_query->the_post();
                            ?>
                            <?php
                            $post_id       = get_the_ID();
                            $testi_img     = get_field( 'crpking_testi_author_image', $post_id );
                            $testi_img_src = '';
                            if ( ! empty( $testi_img ) ) {
                                $testi_img_src = isset( $testi_img[ 'sizes' ][ 'thumbnail' ] ) ? $testi_img[ 'sizes' ][ 'thumbnail' ] : '';
                                $testi_img_src = empty( $testi_img_src ) && isset( $testi_img[ 'url' ] ) ? $testi_img[ 'url' ] : $testi_img_src;
                            }
                            $testi_img_alt = ! empty( $testi_img ) ? $testi_img[ 'alt' ] : '';
                            $testi_img_alt = ! empty( $testi_img_alt ) ? $testi_img_alt : $testi_img[ 'name' ];
                            $testi_name    = get_field( 'crpking_testi_author_name', $post_id );
                            ?>
                            <div class="testimonial-box">
                                <?php the_content(); ?>
                                <div class="testimonial-name">
                                    <?php if ( ! empty( $testi_img_src ) ) { ?>
                                        <div class="t-img">
                                            <img title="<?php echo $testi_img[ 'name' ]; ?>" src="<?php echo $testi_img_src; ?>" alt="<?php echo $testi_img_alt; ?>">
                                        </div>
                                    <?php } ?>
                                    <?php if ( ! empty( $testi_name ) ) { ?>
                                        <h6><?php echo $testi_name; ?></h6>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php } wp_reset_query(); ?>
                    </div>
                    <div class="col-md-6 test-right">
                        <?php
                        while ( $testi_right_query->have_posts() ) {
                            $testi_right_query->the_post();
                            ?>
                            <?php
                            $post_id       = get_the_ID();
                            $testi_img     = get_field( 'crpking_testi_author_image', $post_id );
                            $testi_img_src = '';
                            if ( ! empty( $testi_img ) ) {
                                $testi_img_src = isset( $testi_img[ 'sizes' ][ 'thumbnail' ] ) ? $testi_img[ 'sizes' ][ 'thumbnail' ] : '';
                                $testi_img_src = empty( $testi_img_src ) && isset( $testi_img[ 'url' ] ) ? $testi_img[ 'url' ] : $testi_img_src;
                            }
                            $testi_img_alt = ! empty( $testi_img ) ? $testi_img[ 'alt' ] : '';
                            $testi_img_alt = ! empty( $testi_img_alt ) ? $testi_img_alt : $testi_img[ 'name' ];
                            $testi_name    = get_field( 'crpking_testi_author_name', $post_id );
                            ?>
                            <div class="testimonial-box">
                                <?php the_content(); ?>
                                <div class="testimonial-name">
                                    <?php if ( ! empty( $testi_img_src ) ) { ?>
                                        <div class="t-img">
                                            <img title="<?php echo $testi_img[ 'name' ]; ?>" src="<?php echo $testi_img_src; ?>" alt="<?php echo $testi_img_alt; ?>">
                                        </div>
                                    <?php } ?>
                                    <?php if ( ! empty( $testi_name ) ) { ?>
                                        <h6><?php echo $testi_name; ?></h6>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php } wp_reset_query(); ?>
                    </div>
                </div>
            </div>
        </section>
        <?php
    }

    $html = ob_get_clean();
    return $html;
}

/**
 * Register [crpking-testimonial-slider] shortcode
 */
function crpking_testimonial_slider($atts) {

    extract( shortcode_atts( array (
        'per_page'   => '-1',
        'ajax_query' => ''
                    ), $atts ) );

    ob_start();

    $testi_query = new WP_Query(
            array (
        'post_type'      => CRPKING_TESTIMONIAL_POST_TYPE,
        'posts_per_page' => -1,
            )
    );

    if ( ( $testi_query->have_posts() ) ) {
        ?>
        <section class="testimonial-slidersec">
            <div class="testimonial-slides" >
                <?php
                while ( $testi_query->have_posts() ) {
                    $testi_query->the_post();
                    ?>
                    <?php
                    $post_id    = get_the_ID();
                    $name       = get_field( 'crpking_testi_author_name', $post_id );
                    $company    = get_field( 'crpking_testi_author_company', $post_id );
                    $content    = get_field( 'crpking_testi_slider_content', $post_id );
                    $bg_img     = get_field( 'crpking_testi_slider_bg_img', 'option' );
                    $bg_img_src = ! empty( $bg_img ) ? $bg_img[ 'url' ] : '';
                    $bg_img_src = ! empty( $bg_img_src ) ? ' style="background-image:url(' . $bg_img_src . ')"' : '';
                    if ( ! empty( $content ) ) {
                        ?>
                        <div class="testimonial-bg"<?php echo $bg_img_src; ?>>
                            <div class=" testimonila-slide-content ">
                                <?php echo $content; ?>
                                <?php if ( ! empty( $name ) ) { ?>
                                    <h4><?php echo $name; ?></h4>
                                <?php } ?>
                                <?php if ( ! empty( $company ) ) { ?>
                                    <h6><?php echo $company; ?></h6>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>
                <?php } wp_reset_query(); ?>
            </div>
        </section>
        <?php
    }

    $html = ob_get_clean();
    return $html;
}

/**
 * Register [crpking-zipcode] shortcode
 */
function crpking_zipcode($atts) {

    extract( shortcode_atts( array (
        'per_page'   => '-1',
        'ajax_query' => ''
                    ), $atts ) );

    ob_start();

    if ( ! empty( $_POST ) ) {
        if ( isset( $_POST[ 'home_zipcode' ] ) ) {
            return $_POST[ 'home_zipcode' ];
        }
    } else {
        return '';
    }

    $html = ob_get_clean();
    return $html;
}

/**
 * Register [crpking-home-map] shortcode
 */
function crpking_home_map($atts) {

    extract( shortcode_atts( array (
        'per_page'   => '-1',
        'ajax_query' => ''
                    ), $atts ) );

    ob_start();

    $loc_query = new WP_Query( array (
        'post_type'      => CRPKING_LOCATION_POST_TYPE,
        'posts_per_page' => '-1',
        'post_status'    => 'publish',
            ) );
    if ( $loc_query->have_posts() ) {
        $loc_latitude  = array ();
        $loc_longitude = array ();
        $loc_link      = array ();
        $count         = '';
        while ( $loc_query->have_posts() ) {
            $loc_query->the_post();
            $loc_id          = get_the_ID();
            $loc_latitude[]  = get_field( 'crpking_loc_latitude', $loc_id );
            $loc_longitude[] = get_field( 'crpking_loc_longitude', $loc_id );
            $loc_link[]      = get_the_permalink();
            $count ++;
        }
        wp_reset_query();
        ?>
        <div class="footer_map">
            <div class="map" id="map" style="height: 400px;"></div>
        </div>
        <script type="text/javascript">
            jQuery(window).load(function ($) {
                var locations = new Array();
        <?php for ( $i = 0; $i < $count; $i ++ ) { ?>
                    locations[<?php echo $i; ?>] = [<?php echo $loc_latitude[ $i ]; ?>, <?php echo $loc_longitude[ $i ]; ?>, '<?php echo $loc_link[ $i ]; ?>'];
        <?php } ?>
                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 9,
                    center: new google.maps.LatLng(45.004998, -93.167421),
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    scaleControl: false,
                    scrollwheel: false,
                });

                var marker, i;

                for (i = 0; i < locations.length; i++) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(locations[i][0], locations[i][1]),
                        map: map
                    });

                    google.maps.event.addListener(marker, 'click', (function (marker, i) {
                        return function () {
                            window.location.href = locations[i][2];
                        }
                    })(marker, i));
                }
            });
        </script>
        <?php
    }

    $html = ob_get_clean();
    return $html;
}

//add shortcode to display Products categories in Home Page
add_shortcode( 'crpking-home-products', 'crpking_home_products' );

//add shortcode to display Search option in home page
add_shortcode( 'crpking-home-search-zip', 'crpking_home_search_zip' );

//add shortcode to display all Locations in Location Page
add_shortcode( 'crpking-locations', 'crpking_locations' );

//add shortcode to display all the details in single location page
add_shortcode( 'crpking-location-details', 'crpking_location_details' );

//add shortcode to display appointment confirmed page
add_shortcode( 'crpking-appointment-confirmed', 'crpking_appointment_confirmed' );

//add shortcode to display products searched by ajax
add_shortcode( 'crpking-ajax-products', 'crpking_ajax_products' );

//add shortcode to display common links section
add_shortcode( 'crpking-common-links', 'crpking_common_links' );

//add shortcode to display testimonials in testimonial page
add_shortcode( 'crpking-testimonials', 'crpking_testimonials' );

//add shortcode to display testimonials slider in a page
add_shortcode( 'crpking-testimonial-slider', 'crpking_testimonial_slider' );

//add shortcode to enter dynamic value in form
add_shortcode( 'crpking-zipcode', 'crpking_zipcode' );

//add shortcode to display map above footer
add_shortcode( 'crpking-home-map', 'crpking_home_map' );
?>