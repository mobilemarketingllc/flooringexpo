<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Register Custom Post Types
 */
function crpking_register_post_types() {

    $order_labels = array(
                            'name'               => _x( 'Order', CRPKING_ORDER_POST_TYPE, 'crpking' ),
                            'singular_name'      => _x( 'Order', CRPKING_ORDER_POST_TYPE, 'crpking' ),
                            'menu_name'          => _x( 'Orders', CRPKING_ORDER_POST_TYPE, 'crpking' ),
                            'name_admin_bar'     => _x( 'Orders', CRPKING_ORDER_POST_TYPE, 'crpking' ),
                            'add_new'            => _x( 'Add New', CRPKING_ORDER_POST_TYPE, 'crpking' ),
                            'add_new_item'       => __( 'Add New Order', 'crpking' ),
                            'new_item'           => __( 'New Order', 'crpking' ),
                            'edit_item'          => __( 'Edit Order', 'crpking' ),
                            'view_item'          => __( 'View Order', 'crpking' ),
                            'all_items'          => __( 'All Orders', 'crpking' ),
                            'search_items'       => __( 'Search Order', 'crpking' ),
                            'parent_item_colon'  => __( 'Parent Order:', 'crpking' ),
                            'not_found'          => __( 'No Orders found.', 'crpking' ),
                            'not_found_in_trash' => __( 'No Orders found in Trash.', 'crpking' ),
                        );

    $order_args = array(
                            'labels'             => $order_labels,
                            'public'             => true,
                            'publicly_queryable' => false,
                            'show_ui'            => true,
                            'show_in_menu'       => true,
                            'query_var'          => true,
                            'rewrite'            => array( 'slug'=> 'order', 'with_front' => false ),
                            'capability_type'    => 'post',
                            'has_archive'        => false,
                            'hierarchical'       => false,
                            'menu_position'      => null,
                            'menu_icon'          => 'dashicons-chart-line',
                            'supports'           => array( 'title', 'editor', 'excerpt', 'thumbnail', 'page-attributes' )
                        );

    register_post_type( CRPKING_ORDER_POST_TYPE, $order_args );
    
    $testimonial_labels = array(
                            'name'               => _x( 'Testimonials', 'crpking_testimonial', 'crpking' ),
                            'singular_name'      => _x( 'Testimonial', 'crpking_testimonial', 'crpking' ),
                            'menu_name'          => _x( 'Testimonials', 'crpking_testimonial', 'crpking' ),
                            'name_admin_bar'     => _x( 'Testimonials', 'crpking_testimonial', 'crpking' ),
                            'add_new'            => _x( 'Add New', 'crpking_testimonial', 'crpking' ),
                            'add_new_item'       => __( 'Add New Testimonial', 'crpking' ),
                            'new_item'           => __( 'New Testimonial', 'crpking' ),
                            'edit_item'          => __( 'Edit Testimonial', 'crpking' ),
                            'view_item'          => __( 'View Testimonial', 'crpking' ),
                            'all_items'          => __( 'All Testimonials', 'crpking' ),
                            'search_items'       => __( 'Search Testimonial', 'crpking' ),
                            'parent_item_colon'  => __( 'Parent Testimonial:', 'crpking' ),
                            'not_found'          => __( 'No testimonials found.', 'crpking' ),
                            'not_found_in_trash' => __( 'No testimonials found in Trash.', 'crpking' ),
                        );

    $testimonial_args = array(
                            'labels'             => $testimonial_labels,
                            'public'             => true,
                            'publicly_queryable' => false,
                            'show_ui'            => true,
                            'show_in_menu'       => true,
                            'query_var'          => true,
                            'rewrite'            => array( 'slug'=> 'testimonials', 'with_front' => false ),
                            'capability_type'    => 'post',
                            'has_archive'        => false,
                            'hierarchical'       => false,
                            'menu_position'      => null,
                            'menu_icon'          => 'dashicons-testimonial',
                            'supports'           => array( 'title', 'editor', 'excerpt', 'thumbnail', 'page-attributes' )
                        );

    register_post_type( CRPKING_TESTIMONIAL_POST_TYPE, $testimonial_args );

    $location_labels = array(
                            'name'               => _x( 'Locations', 'crpking_location', 'crpking' ),
                            'singular_name'      => _x( 'Location', 'crpking_location', 'crpking' ),
                            'menu_name'          => _x( 'Locations', 'crpking_location', 'crpking' ),
                            'name_admin_bar'     => _x( 'Locations', 'crpking_location', 'crpking' ),
                            'add_new'            => _x( 'Add New', 'crpking_location', 'crpking' ),
                            'add_new_item'       => __( 'Add New Location', 'crpking' ),
                            'new_item'           => __( 'New Location', 'crpking' ),
                            'edit_item'          => __( 'Edit Location', 'crpking' ),
                            'view_item'          => __( 'View Location', 'crpking' ),
                            'all_items'          => __( 'All Locations', 'crpking' ),
                            'search_items'       => __( 'Search Location', 'crpking' ),
                            'parent_item_colon'  => __( 'Parent Location:', 'crpking' ),
                            'not_found'          => __( 'No locations found.', 'crpking' ),
                            'not_found_in_trash' => __( 'No locations found in Trash.', 'crpking' ),
                        );

    $location_args = array(
                            'labels'             => $location_labels,
                            'public'             => true,
                            'publicly_queryable' => true,
                            'show_ui'            => true,
                            'show_in_menu'       => true,
                            'query_var'          => true,
                            'rewrite'            => array( 'slug'=> 'locations', 'with_front' => false ),
                            'capability_type'    => 'post',
                            'has_archive'        => false,
                            'hierarchical'       => false,
                            'menu_position'      => null,
                            'menu_icon'          => 'dashicons-location',
                            'supports'           => array( 'title', 'editor', 'excerpt', 'thumbnail', 'page-attributes' )
                        );

    register_post_type( CRPKING_LOCATION_POST_TYPE, $location_args );
    
    // Add new taxonomy, make it hierarchical (like categories)
    /*$labels = array(
                    'name'              => _x( 'Categories', 'taxonomy general name', 'crpking'),
                    'singular_name'     => _x( 'Category', 'taxonomy singular name','crpking' ),
                    'search_items'      => __( 'Search Categories','crpking' ),
                    'all_items'         => __( 'All Categories','crpking' ),
                    'parent_item'       => __( 'Parent Category','crpking' ),
                    'parent_item_colon' => __( 'Parent Category:','crpking' ),
                    'edit_item'         => __( 'Edit Category' ,'crpking'), 
                    'update_item'       => __( 'Update Category' ,'crpking'),
                    'add_new_item'      => __( 'Add New Category' ,'crpking'),
                    'new_item_name'     => __( 'New Category Name' ,'crpking'),
                    'menu_name'         => __( 'Categories' ,'crpking')
                );

    $args = array(
                    'hierarchical'      => true,
                    'labels'            => $labels,
                    'show_ui'           => true,
                    'show_admin_column' => true,
                    'query_var'         => true,
                    'rewrite'           => array( 'slug'=> 'location' )
                );
	
    register_taxonomy( CRPKING_LOCATION_POST_TAX, CRPKING_LOCATION_POST_TYPE, $args );*/
    
    //flush rewrite rules
    flush_rewrite_rules();
}

//add action to create custom post type
add_action( 'init', 'crpking_register_post_types' );
?>