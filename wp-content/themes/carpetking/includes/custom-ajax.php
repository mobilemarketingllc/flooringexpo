<?php

// Exit if accessed directly
if (!defined('ABSPATH'))
    exit;

/*
 * Hooks for ajax
 */

/*
 * Hooks for search product ajax
 */

function search_product_callback() {

    if (isset($_POST['cat_slug'])) {

        $cat_slug = $_POST['cat_slug'];

        $tax_query_args             = array();
        $tax_query_args['relation'] = 'AND';

        if (!empty($cat_slug)) {
            $tax_query_args[] = array(
                'taxonomy' => CRPKING_PRODUCT_POST_CAT,
                'field'    => 'slug',
                'terms'    => $cat_slug
            );
        }

        $search_args  = array(
            'post_type'      => CRPKING_PRODUCT_POST_TYPE,
            'tax_query'      => $tax_query_args,
            'posts_per_page' => -1,
        );
        $search_query = new WP_Query($search_args);
        ?>

        <?php if ($search_query->have_posts()) : ?>

            <?php

            /**
             * woocommerce_before_shop_loop hook.
             *
             * @hooked woocommerce_result_count - 20
             * @hooked woocommerce_catalog_ordering - 30
             */
            do_action('woocommerce_before_shop_loop');
            ?>

            <?php woocommerce_product_loop_start(); ?>

            <?php woocommerce_product_subcategories(); ?>

            <?php while ($search_query->have_posts()) : $search_query->the_post(); ?>

                <?php wc_get_template_part('content', 'product'); ?>

            <?php endwhile; // end of the loop. ?>

            <?php woocommerce_product_loop_end(); ?>

            <?php

            /**
             * woocommerce_after_shop_loop hook.
             *
             * @hooked woocommerce_pagination - 10
             */
            do_action('woocommerce_after_shop_loop');
            ?>

        <?php elseif (!woocommerce_product_subcategories(array('before' => woocommerce_product_loop_start(false), 'after' => woocommerce_product_loop_end(false)))) : ?>

            <?php wc_get_template('loop/no-products-found.php'); ?>

        <?php endif; ?>

        <?php

    } else {

    }
    exit();
}

/*
 * Hooks for sort featured product by ajax
 */

function sort_featured_product_callback() {

//    echo do_shortcode( '[featured_products]' );
    $ajax_args = array(
        'post_type'      => CRPKING_PRODUCT_POST_TYPE,
        'meta_key'       => '_featured',
        'meta_value'     => 'yes',
        'posts_per_page' => 1
    );

    $ajax_query = new WP_Query($ajax_args);
    ?>

    <?php if ($ajax_query->have_posts()) : ?>

        <?php

        /**
         * woocommerce_before_shop_loop hook.
         *
         * @hooked woocommerce_result_count - 20
         * @hooked woocommerce_catalog_ordering - 30
         */
        do_action('woocommerce_before_shop_loop');
        ?>

        <?php woocommerce_product_loop_start(); ?>

        <?php woocommerce_product_subcategories(); ?>

        <?php while ($ajax_query->have_posts()) : $ajax_query->the_post(); ?>

            <?php wc_get_template_part('content', 'product'); ?>

        <?php endwhile; // end of the loop.  ?>

        <?php woocommerce_product_loop_end(); ?>

        <?php

        /**
         * woocommerce_after_shop_loop hook.
         *
         * @hooked woocommerce_pagination - 10
         */
        do_action('woocommerce_after_shop_loop');
        ?>

    <?php elseif (!woocommerce_product_subcategories(array('before' => woocommerce_product_loop_start(false), 'after' => woocommerce_product_loop_end(false)))) : ?>

        <?php wc_get_template('loop/no-products-found.php'); ?>

    <?php endif; ?>

    <?php

    exit();
}

/*
 * Hooks for sort newest product by ajax
 */

function sort_newest_product_callback() {

//    echo do_shortcode( '[featured_products]' );
    $ajax_args = array(
        'post_type'      => CRPKING_PRODUCT_POST_TYPE,
        'posts_per_page' => -1,
        'orderby'        => 'date',
        'order'          => 'DESC'
    );

    $ajax_query = new WP_Query($ajax_args);
    ?>

    <?php if ($ajax_query->have_posts()) : ?>

        <?php

        /**
         * woocommerce_before_shop_loop hook.
         *
         * @hooked woocommerce_result_count - 20
         * @hooked woocommerce_catalog_ordering - 30
         */
        do_action('woocommerce_before_shop_loop');
        ?>

        <?php woocommerce_product_loop_start(); ?>

        <?php woocommerce_product_subcategories(); ?>

        <?php while ($ajax_query->have_posts()) : $ajax_query->the_post(); ?>

            <?php wc_get_template_part('content', 'product'); ?>

        <?php endwhile; // end of the loop. ?>

        <?php woocommerce_product_loop_end(); ?>

        <?php

        /**
         * woocommerce_after_shop_loop hook.
         *
         * @hooked woocommerce_pagination - 10
         */
        do_action('woocommerce_after_shop_loop');
        ?>

    <?php elseif (!woocommerce_product_subcategories(array('before' => woocommerce_product_loop_start(false), 'after' => woocommerce_product_loop_end(false)))) : ?>

        <?php wc_get_template('loop/no-products-found.php'); ?>

    <?php endif; ?>

    <?php

    exit();
}

/*
 * Hooks for sort name product by ajax
 */

function sort_name_product_callback() {

//    echo do_shortcode( '[featured_products]' );
    $ajax_args = array(
        'post_type'      => CRPKING_PRODUCT_POST_TYPE,
        'posts_per_page' => -1,
        'orderby'        => 'title',
        'order'          => 'ASC'
    );

    $ajax_query = new WP_Query($ajax_args);
    ?>

    <?php if ($ajax_query->have_posts()) : ?>

        <?php

        /**
         * woocommerce_before_shop_loop hook.
         *
         * @hooked woocommerce_result_count - 20
         * @hooked woocommerce_catalog_ordering - 30
         */
        do_action('woocommerce_before_shop_loop');
        ?>

        <?php woocommerce_product_loop_start(); ?>

        <?php woocommerce_product_subcategories(); ?>

        <?php while ($ajax_query->have_posts()) : $ajax_query->the_post(); ?>

            <?php wc_get_template_part('content', 'product'); ?>

        <?php endwhile; // end of the loop. ?>

        <?php woocommerce_product_loop_end(); ?>

        <?php

        /**
         * woocommerce_after_shop_loop hook.
         *
         * @hooked woocommerce_pagination - 10
         */
        do_action('woocommerce_after_shop_loop');
        ?>

    <?php elseif (!woocommerce_product_subcategories(array('before' => woocommerce_product_loop_start(false), 'after' => woocommerce_product_loop_end(false)))) : ?>

        <?php wc_get_template('loop/no-products-found.php'); ?>

    <?php endif; ?>

    <?php

    exit();
}

/*
 * Hooks for test ajax
 */

function test_callback() {

    echo "<pre>";
    print_r($_REQUEST);
    echo "</pre>";
    exit();
}

/*
 * Hooks for changing time slots ajax
 */

function change_time_slots_callback() {

    global $wpdb;
    $date       = isset($_POST['appointment_date']) ? $_POST['appointment_date'] : '';
    $choices    = '';
    $check_data = '';
    $table      = 'wp_timing';

    if (!empty($date)) {
//        if( is_array( $timings ) ) {
//            $choices .= '<option value=""></option>';
//            foreach ( $timings as $key => $value ) {
//                if( in_array($date , $value) ) {
//                    $start_time = $value['crpking_timings_start_time'];
//                    $end_time   = $value['crpking_timings_end_time'];
////                    $choices[ $start_time . ' - ' . $end_time ] = $start_time . ' - ' . $end_time;
//                    $choices .= '<option value="' . $start_time . ' - ' . $end_time . '">' . $start_time . ' - ' . $end_time . '</option>';
//                }
//            }
//        }

        $previous_entries = new WP_Query(array(
            'post_type'       => CRPKING_CONTACT_FORM_ENTRY_POST_TYPE,
            'post_parent__in' => array(CRPKING_SCHEDULE_APPOINTMENT_FORM_ID, CRPKING_PAGE_SCHEDULE_APPOINTMENT_FORM_ID),
            'posts_per_page'  => '-1',
            'fields'          => 'ids',
        ));

        $entry_time_details = array();
        if (!empty($previous_entries->posts)) {
            foreach ($previous_entries->posts as $key => $value) {
                $temp_details = get_post_meta($value, 'form_fields', false);
                if (isset($temp_details[0]['appointment_date'])) {
                    if (strtotime($temp_details[0]['appointment_date']) == strtotime($date)) {
                        $entry_time_details[$value]['appointment_date'] = $temp_details[0]['appointment_date'];
                        $entry_time_details[$value]['appointment_time'] = $temp_details[0]['appointment_time'];
                    }
                }
                if (isset($temp_details[0]['app_date'])) {
                    if (strtotime($temp_details[0]['app_date']) == strtotime($date)) {
                        $entry_time_details[$value]['appointment_date'] = $temp_details[0]['app_date'];
                        $entry_time_details[$value]['appointment_time'] = $temp_details[0]['app_time'];
                    }
                }
            }
        }

        $check_data .= " for_date = '" . $date . "'";
        $query      = 'SELECT * FROM ' . $table . ' where ' . $check_data . ' ORDER BY time_id asc,start_meridian asc, start_time asc, end_time asc';
        $timings    = $wpdb->get_results($query);

        if (!empty($timings)) {
//            $choices .= '<option value="">' . __( '-- Make a Selection --' ) . '</option>';
            foreach ($timings as $key => $value) {
                $start_time    = $value->start_time;
                $start_meri    = $value->start_meridian;
                $end_time      = $value->end_time;
                $end_meri      = $value->end_meridian;
                $slots         = $value->slots;
                $fmt_value     = date("H:i:s", strtotime($start_time . ' ' . $start_meri));
                $fmt_end_value = date("H:i:s", strtotime($end_time . ' ' . $end_meri));
                $flag          = 0;
                foreach ($entry_time_details as $key => $value) {
                    if (strtotime($value['appointment_time']) == strtotime($fmt_value)) {
                        $flag++;
                    }
                }
                if ($flag < $slots) {
                    $choices .= '<option value="' . $fmt_value . '" data-value="' . $fmt_end_value . '">' . $start_time . ' ' . $start_meri . ' - ' . $end_time . ' ' . $end_meri . '</option>';
                }
            }
        }
    }

    echo $choices;

    exit();
}

function check_home_page_zipcode_callback() {

    global $wpdb;
    $zipcode = isset($_POST['zipcode']) ? $_POST['zipcode'] : '';
    if (!empty($zipcode)) {
        if (!preg_match('/^[0-9]{5}$/', $zipcode)) {
            echo "Please enter a valid zipcode.";
        } else {
            $answer = getLatLong($zipcode);
            $lat1   = '';
            $lon1   = '';
            if (!empty($answer)) {
                $lat1 = $answer['latitude'];
                $lon1 = $answer['longitude'];

                $distance = '';
                if (crpking_check_acf_activation) {
                    $distance = get_field('crpking_appointment_distance', CRPKING_SCHEDULE_APPOINTMENT_PAGE_ID);
                }
                $d = !empty($distance) ? $distance : 30;
                //earth's radius in miles
                $r = 3959;

                //compute max and min latitudes / longitudes for search square
                $latN = rad2deg(asin(sin(deg2rad($lat1)) * cos($d / $r) + cos(deg2rad($lat1)) * sin($d / $r) * cos(deg2rad(0))));
                $latS = rad2deg(asin(sin(deg2rad($lat1)) * cos($d / $r) + cos(deg2rad($lat1)) * sin($d / $r) * cos(deg2rad(180))));
                $lonE = rad2deg(deg2rad($lon1) + atan2(sin(deg2rad(90)) * sin($d / $r) * cos(deg2rad($lat1)), cos($d / $r) - sin(deg2rad($lat1)) * sin(deg2rad($latN))));
                $lonW = rad2deg(deg2rad($lon1) + atan2(sin(deg2rad(270)) * sin($d / $r) * cos(deg2rad($lat1)), cos($d / $r) - sin(deg2rad($lat1)) * sin(deg2rad($latN))));

                $args      = array(
                    'post_type'  => 'crpking_location',
                    'meta_query' => array(
                        'relation' => 'AND',
                        array(
                            'key'     => 'crpking_loc_latitude',
                            'value'   => array($latS, $latN),
                            'compare' => 'BETWEEN',
                        ),
                        array(
                            'key'     => 'crpking_loc_longitude',
                            'value'   => array($lonE, $lonW),
                            'compare' => 'BETWEEN',
                        ),
                    ),
                );
                $loc_query = new WP_Query($args);
                if (!( $loc_query->have_posts() )) {
                    echo 'Unfortunately we currently don\'t service the zip code that you requested.';
                } else {
                    echo 'Success';
                }
            }
        }
    } else {
        echo "Please enter a zipcode.";
    }
    exit();
}

function add_schedule_two_hours_more() {
    $time = isset($_POST['time']) ? $_POST['time'] : '';
    if (!empty($time)) {
        $pro_time_after_twoh = date('H:i:s', strtotime($time . '+2 hour'));
    }
    echo $time . ' - ' . $pro_time_after_twoh . '  We will arrive within a two-hour window';
    exit();
}

//add_action('wp_ajax_search_product_action', 'search_product_callback');
//add_action('wp_ajax_nopriv_search_product_action', 'search_product_callback');

add_action('wp_ajax_sort_featured_product_action', 'sort_featured_product_callback');
add_action('wp_ajax_nopriv_sort_featured_product_action', 'sort_featured_product_callback');

add_action('wp_ajax_sort_newest_product_action', 'sort_newest_product_callback');
add_action('wp_ajax_nopriv_sort_newest_product_action', 'sort_newest_product_callback');

add_action('wp_ajax_sort_name_product_action', 'sort_name_product_callback');
add_action('wp_ajax_nopriv_sort_name_product_action', 'sort_name_product_callback');

add_action('wp_ajax_test_action', 'test_callback');
add_action('wp_ajax_nopriv_test_action', 'test_callback');

add_action('wp_ajax_change_time_slots_action', 'change_time_slots_callback');
add_action('wp_ajax_nopriv_change_time_slots_action', 'change_time_slots_callback');

add_action('wp_ajax_check_home_page_zipcode', 'check_home_page_zipcode_callback');
add_action('wp_ajax_nopriv_check_home_page_zipcode', 'check_home_page_zipcode_callback');

add_action('wp_ajax_add_schedule_two_hours_more', 'add_schedule_two_hours_more');
add_action('wp_ajax_nopriv_add_schedule_two_hours_more', 'add_schedule_two_hours_more');

function fun_carpetking_order_csv_import() {
    $delete_all = $_POST['delete_existing'];
    if ( $delete_all == 'true' ) {
        $allposts= get_posts( array('post_type'=>'orders','numberposts'=>-1) );
        foreach ($allposts as $eachpost) {
            wp_trash_post( $eachpost->ID, true );
        }
    }
    $array  = $fields = array();
    $i      = 0;
    $handle = @fopen($_POST['csv_url'], "r");
    if ($handle) {
        while (($row = fgetcsv($handle, 4096)) !== false) {
            if (empty($fields)) {
                $fields = $row;
                continue;
            }
            foreach ($row as $k => $value) {
                $array[$i][trim(str_replace(' ', '', $fields[$k]))] = utf8_encode(trim($value));
            }
            $i++;
        }
        if (!feof($handle)) {
            echo "Error: unexpected fgets() fail\n";
        }
        fclose($handle);
    }
    if (!empty($array)) {
        foreach ($array as $arr_key => $arr_val) {
            $title           = $arr_val['Ref#'];
            $store_order     = $arr_val['STORE-ORDER'];
            $customer        = $arr_val['CUSTOMER'];
            $name            = $arr_val['NAME'];
            $orderdate       = $arr_val['ORDERDATE'];
            $type            = $arr_val['TYPE'];
            $salesman        = $arr_val['SALESMAN'];
            $taxcode         = $arr_val['TAXCODE'];
            $taxamount       = $arr_val['TAXAMOUNT'];
            $freight         = $arr_val['FREIGHT'];
            $line            = $arr_val['LINE#'];
            $itemdescription = $arr_val['ITEMDESCRIPTION'];
            $mill            = $arr_val['MILL'];
            $style_color     = $arr_val['STYLE/COLOR'];
            $ordergross      = $arr_val['ORDERGROSS'];
            $qtyordered      = $arr_val['QTYORDERED'];
            $unitprice       = $arr_val['UNITPRICE'];
            $extprice        = $arr_val['EXTPRICE'];
            $unitcost        = $arr_val['UNITCOST'];
            $extcost         = $arr_val['EXTCOST'];
            $terms           = $arr_val['TERMS'];
            $salecode        = $arr_val['SALECODE'];
            $prodordered     = $arr_val['PRODORDERED'];
            $prodreceived    = $arr_val['PRODRECEIVED'];
            $prod_code       = $arr_val['PROD_CODE'];
            $shiptoname      = $arr_val['SHIPTONAME'];
            $expecteddt      = $arr_val['EXPECTEDDT'];
            $millnumber      = $arr_val['MILLNUMBER'];
            $contact         = $arr_val['CONTACT/REF'];
            $actualmill      = $arr_val['ACTUALMILL'];
            $taxexempt       = $arr_val['TAXEXEMPT'];
            $schinstall      = $arr_val['SCHINSTALL'];

            $my_post = array(
                'post_type'    => 'orders',
                'post_title'   => $title,
                'post_content' => $itemdescription,
                'post_status'  => 'publish',
            );
            $post_id = wp_insert_post($my_post);
            update_field('cpking_order_cpt_store_order', $store_order, $post_id);
            update_field('cpking_order_cpt_customer', $customer, $post_id);
            update_field('cpking_order_cpt_name', $name, $post_id);
            update_field('cpking_order_cpt_orderdate', $orderdate, $post_id);
            update_field('cpking_order_cpt_type', $type, $post_id);
            update_field('cpking_order_cpt_salesman', $salesman, $post_id);
            update_field('cpking_order_cpt_taxcode', $taxcode, $post_id);
            update_field('cpking_order_cpt_taxamount', $taxamount, $post_id);
            update_field('cpking_order_cpt_freight', $freight, $post_id);
            update_field('cpking_order_cpt_line', $line, $post_id);
            update_field('cpking_order_cpt_itemdescription', $itemdescription, $post_id);
            update_field('cpking_order_cpt_mill', $mill, $post_id);
            update_field('cpking_order_cpt_style_color', $style_color, $post_id);
            update_field('cpking_order_cpt_ordergross', $ordergross, $post_id);
            update_field('cpking_order_cpt_qtyordered', $qtyordered, $post_id);
            update_field('cpking_order_cpt_unitprice', $unitprice, $post_id);
            update_field('cpking_order_cpt_extprice', $extprice, $post_id);
            update_field('cpking_order_cpt_unitcost', $unitcost, $post_id);
            update_field('cpking_order_cpt_extcost', $extcost, $post_id);
            update_field('cpking_order_cpt_terms', $terms, $post_id);
            update_field('cpking_order_cpt_salecode', $salecode, $post_id);
            update_field('cpking_order_cpt_prodordered', $prodordered, $post_id);
            update_field('cpking_order_cpt_prodreceived', $prodreceived, $post_id);
            update_field('cpking_order_cpt_prod_code', $prod_code, $post_id);
            update_field('cpking_order_cpt_shiptoname', $shiptoname, $post_id);
            update_field('cpking_order_cpt_expecteddt', $expecteddt, $post_id);
            update_field('cpking_order_cpt_millnumber', $millnumber, $post_id);
            update_field('cpking_order_cpt_contact_ref', $contact, $post_id);
            update_field('cpking_order_cpt_actualmill', $actualmill, $post_id);
            update_field('cpking_order_cpt_taxexempt', $taxexempt, $post_id);
            update_field('cpking_order_cpt_schinstall', $schinstall, $post_id);
            
            if ( $post_id !== '' ) {
                $response = "true";
            } else {
                $response = "false";
            }
        }
    }
    echo $response;
    exit();
}

add_action('wp_ajax_carpetking_order_csv_import', 'fun_carpetking_order_csv_import');
add_action('wp_ajax_nopriv_carpetking_order_csv_import', 'fun_carpetking_order_csv_import');

function fun_cpking_get_order_status(){
    $reference = $_POST['reference'];
    $args              = array (
        "post_type"      => CRPKING_ORDER_POST_TYPE,
        "posts_per_page" => -1,
        "post_status"    => 'publish',
        "s"              => $reference
    );
    $order_status_query = new WP_Query ( $args );
    ?>
    <div class="order_status">
        <div class="order_status_wrap">
            <?php if ( $order_status_query->have_posts() ) { ?>
                    <h3>order status : <?php echo $reference; ?></h3>
                    <?php while ( $order_status_query->have_posts() ) { 
                            $order_status_query->the_post();
                            $title         = get_the_title ();
                            $name          = get_field ( 'cpking_order_cpt_name' );
                            $order         = get_field ( 'cpking_order_cpt_store_order' );
                            $desc          = get_field ( 'cpking_order_cpt_itemdescription' );
                            $received      = get_field ( 'cpking_order_cpt_prodreceived' );
                            $received      = ! empty ( $received ) ? $received : 'No';
                            $expected_date = get_field ( 'cpking_order_cpt_expecteddt' );
                            $expected_date = ! empty ( $expected_date ) ? $expected_date : 'N/A';
                            $install_date  = get_field ( 'cpking_order_cpt_schinstall' );
                            $install_date  = ! empty ( $install_date ) ? $install_date : 'N/A';
                            
//                            if ( $expected_date !== 'N/A' ){
//                                $expected_date = DateTime::createFromFormat('d/m/Y', $expected_date)->format('m/d/Y');
//                            }
//                            if ( $install_date !== 'N/A' ){
//                                $install_date = DateTime::createFromFormat('d/m/Y', $install_date)->format('m/d/Y');
//                            }
//                            if ( $received !== 'No' ){
//                                $received = DateTime::createFromFormat('d/m/Y', $received)->format('m/d/Y');
//                            }
                            ?>
                            <div class="content">
                                <ul>
                                    <?php if ( !empty ( $name ) ) { ?>
                                            <li>Name<strong><?php echo $name; ?></strong> </li>
                                    <?php } ?>
                                    <?php if ( !empty ( $order ) ) { ?>
                                            <li>Order Number<strong> <?php echo $order; ?></strong> </li>
                                    <?php } ?>
                                    <?php if ( !empty ( $desc ) ) { ?>
                                            <li>Description<strong><?php echo $desc; ?></strong> </li>
                                    <?php } ?>
                                    <?php if ( !empty ( $received ) ) { ?>
                                            <li>Product Received in Warehouse<strong><?php echo $received; ?></strong></li>
                                    <?php } ?>
                                    <?php if ( !empty ( $expected_date ) ) { ?>
                                            <li>Expected Date at Warehouse<strong><?php echo $expected_date; ?></strong></li>
                                    <?php } ?>
                                    <?php if ( !empty ( $install_date ) ) { ?>
                                            <li>Scheduled Install Date<strong><?php echo $install_date; ?></strong></li>
                                    <?php } ?>
                                </ul>
                            </div>
                            <?php 
                        }
                        wp_reset_postdata();
                        wp_reset_query();
                } else { ?>
                    <h4>No Data Found With Reference No. <?php echo $reference; ?></h4>
            <?php } ?>
            <div class="btn serach_again"><a href="#">Search Again</a></div>
        </div>
    </div>
    <?php
    exit();
}

add_action('wp_ajax_cpking_get_order_status', 'fun_cpking_get_order_status');
add_action('wp_ajax_nopriv_cpking_get_order_status', 'fun_cpking_get_order_status');
?>