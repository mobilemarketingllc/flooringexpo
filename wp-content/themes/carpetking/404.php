<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage CRPKING
 * @since Crp King 1.0
 */

get_header(); ?>

<div class="container">
    <div class="featured_page_main">

        <div id="primary" class="content-area">
            <main id="main" class="site-main" role="main">

                    <section class="error-404 not-found">
                            <header class="page-header">
                                    <h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'crpking' ); ?></h1>
                            </header><!-- .page-header -->

                            <div class="page-content">
                                    <p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'crpking' ); ?></p>

                                    <?php get_search_form(); ?>
                            </div><!-- .page-content -->
                    </section><!-- .error-404 -->

            </main><!-- .site-main -->
        </div><!-- .content-area -->
    
    </div>
</div>

<?php get_footer(); ?>