<div class="product-grid">
<div class="row">
<?php while ( have_posts() ): the_post(); ?>

<div class="col-md-4 col-sm-6 col-xs-12">
                <div class="fl-post-grid-post" itemscope itemtype="Product">
                    <div class="fl-post-grid-image">
                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                            <?php
                                $product_swatch = get_field('swatch_image_link');
									
                                if($product_swatch != '' ) {									
                            ?>
                                   <img src="https://mobilem.liquifire.com/mobilem?source=url[<?php the_field('swatch_image_link')?>]&scale=size[300x300],options[crop]&sink"  class="prod-img"/>	
				
                            <?php 
                                }
                                else {
                            ?>
                                    <img src="http://placehold.it/300x300?text=NO+IMAGE" style="width: 300px;">
                            <?php
                                }
                            ?>
                        </a>
                    </div>
                    
                    <div class="fl-post-grid-text product-grid">   
                            <i class="fa fa-caret-up" aria-hidden="true"></i>             
                            <div class="shop-collection"><?php the_field('collection'); ?></div>
                            <div class="shop-title"><?php echo the_title(); ?></div>
                            <div class="shop-brand"><?php the_field('brand'); ?> </div>
         
                    </div>
                </div>
            </div>
<?php endwhile; ?>
</div>
</div>