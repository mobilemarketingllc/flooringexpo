<?php
/**
 * Single Product Share
 *
 * Sharing plugins can hook into here or you can add your own code directly.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/share.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$permalink = get_the_permalink();
$title = get_the_title();
?>

<div class="product-share">
    <h6><?php _e( 'SHARE', 'crpking' ); ?></h6>
    <ul>
        <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $permalink; ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
        <li><a href="https://twitter.com/intent/tweet?text=<?php echo $title; ?>&url=<?php echo $permalink; ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
        <li><a href="https://www.linkedin.com/shareArticle?title=<?php echo $title; ?>&url=<?php echo $permalink; ?>" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
        <li><a href="https://plus.google.com/share?url=<?php echo $permalink; ?>" target="_blank"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
    </ul>
</div>
