<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $post;
$post_id = get_option( 'woocommerce_shop_page_id' );;

$btm_title = '';
$btm_desc  = '';
if( crpking_check_acf_activation() ) {
    $btm_title = get_field( 'crpking_shop_page_btm_title', $post_id );
    $btm_desc  = get_field( 'crpking_shop_page_btm_desc', $post_id );
}

get_header(); ?>
    <div class="container">
        <div class="prodct-main-sec">
            <?php
                    /**
                     * woocommerce_before_main_content hook.
                     *
                     * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
                     * @hooked woocommerce_breadcrumb - 20
                     */
    //		do_action( 'woocommerce_before_main_content' );
            ?>

                    <?php
                            /**
                             * woocommerce_archive_description hook.
                             *
                             * @hooked woocommerce_taxonomy_archive_description - 10
                             * @hooked woocommerce_product_archive_description - 10
                             */
                            do_action( 'woocommerce_archive_description' );
                    ?>
                    <div class="row">
                        <div class="col-md-3 prod-left">
                            <?php
                                    /**
                                     * woocommerce_sidebar hook.
                                     *
                                     * @hooked woocommerce_get_sidebar - 10
                                     */
                                    do_action( 'woocommerce_sidebar' );
                            ?>
                        </div>

                        <div class="col-md-9 prod-right">

                            <div class="sort_products">
                                <h6>Sort By</h6>
                                <span class="sort_featured" id="sort_featured">Featured</span>
                                <span class="sort_newest" id="sort_newest">Newest</span>
                                <span class="sort_name" id="sort_name">Name</span>
                            </div>
                            
                            <?php if ( have_posts() ) : ?>
                            
                                    <div class="product_pagination">
                                        <?php
                                                /**
                                                 * woocommerce_after_shop_loop hook.
                                                 *
                                                 * @hooked woocommerce_pagination - 10
                                                 */
                                                do_action( 'woocommerce_after_shop_loop' );
                                        ?>
                                    </div>
                            
                            <?php endif; ?>

                            <div class="ajax_loader" id="ajax_loader" style="display: none;">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/loader.gif">
                            </div>

                            <div class="product-list" id="product_list">

                                <?php if ( have_posts() ) : ?>

                                        <?php
                                                /**
                                                 * woocommerce_before_shop_loop hook.
                                                 *
                                                 * @hooked woocommerce_result_count - 20
                                                 * @hooked woocommerce_catalog_ordering - 30
                                                 */
    //                                            do_action( 'woocommerce_before_shop_loop' );
                                        ?>

                                        <?php woocommerce_product_loop_start(); ?>

                                                <?php woocommerce_product_subcategories(); ?>

                                                <?php while ( have_posts() ) : the_post(); ?>

                                                        <?php wc_get_template_part( 'content', 'product' ); ?>

                                                <?php endwhile; // end of the loop. ?>

                                        <?php woocommerce_product_loop_end(); ?>

                                        

                                <?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

                                        <?php wc_get_template( 'loop/no-products-found.php' ); ?>

                                <?php endif; ?>

                            </div>

                            <?php if( !empty( $btm_title ) || !empty( $btm_desc ) ) { ?>
                                <div class="bottom-description">
                                    <?php if( !empty( $btm_title ) ) { ?>
                                        <h6><?php echo $btm_title; ?></h6>
                                    <?php } ?>
                                    <?php if( !empty( $btm_desc ) ) { ?>
                                        <p><?php echo $btm_desc; ?></p>
                                    <?php } ?>
                                </div>
                            <?php } ?>

                        </div>
                    </div>

            <?php
                    /**
                     * woocommerce_after_main_content hook.
                     *
                     * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
                     */
    //		do_action( 'woocommerce_after_main_content' );
            ?>

            
        </div>
    </div>

<?php get_footer(); ?>
