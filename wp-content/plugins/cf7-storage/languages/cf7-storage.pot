# Copyright (C) 2016 Contact Form 7 Storage
# This file is distributed under the same license as the Contact Form 7 Storage package.
msgid ""
msgstr ""
"Project-Id-Version: Contact Form 7 Storage 1.5\n"
"Report-Msgid-Bugs-To: http://wordpress.org/support/plugin/git\n"
"POT-Creation-Date: 2016-05-04 09:16:57+00:00\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2016-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"

#: cf7-storage.php:94 cf7-storage.php:318
msgid "Entries"
msgstr ""

#: cf7-storage.php:317 cf7-storage.php:603
msgid "Contact Form Entries"
msgstr ""

#: cf7-storage.php:450
msgid "You are not allowed to move this item to Trash."
msgstr ""

#: cf7-storage.php:453
msgid "Error moving an item to Trash."
msgstr ""

#: cf7-storage.php:469
msgid "You are not allowed to restore this item from Trash."
msgstr ""

#: cf7-storage.php:472
msgid "Error in restoring an item from Trash."
msgstr ""

#: cf7-storage.php:489
msgid "You are not allowed to delete this entry."
msgstr ""

#: cf7-storage.php:492
msgid "Error in deleting an entry."
msgstr ""

#: cf7-storage.php:571
msgid "Missing entry ID."
msgstr ""

#: cf7-storage.php:610
msgid "Search results for \"%s\""
msgstr ""

#: cf7-storage.php:626
msgid "Search Entries"
msgstr ""

#: cf7-storage.php:645
msgid "This contact form submission doesn't exist!"
msgstr ""

#: cf7-storage.php:678 cf7-storage.php:927
msgctxt "No attachments found"
msgid "None"
msgstr ""

#: cf7-storage.php:686 inc/admin-list-view.php:65
msgid "Contact Form"
msgstr ""

#: cf7-storage.php:694 cf7-storage.php:884
msgid "From"
msgstr ""

#: cf7-storage.php:698 cf7-storage.php:885
msgid "To"
msgstr ""

#: cf7-storage.php:702 cf7-storage.php:883 inc/admin-list-view.php:66
msgid "Date"
msgstr ""

#: cf7-storage.php:710 cf7-storage.php:886
msgid "Subject"
msgstr ""

#: cf7-storage.php:714
msgid "Message"
msgstr ""

#: cf7-storage.php:721 cf7-storage.php:888
msgid "Attachments"
msgstr ""

#: cf7-storage.php:773
msgid "No field values were captured."
msgstr ""

#: cf7-storage.php:782
msgid "Post"
msgstr ""

#: cf7-storage.php:789 cf7-storage.php:803 cf7-storage.php:892
msgid "Referer"
msgstr ""

#: cf7-storage.php:808
msgid "Visit"
msgstr ""

#: cf7-storage.php:815
msgid "User agent"
msgstr ""

#: cf7-storage.php:822 cf7-storage.php:894
msgid "IP Address"
msgstr ""

#: cf7-storage.php:862
msgid "Form Submission"
msgstr ""

#: cf7-storage.php:864
msgid "Field Values"
msgstr ""

#: cf7-storage.php:866
msgid "Submission Details"
msgstr ""

#: cf7-storage.php:887
msgid "Body"
msgstr ""

#: cf7-storage.php:889
msgid "Form Name"
msgstr ""

#: cf7-storage.php:890
msgid "Entry ID"
msgstr ""

#: cf7-storage.php:891
msgid "Entry URL"
msgstr ""

#: cf7-storage.php:893
msgid "User-agent"
msgstr ""

#: cf7-storage.php:1053
msgid "Storage for Contact Form 7 requires the latest version of Contact Form 7 plugin."
msgstr ""

#: inc/admin-list-view.php:64
msgid "Entry"
msgstr ""

#: inc/admin-list-view.php:88
msgid "All"
msgstr ""

#: inc/admin-list-view.php:119 inc/admin-list-view.php:170
msgid "Restore"
msgstr ""

#: inc/admin-list-view.php:122 inc/admin-list-view.php:182
msgid "Delete Permanently"
msgstr ""

#: inc/admin-list-view.php:124
msgid "Move to Trash"
msgstr ""

#: inc/admin-list-view.php:162
msgid "Preview"
msgstr ""

#: inc/admin-list-view.php:176 inc/admin-list-view.php:196
#: inc/admin-list-view.php:376
msgid "Export as CSV"
msgstr ""

#: inc/admin-list-view.php:190
msgid "View"
msgstr ""

#: inc/admin-list-view.php:202
msgid "Trash"
msgstr ""

#: inc/admin-list-view.php:228
msgid "View this submission from %s"
msgstr ""

#: inc/admin-list-view.php:246
msgid "%s ago"
msgstr ""

#: inc/admin-list-view.php:296
msgid "All Contact Forms"
msgstr ""

#: inc/admin-list-view.php:316
msgid "Filter"
msgstr ""

#: inc/admin-list-view.php:328
msgid "Empty Trash"
msgstr ""

#: inc/admin-list-view.php:343
msgid "Comma delimited"
msgstr ""

#: inc/admin-list-view.php:344
msgid "Semicolon delimited"
msgstr ""

#: inc/admin-list-view.php:345
msgid "Tab delimited"
msgstr ""

#: inc/admin-list-view.php:371
msgid "Choose the CSV delimiter character"
msgstr ""

#: lib/envato-automatic-plugin-update/envato-plugin-update.php:167
msgid "Failed to retreive plugin details from the Envato API."
msgstr ""

#: lib/envato-automatic-plugin-update/envato-plugin-update.php:176
msgid "New version of <strong>%s</strong> is available."
msgstr ""
#. Plugin Name of the plugin/theme
msgid "Contact Form 7 Storage"
msgstr ""

#. Plugin URI of the plugin/theme
msgid "http://preseto.com/plugins/contact-form-7-storage"
msgstr ""

#. Description of the plugin/theme
msgid "Store all Contact Form 7 submissions (including attachments) in your WordPress dashboard."
msgstr ""

#. Author of the plugin/theme
msgid "Kaspars Dambis"
msgstr ""

#. Author URI of the plugin/theme
msgid "http://kaspars.net"
msgstr ""
