<?php /** * Settings Page * Handles to settings */// Exit if accessed directlyif ( !defined( 'ABSPATH' ) ) exit;?>
<div class="wrap">        
    <h2><?php _e('Add new data', 'esbcie'); ?></h2>        <!-- Data table start -->   
 <?php global $wpdb;
$table = 'esb_timing';
$temp_data = '';
if (isset($_POST['schedule_submit'])) {
    if (isset($_POST['schedule_day']) && isset($_POST['start_time']) && isset($_POST['start_meridian']) && isset($_POST['end_time']) && isset($_POST['end_meridian'])) {
        if (!empty($_POST['schedule_day']) && !empty($_POST['start_time']) && !empty($_POST['start_meridian']) && !empty($_POST['end_time']) && !empty($_POST['end_meridian'])) {
            $temp_data['start_time'] = $_POST['start_time'];
            $temp_data['start_time_sort'] = !empty($_POST['start_time_sort']) ? $_POST['start_time_sort'] : '';
            $temp_data['start_meridian'] = $_POST['start_meridian'];
            $temp_data['end_time'] = $_POST['end_time'];
            $temp_data['end_meridian'] = $_POST['end_meridian'];
            $temp_data['created_date'] = date('Y-m-d');
            $temp_data['modified_date'] = date('Y-m-d');
            $temp_data['for_date'] = $_POST['schedule_day'];
            $temp_data['slots'] = !empty($_POST['slot_number']) ? $_POST['slot_number'] : '';
            $wpdb->insert($table, $temp_data);
        }
    }
} ?>                <form class="timing_form" method="post">                    
    <div class="schedule-row">                        
        <label for="schedule_day"><?php _e('Schedule Day:', 'esbcie'); ?></label>                       
        <input id="schedule_day" name="schedule_day" type="text" />                    
    </div>                   
    <div class="schedule-row">                        
        <label for="start_time"><?php _e('Start time:', 'esbcie'); ?></label>                        
        <div class="schedule-sub-row">                           
            <select name="start_time" id="start_time">                               
                <option value="1"><?php _e('1', 'esbcie'); ?></option>                               
                <option value="2"><?php _e('2', 'esbcie'); ?></option>                                
                <option value="3"><?php _e('3', 'esbcie'); ?></option>                               
                <option value="4"><?php _e('4', 'esbcie'); ?></option>                              
                <option value="5"><?php _e('5', 'esbcie'); ?></option>                                
                <option value="6"><?php _e('6', 'esbcie'); ?></option>                               
                <option value="7"><?php _e('7', 'esbcie'); ?></option>                               
                <option value="8"><?php _e('8', 'esbcie'); ?></option>                               
                <option value="9"><?php _e('9', 'esbcie'); ?></option>                                
                <option value="10"><?php _e('10', 'esbcie'); ?></option>                              
                <option value="11"><?php _e('11', 'esbcie'); ?></option>                                
                <option value="12"><?php _e('12', 'esbcie'); ?></option>                            
            </select>                           
            <select name="start_meridian" id="start_meridian">                                
                <option value="AM"><?php _e('AM', 'esbcie'); ?></option>                              
                <option value="PM"><?php _e('PM', 'esbcie'); ?></option>                          
            </select>                       
        </div>                  
    </div>                   
    <div class="schedule-row">                       
        <label for="start_time_sort"><?php _e('Start time sort:', 'esbcie'); ?></label>                       
        <div class="schedule-sub-row">                            
            <select name="start_time_sort" id="start_time_sort">                               
                <option value="1"><?php _e('1', 'esbcie'); ?></option>                           
                <option value="2"><?php _e('2', 'esbcie'); ?></option>                            
                <option value="3"><?php _e('3', 'esbcie'); ?></option>                            
                <option value="4"><?php _e('4', 'esbcie'); ?></option>                            
                <option value="5"><?php _e('5', 'esbcie'); ?></option>                           
                <option value="6"><?php _e('6', 'esbcie'); ?></option>                              
                <option value="7"><?php _e('7', 'esbcie'); ?></option>                             
                <option value="8"><?php _e('8', 'esbcie'); ?></option>                            
                <option value="9"><?php _e('9', 'esbcie'); ?></option>                            
                <option value="10"><?php _e('10', 'esbcie'); ?></option>                         
                <option value="11"><?php _e('11', 'esbcie'); ?></option>                          
                <option value="12"><?php _e('12', 'esbcie'); ?></option>                          
                <option value="13"><?php _e('13', 'esbcie'); ?></option>                          
                <option value="14"><?php _e('14', 'esbcie'); ?></option>                         
                <option value="15"><?php _e('15', 'esbcie'); ?></option>                        
                <option value="16"><?php _e('16', 'esbcie'); ?></option>                        
                <option value="17"><?php _e('17', 'esbcie'); ?></option>                            
                <option value="18"><?php _e('18', 'esbcie'); ?></option>                         
                <option value="19"><?php _e('19', 'esbcie'); ?></option>                          
                <option value="20"><?php _e('20', 'esbcie'); ?></option>                          
                <option value="21"><?php _e('21', 'esbcie'); ?></option>                       
                <option value="22"><?php _e('22', 'esbcie'); ?></option>                      
                <option value="23"><?php _e('23', 'esbcie'); ?></option>                     
                <option value="24"><?php _e('24', 'esbcie'); ?></option>                    
            </select>                       
        </div>                    
    </div>                   
    <div class="schedule-row">             
        <label for="end_time"><?php _e('End time:', 'esbcie'); ?></label>                        
        <div class="schedule-sub-row">                         
            <select name="end_time" id="end_time">                          
                <option value="1"><?php _e('1', 'esbcie'); ?></option>          
                <option value="2"><?php _e('2', 'esbcie'); ?></option>                         
                <option value="3"><?php _e('3', 'esbcie'); ?></option>                         
                <option value="4"><?php _e('4', 'esbcie'); ?></option>               
                <option value="5"><?php _e('5', 'esbcie'); ?></option>             
                <option value="6"><?php _e('6', 'esbcie'); ?></option>               
                <option value="7"><?php _e('7', 'esbcie'); ?></option>               
                <option value="8"><?php _e('8', 'esbcie'); ?></option>                   
                <option value="9"><?php _e('9', 'esbcie'); ?></option>                    
                <option value="10"><?php _e('10', 'esbcie'); ?></option>                   
                <option value="11"><?php _e('11', 'esbcie'); ?></option>                    
                <option value="12"><?php _e('12', 'esbcie'); ?></option>                    
            </select>                            
            <select name="end_meridian" id="end_meridian">                 
                <option value="AM"><?php _e('AM', 'esbcie'); ?></option>                 
                <option value="PM"><?php _e('PM', 'esbcie'); ?></option>                  
            </select>                   
        </div>                    
    </div>                
    
    <div class="schedule-row">                    
        <label for="slot_number"><?php _e('Number of slots:', 'esbcie'); ?></label>   
        <div class="schedule-sub-row">                       
            <input type="text" name="slot_number" id="slot_number" required="">         
        </div>                  
    </div>              
    <div class="schedule-row">                    
        <input type="submit" value="<?php _e('Add', 'esbcie'); ?>" name="schedule_submit" id="schedule_submit">           
    </div>            
</form>          
  <?php ?>    <!-- Data table end -->   
</div>