<?php

/**
 * Settings Page
 * Handles to settings
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

?>
<div class="wrap">
    
    <h2><?php _e( 'View all the data', 'esbcie' ); ?></h2>
    
    <!-- Data table start -->
    <?php
        global $wpdb;
        $table = 'esb_timing';
        $query = "SELECT * FROM " . $table . ' ORDER BY for_date desc';
        $timings = $wpdb->get_results($query);
//        echo "<pre>" ;
//        print_r($wpdb->last_query) ;
//        echo "</pre>" ;
//        echo "<pre>" ;
//        print_r( $timings ) ;
//        echo "</pre>" ;
        if( !empty( $timings ) ) {
            $i = 1;
            ?>
    <div class="view-section">
                <table>
                    <tr>
                        <th></th>
                        <th><?php _e( 'Start time', 'esbcie' ); ?></th>
                        <th><?php _e( 'Start time meridian', 'esbcie' ); ?></th>
                        <th><?php _e( 'Start time sort', 'esbcie' ); ?></th>
                        <th><?php _e( 'End time', 'esbcie' ); ?></th>
                        <th><?php _e( 'End time meridian', 'esbcie' ); ?></th>
                        <th><?php _e( 'For date', 'esbcie' ); ?></th>
                        <th><?php _e( 'Slots', 'esbcie' ); ?></th>
                    </tr>
                    <?php foreach( $timings as $key => $value ) { ?>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $value->start_time; ?></td>
                        <td><?php echo $value->start_meridian; ?></td>
                        <td><?php echo $value->start_time_sort; ?></td>
                        <td><?php echo $value->end_time; ?></td>
                        <td><?php echo $value->end_meridian; ?></td>
                        <td><?php echo $value->for_date; ?></td>
                        <td><?php echo $value->slots; ?></td>
                    <?php $i++; } ?>
                </table>
    </div>
            <?php
        }
    ?>
    <!-- Data table end -->    
</div>