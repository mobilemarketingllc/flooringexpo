<?php

/**
 * Settings Page
 * Handles to settings
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

?>
<div class="wrap">
    
    <h2><?php _e( 'Comment Import - Export', 'esbcie' ); ?></h2>
    
    <!-- Export user start -->
    <?php 
        $comment_fields = esb_cie_get_comment_fields();
        if( !empty( $comment_fields ) ){
    ?>
            <table class="form-table esb-cie-form-table">
                <tr>
                    <td colspan="2" valign="top" scope="row">
                        <strong><?php _e( 'Export', 'esbcie' ); ?></strong>
                    </td>
                </tr>
            </table>
    
            <form method="post">                
                <table class="widefat importers esb-cie-importers">                    
                    <thead>
                        <tr>
                            <th class="cb"></th>
                            <th><strong><?php _e( 'ATTRIBUTE', 'esbcie' ) ?></strong></th>
                            <th><strong><?php _e( 'COLUMN NAME', 'esbcie' ) ?></strong></th>
                            <th><strong><?php _e( 'NOTICE', 'esbcie' ) ?></strong></th>
                        </tr>
                    </thead>
                    
                    <?php
                        foreach( $comment_fields as $field_key => $field_data )
                        {
                            $row_class = ( $field_key % 2 == 0 ) ? ' alternate ' : '';
                            $key    = isset( $field_data['key'] ) ? $field_data['key'] : '';
                            $label  = isset( $field_data['label'] ) ? $field_data['label'] : '';
                            $notice = isset( $field_data['notice'] ) ? $field_data['notice'] : '';
                    ?>
                            <tr class="<?php echo $row_class ?>">
                                <td><input type="checkbox" id="esb_cie_user_<?php echo $key ?>" name="esb_cie_column_name[]" value="<?php echo $key ?>" checked="checked" /></td>
                                <td class="row-title"><label for="esb_cie_comment_<?php echo $key ?>"><?php echo $label ?></label></td>
                                <td><code><?php echo $key ?></code></td>
                                <td><span class="description"><?php echo $notice ?></span></td>
                            </tr>
                            
                    <?php } ?>                            
                </table>
                
                <p>                    
                    <input type="submit" name="esb_cie_export_comment_csv" class="button-secondary" value="<?php _e( 'Export CSV', 'esbcie' ) ?>" />                    
                </p>
                
            </form>
    <?php
        }
    ?>
    <!-- Export user end -->    
</div>