<?php

/**
 * Settings Page
 * Handles to settings
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

?>
<div class="wrap">
    
    <h2><?php _e( 'User Import - Export', 'esbcie' ); ?></h2>
    
    <?php 
        if( isset( $_GET['cie_import'] ) || isset( $_GET['cie_ignore'] ) ){
            $total_import = !empty( $_GET['cie_import'] ) ? $_GET['cie_import'] : 0;
            $total_ignore = !empty( $_GET['cie_ignore'] ) ? $_GET['cie_ignore'] : 0;
            
            echo "<div class='updated below-h2'>
                    <p><strong>{$total_import}</strong> user was successfully imported. <strong>{$total_ignore}</strong> user ignored.</p>
                </div>";
        }
    ?>
    
    <!-- Export user start -->
    <?php 
        $user_fields = esb_cie_get_user_fields();
        if( !empty( $user_fields ) ){
    ?>
            <table class="form-table esb-cie-form-table">
                <tr>
                    <td colspan="2" valign="top" scope="row">
                        <strong><?php _e( 'Export', 'esbcie' ); ?></strong>
                    </td>
                </tr>
            </table>
    
            <form method="post">                
                <table class="widefat importers esb-cie-importers">                    
                    <thead>
                        <tr>
                            <th class="cb"></th>
                            <th><strong><?php _e( 'ATTRIBUTE', 'esbcie' ) ?></strong></th>
                            <th><strong><?php _e( 'COLUMN NAME', 'esbcie' ) ?></strong></th>
                            <th><strong><?php _e( 'NOTICE', 'esbcie' ) ?></strong></th>
                        </tr>
                    </thead>
                    
                    <?php
                        foreach( $user_fields as $field_key => $field_data )
                        {
                            $row_class = ( $field_key % 2 == 0 ) ? ' alternate ' : '';
                            $key    = isset( $field_data['key'] ) ? $field_data['key'] : '';
                            $label  = isset( $field_data['label'] ) ? $field_data['label'] : '';
                            $notice = isset( $field_data['notice'] ) ? $field_data['notice'] : '';
                    ?>
                            <tr class="<?php echo $row_class ?>">
                                <td><input type="checkbox" id="esb_cie_user_<?php echo $key ?>" name="esb_cie_column_name[]" value="<?php echo $key ?>" checked="checked" /></td>
                                <td class="row-title"><label for="esb_cie_user_<?php echo $key ?>"><?php echo $label ?></label></td>
                                <td><code><?php echo $key ?></code></td>
                                <td><span class="description"><?php echo $notice ?></span></td>
                            </tr>
                            
                    <?php } ?>                            
                </table>
                
                <table class="widefat importers esb-cie-importers">
                    <thead>
                        <th colspan="2" valign="top" scope="row">
                            <strong><?php _e( 'Enter value for below options to retrive specific users only', 'esbcie' ); ?></strong>
                        </th>
                    </thead>
                    <tr class="alternate">
                        <td><?php _e( 'Role ', 'esbcie' ); ?></td> 
                        <td>
                            <select name="esb_cie_selected_role" class="esb-role-select">
                                <option value=""><?php _e( '---Select Role---', 'esbcie' ); ?></option>
                                <?php wp_dropdown_roles(); ?>
                            </select>
                        </td> 
                    </tr>
                    <tr>
                        <td><?php _e( 'Start Date ', 'esbcie' ); ?></td>
                        <td>
                            <input type="text" name="esb_cie_start_selected_date" class="datepicker">
                        </td>
                    </tr>
                    <tr class="alternate">
                        <td><?php _e( 'End Date ', 'esbcie' ); ?></td>
                        <td>
                            <input type="text" name="esb_cie_end_selected_date" class="datepicker">
                        </td>
                    </tr>
                </table>
                
                <p>                    
                    <input type="submit" name="esb_cie_export_user_csv" class="button-secondary" value="<?php _e( 'Export CSV', 'esbcie' ) ?>" />                    
                </p>
                
            </form>
    <?php
        }
    ?>
    <!-- Export user end -->
    
    <!-- Import user start -->
    <table class="form-table esb-cie-form-table">
        <tr>
            <td colspan="2" valign="top" scope="row">
                <strong><?php _e( 'Import from file', 'esbcie' ); ?></strong>
            </td>
        </tr>
    </table>
    
    <form method="post" enctype="multipart/form-data">        
        <p>            
            <input type="file" name="esb_cie_user_import_file" />
            <input type="submit" name="esb_cie_user_import_csv" class="button-secondary" value="<?php _e( 'Import From CSV', 'esbcie' ) ?>" />
        </p>
    </form>
    <!-- Import user end -->
</div>