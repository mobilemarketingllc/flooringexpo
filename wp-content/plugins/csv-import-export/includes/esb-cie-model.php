<?php

/**
 * Model File
 * Handles to database functionality & other functions
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
* Escape Attr
*/
function esb_cie_escape_attr($data){

    if( !empty( $data ) ) {
        $data = esc_attr(stripslashes_deep($data));
    }
    return $data;
}

/**
* Strip Slashes From Array
*/
function esb_cie_escape_slashes_deep($data = array(),$flag=true){

    if($flag != true) {
            $data = esb_cie_nohtml_kses($data);
    }
    $data = stripslashes_deep($data);
    return $data;
}

/**
* Strip Html Tags 
* 
* It will sanitize text input (strip html tags, and escape characters)
*/
function esb_cie_nohtml_kses($data = array()) {

    if ( is_array($data) ) {

            $data = array_map(array($this,'esb_cie_nohtml_kses'), $data);

    } elseif ( is_string( $data ) ) {

            $data = wp_filter_nohtml_kses($data);
    }

    return $data;
}

/**
 * Convert Object To Array
 */
function esb_cie_object_to_array($result) {

    $array = array();
    foreach ($result as $key=>$value)
    {	
        if (is_object($value)) {
            $array[$key]=esb_cie_object_to_array($value);
        } else {
            $array[$key]=$value;
        }
    }
    return $array;
}

/**
 * Get Date Format
 * 
 * Handles to return formatted date which format is set in backend
 */
function esb_cie_get_date_format( $date, $time = false ) {

    $format = $time ? get_option( 'date_format' ).' '.get_option('time_format') : get_option('date_format');
    $date = date_i18n( $format, strtotime($date));
    return $date;
}

/**
 * Get Absulate path
 */
function esb_cie_get_absulate_path( $url ) {
    
    $abs = '';
    if( !empty( $url ) ) {
        
        $upload_path = wp_upload_dir();
        $abs = str_replace($upload_path['baseurl'], $upload_path['basedir'], $url);
    }
    return $abs;
}

/**
 * String Conversation with UTF-8
 * Version 1.1.0
 */
function esb_cie_string_conversion($string){
    if( !preg_match( '%(?:
       [\xC2-\xDF][\x80-\xBF]        # non-overlong 2-byte
       |\xE0[\xA0-\xBF][\x80-\xBF]               # excluding overlongs
       |[\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}      # straight 3-byte
       |\xED[\x80-\x9F][\x80-\xBF]               # excluding surrogates
       |\xF0[\x90-\xBF][\x80-\xBF]{2}    # planes 1-3
       |[\xF1-\xF3][\x80-\xBF]{3}                  # planes 4-15
       |\xF4[\x80-\x8F][\x80-\xBF]{2}    # plane 16
       )+%xs', $string ) ) {
        
        return utf8_encode($string);
    } else {
        return $string;
    }
}

/**
 * Get all post status
 */
function esb_cie_get_all_status() {
    $all_status = array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash');
    return $all_status;
}

/**
 * Check post status and return valid post status
 */
function esb_cie_check_post_status( $status = 'draft' ) {
    $all_status = esb_cie_get_all_status();
    if( !empty( $status ) && in_array( $status, $all_status ) ) {
        return $status;
    } else {
        return 'draft';
    }
}

/**
 * Check time and return valid time
 */
function esb_cie_check_time( $start_time ) {
    $time = array( 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 );
    if( !empty( $start_time ) && in_array( $start_time, $time ) ) {
        return $start_time;
    } else {
        return '';
    }
}

/**
 * Check time meridian and return valid time meridian
 */
function esb_cie_check_time_meridian( $time_meridian ) {
    $time = array( 'AM', 'PM' );
    if( !empty( $time_meridian ) && in_array( $time_meridian, $time ) ) {
        return $time_meridian;
    } else {
        return '';
    }
}

/**
 * Check start time sort and return valid start time sort
 */
function esb_cie_check_time_sort( $time_sort ) {
    $time = array( 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 );
    if( !empty( $time_sort ) && in_array( $time_sort, $time ) ) {
        return $time_sort;
    } else {
        return '';
    }
}

/**
 * Check post author and return valid post author
 */
function esb_cie_check_post_author( $author ) {
    
    global $user_ID;

    if( !empty( $author ) && !intval( $author ) ) {
        $user = get_user_by( 'login', $author );
        return $user->ID;
    } else {
        return $user_ID;
    }
}

/**
 * Check post date and return valid post date
 */
function esb_cie_check_post_date( $post_date = '' ) {
    
    if( !empty( $post_date ) ) {
        $timestamp = strtotime( $post_date );
        $post_date = date( 'Y-m-d H:i:s', $timestamp );
    }
    return $post_date;
}


/**
 * Check post by post slug and return valid post id
 */
function esb_cie_check_post_by_slug( $post_slug, $post_type = 'any' ) {
    
    if( !empty( $post_slug ) ) {
        $args = array(
                            'post_type'     => $post_type,
                            'name'          => sanitize_title($post_slug),
                            'posts_per_page'=> 1,
                            'fields'        => 'ids'
                        );
        $post_ids = get_posts( $args );
        return !empty( $post_ids ) && !empty( $post_ids['0'] ) ? $post_ids['0'] : 0;
    } else {
        return 0;
    }
}

/**
 * Check post image and return valid post image
 */
function esb_cie_check_post_image( $post_image_slug ) {
    
    if( !empty( $post_image_slug ) ) {
        $args = array(
                            'post_type'     => 'attachment',
                            'name'          => sanitize_title($post_image_slug),
                            'posts_per_page'=> 1,
                            'fields'        => 'ids'
                        );
        $post_ids = get_posts( $args );
        return !empty( $post_ids ) && !empty( $post_ids['0'] ) ? $post_ids['0'] : 0;
    } else {
        return 0;
    }
}

/**
 * Check comment status and return valid comment status
 */
function esb_cie_check_comment_status( $status = 'closed' ) {
    
    if( !empty( $status ) && in_array( $status, array( 'open', 'closed' ) ) ) {
        return $status;
    } else {
        return 'closed';
    }
}

/**
 * Check ping status and return valid ping status
 */
function esb_cie_check_ping_status( $status = 'closed' ) {
    
    if( !empty( $status ) && in_array( $status, array( 'open', 'closed' ) ) ) {
        return $status;
    } else {
        return 'closed';
    }
}

/**
 * Get all post types
 */
function esb_cie_get_all_post_types() {
    
    $args = array(
                    'public'   => true
                );
    $output = 'objects'; // names or objects
    $post_types = get_post_types( $args, $output );
    unset( $post_types['attachment'] );
    
    return $post_types;
}

/**
 * Get all taxonomies
 */
function esb_cie_get_all_taxonomies( $post_type ) {
    
    $taxonomies = array();
    if( !empty( $post_type ) ) {
        
        $taxonomies = get_object_taxonomies( $post_type, 'objects' );
        if( !empty( $taxonomies ) ) {
            if( isset( $taxonomies['post_format'] ) ) {
                unset( $taxonomies['post_format'] );
            }
            foreach( $taxonomies as $key => $taxonomy ) {               
                if( empty( $taxonomy->public ) ) {
                    unset( $taxonomies[$key] );
                }
            }
        }
    }
    return $taxonomies;
}

/**
 * Get Post Type Tabs
 */
function esb_cie_post_type_tabs() {
    
    $post_types = esb_cie_get_all_post_types();
    
    $all_post_types = array();
    if( !empty( $post_types ) ) {
        foreach ( $post_types as $post_type => $post_type_data ) {            
            $menu_title = !empty( $post_type_data->labels ) && !empty( $post_type_data->labels->menu_name ) ? $post_type_data->labels->menu_name : $post_type_data->label;
            $all_post_types[$post_type] = $menu_title;
        }
    }
    
    $tabs = array();
    
    $tabs = array_merge( $tabs, $all_post_types );
    
    return $tabs;
}

/**
 * Get Tabs For Importing ACF fields
 */
function esb_cie_acf_field_tabs() {
    
    $tabs = array(
                    'acf-timing' => 'Timings'
    );
    
    return $tabs;
}

/**
 * Insert Term
 */
function esb_cie_insert_term( $taxonomy_name, $term_title, $term_slug = '', $term_description = '', $parent_term_id = 0 ) {
    
    $current_term_id = 0;
    if( !empty( $taxonomy_name ) && !empty( $term_title ) ) {
        $result = wp_insert_term(
                        $term_title, // the term 
                        $taxonomy_name, // the taxonomy
                        array(
                            'description'=> $term_description,
                            'slug'       => $term_slug,
                            'parent'     => $parent_term_id
                        )
                    );
        if( !is_wp_error( $result ) ) {
            $current_term_id = isset( $result['term_id'] ) ? $result['term_id'] : 0;
        }
    }
    return $current_term_id;
}

/**
 * Get all post fields
 */
function esb_cie_get_all_post_fields() {
    
    $default_options = array(
                                array(
                                        'key'       => 'post_name',
                                        'label'     => __( 'Name / Identifier / Slug', 'esbcie' ),
                                        'notice'    => __( 'Name (slug) for identifing item', 'esbcie' ),
                                ),
                                array(
                                        'key'       => 'post_title',
                                        'label'     => __( 'Title', 'esbcie' ),
                                        'notice'    => ''
                                ),
                                array(
                                        'key'       => 'post_status',
                                        'label'     => __( 'Status', 'esbcie' ),
                                        'notice'    => __( 'Available values: <b>draft</b>, <b>publish</b>, <b>pending</b>, <b>future</b>, <b>private</b>. Default value is <b>draft</b>', 'esbcie' )
                                ),
                                array(
                                        'key'       => 'post_content',
                                        'label'     => __( 'Content', 'esbcie' ),
                                        'notice'    => ''
                                ),
                                array(
                                        'key'       => 'post_excerpt',
                                        'label'     => __( 'Excerpt', 'esbcie' ),
                                        'notice'    => ''
                                ),
                                array(
                                        'key'       => 'post_author',
                                        'label'     => __( 'Author username', 'esbcie' ),
                                        'notice'    => __( 'Default author is currently logged user', 'esbcie' )
                                ),
                                array(
                                        'key'       => 'post_parent',
                                        'label'     => __( 'Parent', 'esbcie' ),
                                        'notice'    => __( 'Name (slug) of parent item if post type support it', 'esbcie' )
                                ),
                                array(
                                        'key'       => 'post_date',
                                        'label'     => __( 'Date', 'esbcie' ),
                                        'notice'    => __( 'Date in format: <b>Y-m-d H:i:s</b> (e.g. <b>2014-08-20 17:16:18</b>). Default insert current date and time.', 'esbcie' )
                                ),
                                array(
                                        'key'       => 'post_image',
                                        'label'     => __( 'Featured Image', 'esbcie' ),
                                        'notice'    => __( 'Slug (name) of media file', 'esbcie' )
                                ),
                                array(
                                        'key'       => 'comment_status',
                                        'label' => __( 'Comment status', 'esbcie' ),
                                        'notice' => __( 'Available values: <b>closed</b>, <b>open</b>. Default value is <b>closed</b>', 'esbcie' )
                                ),
                                array(
                                        'key'       => 'ping_status',
                                        'label' => __( 'Ping status', 'esbcie' ),
                                        'notice' => __( 'Available values: <b>closed</b>, <b>open</b>. Default value is <b>closed</b>', 'esbcie' )
                                ),
                        );
    return $default_options;
}

/**
 * Get all timing fields
 */
function esb_cie_get_all_timing_fields() {
    
    $default_options = array(
                                array(
                                        'key'       => 'start_time',
                                        'label'     => __( 'Start time', 'esbcie' ),
                                        'notice'    => __( 'Enter the start time', 'esbcie' ),
                                ),
                                array(
                                        'key'       => 'start_meridian',
                                        'label'     => __( 'Start time meridian', 'esbcie' ),
                                        'notice'    => __( 'Enter the start time meridian', 'esbcie' ),
                                ),
                                array(
                                        'key'       => 'start_time_sort',
                                        'label'     => __( 'Start time sort', 'esbcie' ),
                                        'notice'    => __( 'Enter the start time sort', 'esbcie' ),
                                ),
                                array(
                                        'key'       => 'end_time',
                                        'label'     => __( 'End time', 'esbcie' ),
                                        'notice'    => __( 'Enter the end time', 'esbcie' ),
                                ),
                                array(
                                        'key'       => 'end_meridian',
                                        'label'     => __( 'End time meridian', 'esbcie' ),
                                        'notice'    => __( 'Enter the end time meridian', 'esbcie' ),
                                ),
                                array(
                                        'key'       => 'created_date',
                                        'label'     => __( 'Created date', 'esbcie' ),
                                        'notice'    => __( 'Enter the created date', 'esbcie' ),
                                ),
                                array(
                                        'key'       => 'modified_date',
                                        'label'     => __( 'Modified date', 'esbcie' ),
                                        'notice'    => __( 'Enter the modified date', 'esbcie' ),
                                ),
                                array(
                                        'key'       => 'for_date',
                                        'label'     => __( 'For date', 'esbcie' ),
                                        'notice'    => __( 'Enter the for date', 'esbcie' ),
                                ),
                                array(
                                        'key'       => 'slots',
                                        'label'     => __( 'Slots', 'esbcie' ),
                                        'notice'    => __( 'Enter the slots', 'esbcie' ),
                                ),
                        );
    return $default_options;
}

/**
 * Get all term fields
 */
function esb_cie_get_all_term_fields() {
    
    $default_options = array(
                                array(
                                        'key'       => 'slug',
                                        'label'     => __( 'Name / Identifier / Slug', 'esbcie' ),
                                        'notice'    => __( 'Name (slug) for identifing item', 'esbcie' ),
                                ),
                                array(
                                        'key'       => 'name',
                                        'label'     => __( 'Title', 'esbcie' ),
                                        'notice'    => ''
                                ),
                                array(
                                        'key'       => 'description',
                                        'label'     => __( 'Description', 'esbcie' ),
                                        'notice'    => ''
                                ),
                        );
    return $default_options;
}

/**
 * Get all user fields
 */
function esb_cie_get_user_fields(){
    
    $default_options = array(
                                array(
                                        'key'       => 'user_login',
                                        'label'     => __( 'Username', 'esbcie' ),
                                        'notice'    =>'',
                                ),
                                array(
                                        'key'       => 'user_pass',
                                        'label'     => __( 'Password', 'esbcie' ),
                                        'notice'    => __( 'Password in encrypted format.', 'esbcie' ),
                                ),
                                array(
                                        'key'       => 'user_nicename',
                                        'label'     => __( 'Nickname', 'esbcie' ),
                                        'notice'    => ''
                                ),
                                array(
                                        'key'       => 'user_email',
                                        'label'     => __( 'Email', 'esbcie' ),
                                        'notice'    => ''
                                ),
                                array(
                                        'key'       => 'user_url',
                                        'label'     => __( 'Website', 'esbcie' ),
                                        'notice'    => ''
                                ),
                                array(
                                        'key'       => 'user_registered',
                                        'label'     => __( 'User registered date', 'esbcie' ),
                                        'notice'    => __( 'Date in format: <b>Y-m-d H:i:s</b> (e.g. <b>2014-08-20 17:16:18</b>). Default insert current date and time.', 'esbcie' ),
                                ),
                                array(
                                        'key'       => 'display_name',
                                        'label'     => __( 'Display Name', 'esbcie' ),
                                        'notice'    => __( 'This name display publicly')
                                ),
                                array(
                                        'key'       => 'first_name',
                                        'label'     => __( 'First Name', 'esbcie' ),
                                        'notice'    => ''
                                ),
                                array(
                                        'key'       => 'last_name',
                                        'label'     => __( 'Last Name', 'esbcie' ),
                                        'notice'    => ''
                                ),
                                array(
                                        'key'       => 'description',
                                        'label'     => __( 'Biographical Info', 'esbcie' ),
                                        'notice'    => __( 'User related information', 'esbcie' ),
                                ),
                                array(
                                        'key'       => 'roles',
                                        'label'     => __( 'User role', 'esbcie' ),
                                        'notice'    => ''
                                ),
                                array(
                                        'key'       => 'show_admin_bar_front',
                                        'label'     => __( 'Toolbar', 'esbcie' ),
                                        'notice'    => 'Show admin bar in front side'
                                ),
                        );
    return $default_options;
}

/**
 * Check username is exist or not
 */
function esb_cie_check_user_login( $username ){
    if( !empty( $username ) && !username_exists( $username ) ){
        return true;
    }
    return false;    
}

/**
 * Check email is exist or not
 */
function esb_cie_check_user_email( $email ){
    if( !empty( $email ) && !email_exists( $email ) ){
        return true;
    }
    return false;    
}

/**
 * Get all comment fields
 */
function esb_cie_get_comment_fields(){
    
    $default_options = array(
                                array(
                                        'key'       => 'comment_post_ID',
                                        'label'     => __( 'Comment post id', 'esbcie' ),
                                        'notice'    =>'',
                                ),
                                array(
                                        'key'       => 'comment_author',
                                        'label'     => __( 'Author', 'esbcie' ),
                                        'notice'    => ''
                                ),
                                array(
                                        'key'       => 'comment_author_email',
                                        'label'     => __( 'Author email', 'esbcie' ),
                                        'notice'    => ''
                                ),
                                array(
                                        'key'       => 'comment_author_url',
                                        'label'     => __( 'Author url', 'esbcie' ),
                                        'notice'    => ''
                                ),
                                array(
                                        'key'       => 'comment_author_IP',
                                        'label'     => __( 'Author IP', 'esbcie' ),
                                        'notice'    => ''
                                ),
                                array(
                                        'key'       => 'comment_date',
                                        'label'     => __( 'Comment date', 'esbcie' ),
                                        'notice'    => __( 'Date in format: <b>Y-m-d H:i:s</b> (e.g. <b>2014-08-20 17:16:18</b>). Default insert current date and time.', 'esbcie' ),
                                ),
                                array(
                                        'key'       => 'comment_content',
                                        'label'     => __( 'Content', 'esbcie' ),
                                        'notice'    => ''
                                ),
                                array(
                                        'key'       => 'comment_approved',
                                        'label'     => __( 'Comment status', 'esbcie' ),
                                        'notice'    => ''
                                ),
                                array(
                                        'key'       => 'comment_agent',
                                        'label'     => __( 'Comment agent', 'esbcie' ),
                                        'notice'    => ''
                                ),
                                array(
                                        'key'       => 'comment_parent',
                                        'label'     => __( 'Comment parent', 'esbcie' ),
                                        'notice'    => ''
                                ),
                        );
    return $default_options;
}
?>